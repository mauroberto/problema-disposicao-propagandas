import matplotlib.pyplot as plt
import numpy as np

instances = [
	"100_10_50",
	"100_30_40",
	"1000_10_200",
	"1000_100_20",
	"1000_100_20",
	"1000_100_50",
	"1000_100_50_1",
	"1000_100_50_2",
	"1000_100_50",
	"1100_100_50"
];

algorithms = [
	("samplecode", "brkgaAPI"),
	("PLI", "PLI")
];

x = []

for i in range(0, len(instances)):
	x.append(i)

x = np.array(x)

plt.xticks(x, instances,  rotation='vertical')

legends = []

#solution graph
for (alg, path) in algorithms:
	y = []
	for inst in instances:
		file_object  = open("./%s/solutions/%s.sol" % (path, inst), "r")
		y.append(int(file_object.readline()))
		file_object.close()
	print(y)
	plt.plot(x, y)
	legends.append(alg)

plt.ylabel("Solucao")
plt.xlabel("Instancia")
plt.legend(legends)
plt.show()

plt.xticks(x, instances,  rotation='vertical')

#time graph
for (alg, path) in algorithms:
	y = []
	for inst in instances:
		file_object  = open("./%s/solutions_time/%s.out" % (path, inst), "r")
		y.append(float(file_object.readline()))
		file_object.close()
	print(y)
	plt.plot(x, y)

plt.ylabel("Tempo")
plt.xlabel("Instancia")
plt.legend(legends)
plt.show()



