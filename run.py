# coding: utf-8

# # Tests

from multiprocessing.dummy import Pool as ThreadPool
import os
#import commands
import sys
import datetime
import socket

threads = 7
tempo = 600

PATH1 = "MAXSPACE_VARIANTE/"
PATH2 = "MAXSPACE/"
PATH3 = "MINSPACE_VARIANTE/"
PATH4 = "MINSPACE/"

classesMAXSPACE = [1, 5, 9, 13, 17, 21, 25, 29, 33]
classesMAXSPACE_VARIANTE = [2, 3, 4, 6, 7, 8, 10, 11, 12, 14, 15, 16, 18, 19, 20, 22, 23, 24, 26, 27, 28, 30, 31, 32, 34, 35, 36] 
classesMAXSPACE_VARIANTE2 = [39, 40, 41, 42, 43, 44, 45, 46, 47]

algorithms1 = [
    #(PATH1+"Hybrid-GA/HGA", PATH1+"Hybrid-GA", "Hybrid-GA", PATH1+"Hybrid-GA", "350 0.2 0.2 0.7 1 230"), 
    #(PATH1+"GRASP/GRASP", PATH1+"GRASP", "GRASP", PATH1+"GRASP", "1000000 0.6"),
    #(PATH1+"GRASP-VNS/GRASP-VNS", PATH1+"GRASP-VNS", "GRASP-VNS", PATH1+"GRASP-VNS", "1000000 0.2 9 1 1 1 1 1"),
    #(PATH1+"GRASP-Tabu/GRASP-Tabu", PATH1+"GRASP-Tabu", "GRASP-Tabu", PATH1+"GRASP-Tabu", "1000000 0.2 100 320 2 1 1 1 1 1"),
    #(PATH1+"VNS/VNS", PATH1+"VNS", "VNS", PATH1+"VNS", "5 0 1 1 1 1 1"),
    (PATH1+"PLI/PLI", PATH1+"PLI", "PLI", PATH1+"PLI", "")
]

algorithms2 = [
    #(PATH2+"VNS/VNS", PATH2+"VNS", "VNS", PATH2+"VNS", "8 0.2 1 1 1 1"),
    #(PATH2+"Hybrid-GA/HGA", PATH2+"Hybrid-GA", "Hybrid-GA", PATH2+"Hybrid-GA", "400 0.2 0.1 0.7 3 300"),
    #(PATH2+"GRASP/GRASP", PATH2+"GRASP", "GRASP", PATH2+"GRASP", "2000 0.3"),
    #(PATH2+"GRASP-VNS/GRASP-VNS", PATH2+"GRASP-VNS", "GRASP-VNS", PATH2+"GRASP-VNS", "1000 0.5 10 1 1 1 1"),
    #(PATH2+"GRASP-Tabu/GRASP-Tabu", PATH2+"GRASP-Tabu", "GRASP-Tabu", PATH2+"GRASP-Tabu", "2000 0.9 55 60 1 1 1 1 1"),
    (PATH2+"PLI/PLI", PATH2+"PLI", "PLI", PATH2+"PLI", "")
]

algorithms3 = [
    #(PATH1+"VNS-RPCK/VNS", PATH1+"VNS-RPCK", "VNS-RPCK", PATH1+"VNS-RPCK"),
    #(PATH1+"VNS/VNS", PATH1+"VNS", "VNS", PATH1+"VNS"),
    #(PATH1+"GRASP-Tabu/GRASP-Tabu", PATH1+"GRASP-Tabu", "GRASP-Tabu", PATH1+"GRASP-Tabu"),
    #(PATH1+"GRASP/GRASP", PATH1+"GRASP", "GRASP", PATH1+"GRASP"),
    #(PATH1+"GRASP-VNS/GRASP-VNS", PATH1+"GRASP-VNS", "GRASP-VNS", PATH1+"GRASP-VNS"),
    #(PATH1+"Hybrid-GA/HGA", PATH1+"Hybrid-GA", "Hybrid-GA", PATH1+"Hybrid-GA")
]

#(PATH3+"heuristica/main", PATH3+"heuristica", "Heuristica-MINSPACE", PATH3+"heuristica"),
#(PATH3+"PLI/PLI", PATH3+"PLI", "PLI", PATH3+"PLI"),
#(PATH3+"LSLF/LSLF", PATH3+"LSLF", "LSLF", PATH3+"LSLF"),
#(PATH4+"heuristica/main", PATH4+"heuristica", "Heuristica-MINSPACE", PATH4+"heuristica"),
#(PATH4+"PLI/PLI", PATH4+"PLI", "PLI", PATH4+"PLI"),
#(PATH4+"LSLF/LSLF", PATH4+"LSLF", "LSLF", PATH4+"LSLF")

# ## Compile algorithms

#os.system('export GUROBI_HOME="/opt/gurobi752/linux64"')
#os.system('export PATH="${PATH}:${GUROBI_HOME}/bin"')
#os.system('export LD_LIBRARY_PATH="${LD_LIBRARY_PATH}:${GUROBI_HOME}/lib"')

for (alg, path, alg_name, alg_comp, params) in algorithms1 + algorithms2:
    #print("make %s" % (alg_name))
    #os.system("cd %s && make" % (alg_comp))

    #continue
    print("mkdir %s" % (alg_name))
    os.system("mkdir -p %s/solutions" % (path))
    os.system("mkdir -p %s/solutions_log" % (path))
    os.system("mkdir -p %s/solutions_time" % (path))
    os.system("mkdir -p %s/solutions_evol" % (path))

    print("")


# ## Run Tests

def run(tple):
    (cmd, id) = tple
    print (cmd)
    print (str(id)+"/"+str(len(cmds)))
    print (str(id/len(cmds)*100)+"%")
    os.system(cmd)

cmds = []

sizes = [
    (10000, 500, 200),
    (1000, 500, 250),
    (500, 250, 100),
    (100, 75, 50)
]

cont = 0

for classe in classesMAXSPACE_VARIANTE + classesMAXSPACE_VARIANTE2:
    for tam in sizes:
        for i in range(0, 10):
            inst = "%d_%d_%d_%d_%d" % (tam[0], tam[1], tam[2], classe, i)
            for (alg, path, alg_name, alg_comp, params) in algorithms1:
                #print ("./%s %s.pdp" % (alg_name, inst))
                cmd = "(/usr/bin/time -f '%e'"+" ./%s instances/instances/%s.pdp %d %s > %s/solutions_log/%s.log) 2> %s/solutions_time/%s.out" % (alg, inst, tempo, params, path, inst, path, inst)
                cmds.append((cmd, cont+1))
                cont = cont + 1
                #print("")


print(len(cmds))

for classe in classesMAXSPACE:
    for tam in sizes:
        for i in range(0, 10):
            inst = "%d_%d_%d_%d_%d" % (tam[0], tam[1], tam[2], classe, i)
            for (alg, path, alg_name, alg_comp, params) in algorithms2:
                #print ("./%s %s.pdp" % (alg_name, inst))
                cmd = "(/usr/bin/time -f '%e'"+" ./%s instances/instances/%s.pdp %d %s > %s/solutions_log/%s.log) 2> %s/solutions_time/%s.out" % (alg, inst, tempo, params, path, inst, path, inst)
                cmds.append((cmd, cont+1))
                cont = cont + 1
                #print("")

print(len(cmds))

for classe in classesMAXSPACE_VARIANTE2:
    for tam in sizes:
        for i in range(0, 10):
            inst = "%d_%d_%d_%d_%d" % (tam[0], tam[1], tam[2], classe, i)
            for (alg, path, alg_name, alg_comp, params) in algorithms3:
                #print ("./%s %s.pdp" % (alg_name, inst))
                cmd = "(/usr/bin/time -f '%e'"+" ./%s instances/instances/%s.pdp %d %s > %s/solutions_log/%s.log) 2> %s/solutions_time/%s.out" % (alg, inst, tempo, params, path, inst, path, inst)
                cmds.append((cmd, cont+1))
                cont = cont + 1
                #print("")

print(len(cmds))

arr = os.listdir("./instances/literatura") 

for inst in arr:
    inst = inst.split(".")[0]
    for (alg, path, alg_name, alg_comp, params) in algorithms2:
        #print ("./%s %s.pdp" % (alg_name, inst))
        cmd = "(/usr/bin/time -f '%e'"+" ./%s instances/instances/%s.pdp %d %s > %s/solutions_log/%s.log) 2> %s/solutions_time/%s.out" % (alg, inst, tempo, params, path, inst, path, inst)
        cmds.append((cmd, cont+1))
        cont = cont + 1


#cmds = cmds[-100:]

print (len(cmds))
#print (cmds)
print (len(arr))

dias = ((((len(cmds)*tempo)/threads))/84600)


print ("Aproximadamente %.1f dias" % (dias))

pool = ThreadPool(threads)

pool.map(run, cmds)
pool.close()
pool.join()
