#ifndef LOCALSEARCH_H
#define LOCALSEARCH_H

#include <iostream>
#include <list>
#include <vector>
#include <algorithm>
#include <fstream>
#include <string>
#include <cstdlib>
#include <ctime>
#include "Neighborhood.h"
#include "Instance.h"
#include "Solution.h"
#include <time.h>
#include <sys/time.h>

class LocalSearch {
private:
  std::vector<Neighborhood*> neighbors;
  struct timeval start;
  int timeout; 
public:
	Instance* ins;

  std::vector<double> tempo;
  std::vector<int> qtd, melhorias;

  LocalSearch(Instance* ins, int timeout, timeval start);
  LocalSearch();
	~LocalSearch();

  Solution& createSolution();
	void BestImprovement(Solution & sol);
  void FirstImprovement(Solution & sol);
};

#endif