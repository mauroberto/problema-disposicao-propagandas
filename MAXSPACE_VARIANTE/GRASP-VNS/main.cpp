#include <stdio.h>
#include <cstdlib>
#include <ctime>
#include "LocalSearch.h"
#include "Instance.h"
#include "Solution.h"
#include "VNS.h"
#include "ConstructiveHeuristic.h"
#include <time.h>
#include <sys/time.h>
#include <omp.h>

using namespace std;

#ifndef PATHSOL
#define PATHSOL "solutions"
#endif

#ifdef _OPENMP
#define MAXT 4
#else
#define MAXT 1
#endif

Instance ins;

int timeout;
struct timeval start, stop;

int main(int argc, char* argv[]){
  if (argc < 2) {
    cout << "Usage: ./GRASP-VNS filename [timeout #it alpha Kmax MV RPCK ADDCPY ADD CHG]" << endl;
    return 1;
  }

  gettimeofday(&start, NULL);

  timeout = 3600;
  if(argc > 2){
	  timeout = stoi(argv[2]);
  }

	cout.precision(2);

  srand((int)time(0));

  int Kmax = 10, N_MAX = 5;

  if(argc > 5){
    Kmax = stoi(argv[5]);
  }

  Instance ins(argv[1]);

  vector<Neighborhood*> N;
  vector<Neighborhood*> N2;

  N.push_back(new Move(&ins));
  N.push_back(new Repack(&ins));
  N.push_back(new AddCopy(&ins));
  N.push_back(new AddItem(&ins));
  N.push_back(new ChangeItem(&ins));

  N2.push_back(new Move(&ins));
  N2.push_back(new Repack(&ins));
  N2.push_back(new AddCopy(&ins));
  N2.push_back(new AddItem(&ins));
  N2.push_back(new ChangeItem(&ins));

  for(int init = 6; init < 6 + N_MAX; init++){
    int flag;
    if(argc > init){
      flag = strtod(argv[init], NULL);
      if(!flag){
        delete N[init-6];
        delete N2[init-6];
        N[init-6] = NULL;
        N2[init-6] = NULL;
      }
    } else {
      break;
    }
  }

  N.erase(std::remove_if(N.begin(), N.end(), [](Neighborhood* n) { return n == NULL; }), N.end());
  N2.erase(std::remove_if(N2.begin(), N2.end(), [](Neighborhood* n) { return n == NULL; }), N2.end());

  N_MAX = N.size();

  LocalSearch localSearch(&ins, timeout, start);
  ConstructiveHeuristic ch(ins);
  VNS vns(&ins, Kmax, N_MAX, N, N2, timeout, start);

  int it = 1000;
  if(argc > 3){
    it = stoi(argv[3]);
  }
  double alpha = 0.1;
  if(argc > 4){
   alpha = strtod(argv[4], NULL);
  }

  Solution bestSol[MAXT];

  #ifdef _OPENMP
		#pragma omp parallel for num_threads(MAXT)
	#endif
  for(int i = 0; i < MAXT; i++){
    bestSol[i] = Solution(ins.N, ins.A, ins.S, &ins);
  }

  struct timeval stop;
  gettimeofday(&stop, NULL);
  double timeToTarget = ((double) (stop.tv_sec * 1000000 + stop.tv_usec) - (start.tv_sec * 1000000 + start.tv_usec)) / 1000000;

  #ifdef _OPENMP
    #pragma omp parallel for num_threads(MAXT)
  #endif
  int i, bestI = 0;
  for(i = 0; i < it; i++){
    gettimeofday(&stop, NULL);
    double elapsed = ((double) (stop.tv_sec * 1000000 + stop.tv_usec) - (start.tv_sec * 1000000 + start.tv_usec)) / 1000000;
    if(elapsed < timeout){
      #ifdef _OPENMP
        int tid = omp_get_thread_num();
      #else
        int tid = 0;
      #endif
      Solution sol = ch.greedyRandomizedConstruction3(alpha);

      vns.runVNSDualPhases(sol);

      cout << fixed << "iteration " << i << " sol " << sol.value << endl;

      if(sol.value > bestSol[tid].value){
        bestSol[tid] = sol;
        gettimeofday(&stop, NULL);
        timeToTarget = ((double) (stop.tv_sec * 1000000 + stop.tv_usec) - (start.tv_sec * 1000000 + start.tv_usec)) / 1000000;
        bestI = i;
      }
    } else {
      break;
    }
  }

  Solution sol = bestSol[0];

  for(int i = 1; i < MAXT; i++){
    if(bestSol[i].value > sol.value){
      sol = bestSol[i];
    }
  }

  cout << "Calls to change | Calls removed by BIT " << endl;
  cout << Solution::callChange << " | " << Solution::callChangeBIT << endl;

  cout << "Calls to add | Calls removed by BIT " << endl;
  cout << Solution::callAdd << " | " << Solution::callAddBIT << endl;

  cout << "Number of iterations = " << i << endl;
  cout << "Best Solution was found in iteration = " << bestI << endl;

  cout << "Time used by neighbor | Times each neighbor was used | Improvements" << endl;

  for(int i = 0; i < N_MAX; i++){
    cout << fixed << "N_" << i+1 << " = " << vns.tempo[i] << " | " << vns.qtd[i] << " | " << vns.melhorias[i] << endl;
  }

  cout << fixed << "Time to find the best solution = " << timeToTarget << endl;

  gettimeofday(&stop, NULL);
  cout << fixed << "Solution = " << sol.value << endl;
  double elapsed = ((double) (stop.tv_sec * 1000000 + stop.tv_usec) - (start.tv_sec * 1000000 + start.tv_usec)) / 1000000;
  cout << fixed << "Time = " << elapsed << endl;

  sol.exportToFile("MAXSPACE_VARIANTE/GRASP-VNS/solutions/"+ins.instanceName+".sol");

  for(int i = 0; i < N_MAX; i++){
    delete N[i];
    delete N2[i];
  }

  return 0;
}
