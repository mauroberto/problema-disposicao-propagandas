#ifndef SLOT_H
#define SLOT_H
#include <vector>
#include "Ad.h"

class Slot{
public:
  int s;
  int id;
  std::vector <bool> ads;

  Slot();
  Slot(int j, int A);
  Slot(const Slot& slot2);
	~Slot();

  Slot& operator= (const Slot& slot2);

  void addCopy(Ad & ad);
  void removeCopy(Ad & ad);
};

#endif
