#include "BITree.h"
#include <iostream>
#include <limits.h>
#include <math.h>

#define max(a, b) a > b ? a : b
#define G(x) x-(x & (x-1))

using namespace std;

BITree::BITree(){}

BITree::~BITree(){}

BITree::BITree(const BITree& bt2){
  this->N = bt2.N;
  this->biTree = bt2.biTree;
  this->v = bt2.v;
  this->left = bt2.left;
  this->right = bt2.right;
}

BITree& BITree::operator= (const BITree& bt2){
  if (this == &bt2)
    return *this; //self assignment
  this->N = bt2.N;
  this->biTree = bt2.biTree;
  this->v = bt2.v;
  this->left = bt2.left;
  this->right = bt2.right;
  return *this;
}

BITree::BITree(std::vector< int > arr){
  N = arr.size();

  biTree.resize(N+1, 0);
  v.resize(N+1, 0);
  left.resize(N+1, INT_MIN);
  right.resize(N+1, INT_MIN);

  for (int i=0; i<N; i++){
    updateBIT(i, arr[i]);
  }
}

void BITree::updateBIT(int index, int val){
  int i = index+1;
  int replaced = v[i];
  v[i] = val;
  int diff = val - replaced;
  while (i <= N) {
    biTree[i] += diff;
    i += i & (-i);
  }

  // Update left tree
  i = index+1;
  int value = val;
  while (i <= N) {
    if (value < left[i]) {
      if (replaced == left[i]) {
        value = max(value, v[i]);
        for (int r=1 ;; r++) {
          int x = ((unsigned int)i&(unsigned int)-i)>>r;
          if (x == 0) break;
          int child = i-x;
          value = max(value, left[child]);
        }
      } else break;
    }
    if (value == left[i]) break;
    left[i] = value;
    i += (i&-i);
  }
  // Update right tree
  i = index+1;
  value = val;
  while (i > 0) {
    if (value < right[i]) {
      if (replaced == right[i]) {
        value = max(value, v[i]);
        for (int r=1 ;; r++) {
          int x = ((unsigned int)i&(unsigned int)-i)>>r;
          if (x == 0) break;
          int child = i+x;
          if (child > N) break;
          value = max(value, right[child]);
        }
      } else break;
    }
    if (value == right[i]) break;
    right[i] = value;
    i -= (i&-i);
  }
}

int BITree::getSumInterval(int begin, int end){
  return getSum(end) - getSum(begin);
}

int BITree::getSum(int index){
  int sum = 0;
  index++;
  while (index > 0){
    sum += biTree[index];
    index -= index & (-index);
  }
  return sum;
}

int BITree::getMax(int begin, int end){
  int a = begin+1;
  int b = end+1;
  int max = v[a];
  int prev = a;
  int curr = prev + (prev&-prev); // parent right hand side
  while (curr <= b) {
    max = max(max, right[prev]); // value from the other tree
    prev = curr;
    curr = prev + (prev&-prev);
  }

  max = max(max, v[prev]);
  prev = b;
  curr = prev - (prev&-prev); // parent left hand side
  while (curr >= a) {
    max = max(max,left[prev]); // value from the other tree
    prev = curr;
    curr = prev - (prev&-prev);
  }
  return max;
}
