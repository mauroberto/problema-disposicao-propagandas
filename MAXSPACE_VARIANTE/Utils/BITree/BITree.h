#ifndef BITREE_H
#define BITREE_H

#include <vector>

using namespace std;

class BITree {
private:
  int N;
  vector<int> biTree;
  vector<int> v;
  vector<int> left;
  vector<int> right;

public:
	BITree(std::vector< int > arr);
  BITree();
  BITree(const BITree& bt2);
	~BITree();

  void updateBIT(int index, int val);
  int getSum(int index);
  int getSumInterval(int begin, int end);
  int getMax(int begin, int end);

  BITree& operator= (const BITree& sol2);
};

#endif
