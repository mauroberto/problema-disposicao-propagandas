#include "Instance.h"
#include <fstream>
#include <algorithm>

using namespace std;

Instance::Instance(string instanceName){
  ifstream infile(instanceName);

  this->instanceName = this->utils.getFileName(instanceName);

  infile >> this->A >> this->N >> this->S;

  int id, s, wMax, wMin, rd, dl;
  double v;

  while(infile >> id >> s >> rd >> dl >> wMin >> wMax >> v){
    Ad ad = Ad(id, s, wMax, wMin, rd, dl, v);
    this->ads.push_back(ad);
  }

  ranking.resize(A);

  for(int i = 0; i < A; i++) {
    double v = ads[i].v/ads[i].s;
    ranking[i].first = v;
    ranking[i].second = i;
  }

  sort(ranking.begin(), ranking.end(), greater<pair<double, int> >());
}

Instance::Instance(){

}

Instance::~Instance(){

}