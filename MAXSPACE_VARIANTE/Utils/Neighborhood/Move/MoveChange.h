#ifndef MOVE_CHANGE_H
#define MOVE_CHANGE_H

#include "Change.h"

class MoveChange : public Change{
private:
    virtual bool isEqual(const Change& other) const{
        MoveChange mc = static_cast<const MoveChange&>(other);
        return ad1 == mc.ad1 && c1 == mc.c1 && s2 == mc.s2;
    }
public:
    int ad1, c1, s2;
    MoveChange();
    MoveChange(Instance* ins, int ad1, int c1, int s2);
    ~MoveChange() override;
    Solution applyChange(const Solution & sol) override;
    int getSpace();
    virtual Change* clone() const override;

    MoveChange& operator=(const MoveChange& other){
        Change::operator =(other);
        ad1 = other.ad1;
        c1 = other.c1;
        s2 = other.s2;
        return *this;
    }
};

#endif