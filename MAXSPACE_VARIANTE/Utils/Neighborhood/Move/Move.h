#ifndef MOVE_H
#define MOVE_H

#include "Neighborhood.h"
#include "MoveChange.h"
#include <vector>

class Move : public Neighborhood{
public:
  Move();
  Move(Instance* instance);
  ~Move() override;

  void maxNeighborhood(Solution & sol);
  bool randomNeighborhood(Solution & sol);
  void generateNeighbors(Solution & sol) override;
  void bestImprovement(Solution & sol) override;
};

#endif