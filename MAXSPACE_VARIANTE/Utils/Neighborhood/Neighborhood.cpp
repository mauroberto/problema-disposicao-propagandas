#include "Neighborhood.h"
#include <iostream>
#include <algorithm>

Neighborhood::Neighborhood(){}

Neighborhood::Neighborhood(Instance* ins){
  this->ins = ins;
}

Neighborhood::~Neighborhood(){
  for (std::vector< Change* >::iterator it = this->neighbors.begin() ; it != this->neighbors.end(); ++it){
    delete (*it);
  }
}

void Neighborhood::maxNeighborhood(Solution & sol){
  sol = this->max;
}

bool Neighborhood::randomNeighborhood(Solution & sol){
  bool next_neighborhood = false;

  int s = neighbors.size();

  if(s){
    int r = this->utils.getRandomNumber(0, s-1);
    sol = this->neighbors[r]->applyChange(sol);
    Change* c = this->neighbors[r];
    this->neighbors[r] = this->neighbors[s-1];
    this->neighbors[s-1] = c;
    std::for_each(this->neighbors.end() - 1, this->neighbors.end(), []( Change* element) { delete element; });
    this->neighbors.erase(this->neighbors.end() - 1, this->neighbors.end());
  }else{
    next_neighborhood = true;
  }

  return next_neighborhood;
}

void Neighborhood::generateNeighbors(Solution & sol){

}

void Neighborhood::bestImprovement(Solution & sol){

}