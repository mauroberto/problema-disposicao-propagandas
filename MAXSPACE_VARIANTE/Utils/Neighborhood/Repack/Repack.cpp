#include "Repack.h"
#include <iostream>
#include <algorithm>

using namespace std;

Repack::Repack(){

}

Repack::Repack(Instance* instance) : Neighborhood(instance){
  this->ins = instance;
}

Repack::~Repack(){

}

void Repack::generateNeighbors(Solution & sol){
  for (std::vector< Change* >::iterator it = this->neighbors.begin() ; it != this->neighbors.end(); ++it){
    delete (*it);
  }

  this->neighbors.clear();


  this->max = sol;
  for(int a1 = 0; a1 < this->ins->A - 1; a1++){
    if(!sol.y[a1]) continue;
    Ad ad1 = this->ins->ads[a1];

    for(int a2 = a1+1; a2 < this->ins->A; a2++){
      if(!sol.y[a2] || a1 == a2) continue;
      Ad ad2 = this->ins->ads[a2];

      for(int j = 0; j < sol.c[a1]; j++){
        int s1 = sol.w[a1][j];
        if(sol.slots[s1].ads[a2] || ad2.dl < s1 || s1 < ad2.rd) continue;

        for(int k = 0; k < sol.c[a2]; k++){
          int s2 = sol.w[a2][k];
          if(sol.slots[s2].ads[a1] || ad1.dl < s2 || s2 < ad1.rd) continue;

          if(s1 != s2 && sol.canRepack(this->ins->ads[a1], this->ins->ads[a2], j, k)){
            Change *c = new RepackChange(this->ins, a1, a2, j, k);
            this->neighbors.push_back(c);
            Solution sol2 = c->applyChange(sol);
            if(this->max < sol2){
              this->max = sol2;
            }
          }
        }
      }
    }
  }
}

void Repack::bestImprovement(Solution & sol){
  for(int a1 = 0; a1 < this->ins->A - 1; a1++){
    if(!sol.y[a1]) continue;
    Ad ad1 = this->ins->ads[a1];

    for(int a2 = a1+1; a2 < this->ins->A; a2++){
      if(!sol.y[a2] || a1 == a2) continue;
      Ad ad2 = this->ins->ads[a2];

      for(int j = 0, s = sol.c[a1]; j < s; j++){
        int s1 = sol.w[a1][j];
        if(sol.slots[s1].ads[a2] || ad2.dl < s1 || s1 < ad2.rd) continue;

        for(int k = 0, _s = sol.c[a2]; k < _s; k++){
          int s2 = sol.w[a2][k];
          if(sol.slots[s2].ads[a1] || ad1.dl < s2 || s2 < ad1.rd) continue;
          
          if(s1 != s2 && sol.canRepack(this->ins->ads[a1], this->ins->ads[a2], j, k)){
            sol.repack(this->ins->ads[a1], this->ins->ads[a2], j, k);          
            s1 = s2;
            if(sol.slots[s1].ads[a2] || ad2.dl < s1 || s1 < ad2.rd) break;
          }
        }
      }
    }
  }
}