#ifndef REPACK_CHANGE_H
#define REPACK_CHANGE_H

#include "Change.h"

class RepackChange : public Change{
private:
    virtual bool isEqual(const Change& other) const{
        RepackChange rc = static_cast<const RepackChange&>(other);
        return ad1 == rc.ad1 && ad2 == rc.ad2 && c1 == rc.c1 && c2 == rc.c2;
    }
public:
    int ad1, ad2, c1, c2;
    RepackChange();
    RepackChange(Instance* ins, int ad1, int ad2, int c1, int c2);
    ~RepackChange() override;
    Solution applyChange(const Solution & sol) override;
    int getSpace();
    virtual Change* clone() const override;

    RepackChange& operator=(const RepackChange& other){
        Change::operator =(other);
        ad1 = other.ad1;
        ad2 = other.ad2;
        c1 = other.c1;
        c2 = other.c2;
        return *this;
    }
};

#endif