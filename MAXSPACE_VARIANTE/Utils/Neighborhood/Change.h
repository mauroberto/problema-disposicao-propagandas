#ifndef CHANGE_H
#define CHANGE_H

#include "Ad.h"
#include "Slot.h"
#include "Solution.h"
#include "Instance.h"

class Change{
private:
  virtual bool isEqual(const Change& other) const{
    return false;
  };
public:
  Instance* ins;
  Change();
  Change(Instance* ins);
  virtual ~Change();
  virtual Solution applyChange(const Solution & sol);
  virtual Change* clone() const {
    return NULL;
  };


  bool operator==(const Change& other) const{
    return typeid(*this) == typeid(other) && isEqual(other);
  }

  Change& operator=(const Change& other){
    ins = other.ins;
    return *this;
  }
};

#endif