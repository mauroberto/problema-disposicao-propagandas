#include "ChangeItem.h"
#include <iostream>
#include <algorithm>

using namespace std;

ChangeItem::ChangeItem(){

}

ChangeItem::ChangeItem(Instance* instance) : Neighborhood(instance){
  this->ins = instance;
}

ChangeItem::~ChangeItem(){
}

void ChangeItem::generateNeighbors(Solution & sol){
  vector<int> candidate_list;
  vector<int> candidate_list2;

  for (std::vector< Change* >::iterator it = this->neighbors.begin() ; it != this->neighbors.end(); ++it){
    delete (*it);
  }
  
  this->neighbors.clear();

  int s1 = 0, s2 = 0;

  for(int i=0; i<this->ins->A; i++){
    if(sol.y[i]){
      candidate_list.push_back(i);
      s1++;
    }else{
      candidate_list2.push_back(i);
      s2++;
    }
  }

  this->max = sol;
  for(int i=0; i<s1; i++){
    for(int j=0; j<s2; j++){
      Solution sol2 = sol;
      if(sol2.changeAd(this->ins->ads[candidate_list2[j]], this->ins->ads[candidate_list[i]])){
        Change *c = new ChangeItemChange(this->ins, candidate_list2[j], candidate_list[i]);
        this->neighbors.push_back(c);
        Solution sol2 = c->applyChange(sol);
        if(max < sol2){
          this->max = sol2;
        }
      }
    }
  }
}

void ChangeItem::bestImprovement(Solution & sol){
  Solution max(sol);

  do{
    Solution sol2(max);
    Solution aux(sol2);
    sol = max;
    for(int k = 0; k < this->ins->A; k++){
      if(sol2.y[k]) continue;

      for(int l = k + 1; l < this->ins->A; l++){
        if(!sol2.y[l]) continue;

        bool troca = sol2.changeAd(this->ins->ads[k], this->ins->ads[l]);

        if(troca && sol2.value > max.value){
          max = sol2;
          sol2 = aux;
        }

        if(troca){
          break;
        }
      }
    }
  } while(max.value > sol.value);

  sol = max;
}