#ifndef CHANGE_ITEM_H
#define CHANGE_ITEM_H

#include "Neighborhood.h"
#include "ChangeItemChange.h"
#include <vector>

class ChangeItem : public Neighborhood{
public:
  ChangeItem();
  ChangeItem(Instance* instance);
  ~ChangeItem() override;

  void maxNeighborhood(Solution & sol);
  bool randomNeighborhood(Solution & sol);
  void generateNeighbors(Solution & sol) override;
  void bestImprovement(Solution & sol) override;
};

#endif