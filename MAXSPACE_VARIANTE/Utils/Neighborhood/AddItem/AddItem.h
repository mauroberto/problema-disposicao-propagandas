#ifndef ADD_ITEM_H
#define ADD_ITEM_H 

#include "Neighborhood.h"
#include "AddItemChange.h"
#include <vector>

class AddItem : public Neighborhood{
public:
  AddItem();
  AddItem(Instance* instance);
  ~AddItem() override;

  void maxNeighborhood(Solution & sol);
  bool randomNeighborhood(Solution & sol);
  void generateNeighbors(Solution & sol) override;
  void bestImprovement(Solution & sol) override;
};

#endif
