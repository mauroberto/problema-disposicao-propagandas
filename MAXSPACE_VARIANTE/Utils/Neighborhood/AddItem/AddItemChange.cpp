#include "AddItemChange.h"
#include <iostream>

AddItemChange::AddItemChange(){}


AddItemChange::AddItemChange(Instance* ins, int add) : Change(ins){
    this->ins = ins;
    this->add = add;
}


AddItemChange::~AddItemChange(){}


Solution AddItemChange::applyChange(const Solution & sol){
    Solution sol2(sol);
    sol2.addFirstFit(this->ins->ads[this->add]);
    Solution::callAdd--;
    return sol2;
}

Change* AddItemChange::clone() const{
    return new AddItemChange(this->ins, this->add);
}
