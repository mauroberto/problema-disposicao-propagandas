#include "AddCopy.h"
#include <iostream>
#include <algorithm>

AddCopy::AddCopy(){

}

AddCopy::AddCopy(Instance* instance) : Neighborhood(instance){
  this->ins = instance;
}

AddCopy::~AddCopy(){
}

void AddCopy::generateNeighbors(Solution & sol){
  for (std::vector< Change* >::iterator it = this->neighbors.begin() ; it != this->neighbors.end(); ++it){
    delete (*it);
  }
  
  this->neighbors.clear();

  this->max = sol;
  for(int i=0; i<this->ins->A; i++){
    if(!sol.y[i] || sol.c[i] >= this->ins->ads[i].wMax) continue;

    for(int j=this->ins->ads[i].rd; j < this->ins->ads[i].dl; j++){
        if(sol.slots[j].ads[i]) continue;

        if(sol.slots[j].s + this->ins->ads[i].s <= this->ins->S){
            Change *c = new AddCopyChange(this->ins, i, j);
            this->neighbors.push_back(c);
            Solution sol2 = c->applyChange(sol);
            if(max < sol2){
              this->max = sol2;
            }
        }
    }

  }
}

void AddCopy::bestImprovement(Solution & sol){
  for(int k = 0; k < this->ins->A; k++){
    int i = this->ins->ranking[k].second;
    if(sol.y[i]){
      for(int j=this->ins->ads[i].rd; j <= this->ins->ads[i].dl && sol.c[i] < this->ins->ads[i].wMax; j++){
        if(!sol.slots[j].ads[i] && sol.slots[j].s + this->ins->ads[i].s <= this->ins->S){
          sol.addCopy(this->ins->ads[i], j, sol.c[i]);
        }
      }
    }
  }
}