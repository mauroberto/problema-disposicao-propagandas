#ifndef ADD_COPY_CHANGE_H
#define ADD_COPY_CHANGE_H

#include "Change.h"

class AddCopyChange : public Change{
private:
    int add, slot;

    virtual bool isEqual(const Change& other) const{
        AddCopyChange acc = static_cast<const AddCopyChange&>(other);
        return add == acc.add && slot == acc.slot;
    }
public:
    AddCopyChange();
    AddCopyChange(Instance* ins, int add, int slot);
    ~AddCopyChange() override;
    Solution applyChange(const Solution & sol) override;
    virtual Change* clone() const override;

    virtual AddCopyChange& operator=(const AddCopyChange& other){
        Change::operator =(other);
        add = other.add;
        return *this;
    }
};

#endif