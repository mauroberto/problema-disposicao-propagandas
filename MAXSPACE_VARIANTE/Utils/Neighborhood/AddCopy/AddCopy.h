#ifndef ADD_COPY_H
#define ADD_COPY_H 

#include "Neighborhood.h"
#include "AddCopyChange.h"
#include <vector>

class AddCopy : public Neighborhood{
public:
  AddCopy();
  AddCopy(Instance* ins);
  ~AddCopy() override;

  void maxNeighborhood(Solution & sol);
  bool randomNeighborhood(Solution & sol);
  void generateNeighbors(Solution & sol) override;
  void bestImprovement(Solution & sol) override;
};

#endif
