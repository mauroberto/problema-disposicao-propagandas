#include "AddCopyChange.h"
#include <iostream>

AddCopyChange::AddCopyChange(){}


AddCopyChange::AddCopyChange(Instance* ins, int add, int slot) : Change(ins){
    this->ins = ins;
    this->add = add;
    this->slot = slot;
}


AddCopyChange::~AddCopyChange(){}


Solution AddCopyChange::applyChange(const Solution & sol){
    Solution sol2(sol);
    sol2.addCopy(this->ins->ads[this->add], this->slot, sol2.c[this->add]);
    return sol2;
}

Change* AddCopyChange::clone() const{
    return new AddCopyChange(this->ins, this->add, this->slot);
}