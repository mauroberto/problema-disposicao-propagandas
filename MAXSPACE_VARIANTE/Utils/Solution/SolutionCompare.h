#ifndef SOLUTION_COMPARE_H
#define SOLUTION_COMPARE_H

#include "Ad.h"

class SolutionCompare {
public:
    SolutionCompare();
    virtual ~SolutionCompare();
    virtual bool compare(void* sol, void* sol2);
    virtual bool canRepack(void* sol, Ad & a1, Ad & a2, int c1, int c2);
    virtual bool canMove(void* sol, Ad & a1, int s1, int s2);
    virtual SolutionCompare* clone();
};

#endif