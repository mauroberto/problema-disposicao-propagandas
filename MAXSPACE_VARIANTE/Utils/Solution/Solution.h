#ifndef SOLUTION_H
#define SOLUTION_H
#include "Slot.h"
#include "BITree.h"
#include <vector>
#include <string>
#include "Instance.h"
#include "SolutionCompare.h"
#include "MaximizeResidue.h"

class Solution {
public:
  int A, S, N;
  double value;
  std::vector<Slot> slots;
  std::vector<bool> y;
  std::vector <int> c;
  std::vector<std::vector<int> > w;
  SolutionCompare * compare;
  BITree bt;

  static long long int callChange, callAdd, callChangeBIT, callAddBIT;

  Solution();
  Solution(SolutionCompare* compare);
  Solution(int N, int A, int S, Instance * ins);
  Solution(int N, int A, int S, Instance * ins, SolutionCompare* compare);
  Solution(const Solution& sol2);
  void setCompare(SolutionCompare* compare);
  bool addBestFit(Ad & ad);
  bool addWorstFit(Ad & ad);
  void addCopy(Ad & ad, int j, int copy);
  bool addFirstFit(Ad & ad);
  bool changeAd(Ad & add, Ad & rm);
  bool canRepack(Ad & a1, Ad & a2, int s1, int s2);
  bool canMove(Ad & a1, int s1, int s2);
  void repack(Ad & a1, Ad & a2, int c1, int c2);
  void move(Ad & a1, int c1, int s2);
  void removeCopy(Ad & ad, int j, int copy);
  void remakeSol();
  long int spaceSolution();
  void exportToFile(const std::string path);
	~Solution();

  Solution& operator= (const Solution& sol2);
  bool operator< (Solution& sol2);

private:
  void sortRemove(int j);
  void sortAdd(int j);
};

#endif
