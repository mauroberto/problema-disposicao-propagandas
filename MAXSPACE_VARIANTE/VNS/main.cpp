#include "Ad.h"
#include "Slot.h"
#include "Solution.h"
#include "Instance.h"
#include "ConstructiveHeuristic.h"
#include <iostream>
#include <time.h>
#include "VNS.h"
#include <string>

using namespace std;

int timeout;
double elapsed;
clock_t clk;
struct timeval start, stop;

int main(int argc, char* argv[]){
  if (argc < 2) {
    cout << "Usage: ./VNS filename [timeout Kmax alpha MV RPCK ADDCPY ADD CHG]" << endl;
    return 1;
  }

  gettimeofday(&start, NULL);

  int K = 9, N_MAX = 5;

  clk = clock();
  timeout = 3600;
  cout.precision(2);

  if(argc > 2){
	  timeout = stoi(argv[2]);
  }

  if(argc > 3){
    K = stoi(argv[3], NULL);  
  }

  double alpha = 0;
  if(argc > 4){
    alpha = strtod(argv[4], NULL);
  }

  Instance ins(argv[1]);
  ConstructiveHeuristic ch(ins);

  vector<Neighborhood*> N;
  vector<Neighborhood*> N2;

  N.push_back(new Move(&ins));
  N.push_back(new Repack(&ins));
  N.push_back(new AddCopy(&ins));
  N.push_back(new AddItem(&ins));
  N.push_back(new ChangeItem(&ins));

  N2.push_back(new Move(&ins));
  N2.push_back(new Repack(&ins));
  N2.push_back(new AddCopy(&ins));
  N2.push_back(new AddItem(&ins));
  N2.push_back(new ChangeItem(&ins));

  for(int init = 5; init < 5 + N_MAX; init++){
    int flag;
    if(argc > init){
      flag = strtod(argv[init], NULL);
      if(!flag){
        delete N[init-5];
        delete N2[init-5];
        N[init-5] = NULL;
        N2[init-5] = NULL;
      }
    } else {
      break;
    }
  }

  N.erase(std::remove_if(N.begin(), N.end(), [](Neighborhood* n) { return n == NULL; }), N.end());
  N2.erase(std::remove_if(N2.begin(), N2.end(), [](Neighborhood* n) { return n == NULL; }), N2.end());

  N_MAX = N.size();

  Solution sol = ch.greedyRandomizedConstruction3(alpha);

  VNS vns(&ins, K, N_MAX, N, N2, timeout, start);

  vns.timeToTarget = ((double) (clock() - clk)) / CLOCKS_PER_SEC;

  sol.remakeSol();

  vns.runVNSDualPhases(sol);

  sol.exportToFile("MAXSPACE_VARIANTE/VNS/solutions/"+ins.instanceName+".sol");

  cout << "Calls to change | Calls removed by BIT " << endl;
  cout << sol.callChange << " | " << sol.callChangeBIT << endl;

  cout << "Calls to add | Calls removed by BIT " << endl;
  cout << sol.callAdd << " | " << sol.callAddBIT << endl;

  cout << endl << "Time used by neighbor in VND | Times each neighbor was used in VND | Improvements" <<  endl;

  for(int i = 0; i < N_MAX; i++){
    cout << fixed << "N_" << i+1 << " = " << vns.tempoVND[i] << " | " << vns.qtdVND[i] << " | " << vns.melhoriasVND[i] << endl;
  }

  cout << endl << "Time used by local search = " << vns.tempoLS << endl;

  cout << "Time used by neighbor | Times each neighbor was used | Improvements" << endl;

  for(int i = 0; i < N_MAX; i++){
    cout << fixed << "N_" << i+1 << " = " << vns.tempo[i] << " | " << vns.qtd[i] << " | " << vns.melhorias[i] << endl;
  }

  cout << endl << "Time to find the best solution = " << vns.timeToTarget << endl;

  cout << fixed << "Solution = " << sol.value << endl;
  elapsed = ((double) (clock() - clk)) / CLOCKS_PER_SEC;
  cout << fixed << "Time = " << elapsed << endl;

  for(int i = 0; i < N_MAX; i++){
    delete N[i];
    delete N2[i];
  }
  return 0;
}
