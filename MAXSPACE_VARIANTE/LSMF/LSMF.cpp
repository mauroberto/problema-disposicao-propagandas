#include "LSMF.h"

#ifndef PATHSOL
#define PATHSOL "MAXSPACE_VARIANTE/LSMF/solutions"
#endif

using namespace std;

vector<bool> valids;

std::string getFileName(const std::string& s) {

   char sep = '/';

	 #ifdef _WIN32
   	sep = '\\';
	 #endif

   std::string aux;

   size_t i = s.rfind(sep, s.length());
   if (i != std::string::npos) {
      aux = s.substr(i+1, s.length() - i);
      i = aux.rfind('.', aux.length());
      if (i != std::string::npos){
        return (aux.substr(0, i));
      }
   }

   return("");
}

LSMF::LSMF() {}

LSMF::LSMF(char * fn) {
  ifstream infile(fn);

  ins.instanceName = getFileName(fn);

  infile >> ins.A >> ins.N >> ins.S;

  int id, s, wMax, wMin, rd, dl;
  double v;

  while(infile >> id >> s >> rd >> dl >> wMin >> wMax >> v){
    Ad ad;
    ad.id = id;
    ad.s = s;
    ad.wMax = wMax;
    ad.wMin = wMin;
    ad.rd = rd;
    ad.dl = dl;
    ad.v = v;

    ins.ads.push_back(ad);
  }

  valids.resize(ins.A, true);
}

LSMF::~LSMF() { }

inline void ordenaAdiciona(vector<slot> & slots, int j){
  int N = slots.size();

  if(j <= 0 || j > N-1) return;

  Slot aux = slots[j];

  int i = j-1;
  while(i >= 0){
    if(slots[i].s < aux.s){
      slots[i+1] = slots[i];
      i--;
    }else{
      slots[i+1] = aux;
      break;
    }
  }

  if(i < 0){
    slots[0] = aux;
  }
}

void LSMF::firstFit(Sol & sol, Ad & ad, int C){
  int rd = max(ad.rd, 0);
  int dl = min(ad.dl, ins.N-1);

  int count = 0;
  for(int j = rd; j <= dl; j++){
    if(count >= ad.wMin){
      break;
    }

    if(sol.slots[j].s + ad.s <= C){
      count++;
    }
  }

  if(count >= ad.wMin){
    sol.yi[ad.id] = true;
    for(int j = rd, c = 0; j <= dl && c < ad.wMax; j++){
      if(sol.slots[j].s + ad.s <= C){
        sol.slots[j].s += ad.s;
        sol.value += ad.v;
        sol.slots[j].ads.push_back(ad.id);
        c++;
      }
    }
  }
}

pair<Sol,int> LSMF::FFD(int C, vector<int> ranking){
  Sol sol;
  sol.value = 0.0;
  sol.slots.resize(ins.N);
  sol.yi.resize(ins.A, false);
  for(int j=0; j<ins.N; j++){
    sol.slots[j].s = 0;
    sol.slots[j].id = j;
  }

  for(int i=0; i<ins.A; i++){
    int count = 0;
    int index = ranking[i];
    if(!valids[index]) continue;
    firstFit(sol, ins.ads[index], C);
  }

  int count = 0;
  for(int j=0; j<ins.N; j++){
    if(sol.slots[j].s > 0){
      count++;
    }
  }

  return make_pair(sol, count);
}

pair<Sol, int> LSMF::SUBSET_LSMF(){
  vector<pair<pair<int, int>, int > > ranking(ins.A);
  vector<pair<int, int> > ranking3(ins.A);
  vector<int> ranking2(ins.A);
  vector<int> CL(20);
  vector<int> CU(20);

  int ssum = 0;
  int smax = 0;
  for(int i=0; i<ins.A; i++){
    if(!valids[i]) continue;

    if(ins.ads[i].s > smax){
      smax = ins.ads[i].s;
    }

    ssum += ins.ads[i].s;

    ranking[i] = make_pair(make_pair(ins.ads[i].s, ins.ads[i].wMin), i);
    ranking3[i] = make_pair(ins.ads[i].s, i);
  }

  ssum /= ins.N;

  int C_l = max(ssum, smax);
  int C_u = C_l * 2;

  int I = 0, K = 9;

  CL[I] = C_l;
  CU[I] = C_u;

  sort(ranking.begin(), ranking.end(), greater<pair<pair<int, int>, int > >());
  for(int i=0; i<ins.A; i++){
    ranking2[i] = ranking[i].second;
  }

  STEP7:
  int C = (CL[I] + CU[I])/2;

  pair<Sol, int> ffd = FFD(C, ranking2);

  I++;
  if(ffd.second <= ins.N){
    CU[I] = C;
    CL[I] = CL[I-1];
    if(I <= K){
      goto STEP7;
    }
  }else{
    CU[I] = CU[I-1];
    CL[I] = C;
    goto STEP7;
  }

  pair<Sol, int> A = make_pair(ffd.first, C);

  I = 0, K = 9;

  CL[I] = C_l;
  CU[I] = C_u;

  sort(ranking3.begin(), ranking3.end(), greater<pair<int, int> >());
  for(int i=0; i<ins.A; i++){
    ranking2[i] = ranking3[i].second;
  }

  STEP3:
  C = (CL[I] + CU[I])/2;

  ffd = FFD(C, ranking2);

  I++;
  if(ffd.second <= ins.N){
    CU[I] = C;
    CL[I] = CL[I-1];
    if(I <= K){
      goto STEP3;
    }
  }else{
    CU[I] = CU[I-1];
    CL[I] = C;
    goto STEP3;
  }

  pair<Sol, int> B = make_pair(ffd.first, C);

  if(A.second < B.second) return A;

  return B;
}

Sol LSMF::runLSMF(vector<int> ranking){
  vector<int> discard(ins.A);

  pair<Sol, int> ini = SUBSET_LSMF();
  pair<Sol, int> ini2;


  if(ini.second <= ins.S){
    return ini.first;
  }

  for(int i=0; i<ins.A; i++){
    valids[i] = ini.first.yi[i];
  }

  int K = 0;
  int i = 0;

  do{
    int index = ranking[i++];
    valids[index] = false;
    discard[K] = index;
    ini = SUBSET_LSMF();
    if(ini.second <= ins.S) {
      goto STEP9;
    }
    K++;
  } while(1);

  STEP7:
  valids[discard[K-1]] = false;

  STEP8:
  K--;

  STEP9:
  if(K == 0){
    return ini.first;
  }

  valids[discard[K-1]] = true;
  ini2 = SUBSET_LSMF();
  if(ini2.second > ins.S) {
    goto STEP7;
  }else{
    ini = ini2;
    goto STEP8;
  }
}

void LSMF::genFileSol(Sol & sol){

  double result = sol.value;


  char* fn = new char[256];

  sprintf(fn, "%s/%s.sol", PATHSOL, ins.instanceName.c_str());

  ofstream opf;
  opf.open(fn);

  vector<Slot> slots(ins.N);

  for(int j=0; j < ins.N; j++){
    slots[sol.slots[j].id] = sol.slots[j];
  }

  opf << fixed << result << endl;
  for(int j = 0; j < ins.N; j++){
    opf << j << " ";
    int s = slots[j].ads.size();
    for(int i = 0; i < s; i++){
      opf << slots[j].ads[i] << " ";
    }
    opf << endl;
  }

  opf.close();

  delete[] fn;
}
