#ifndef LSMF_H
#define LSMF_H

#include <iostream>
#include <vector>
#include <algorithm>
#include <fstream>
#include <string>

typedef struct ad{
  int id;
  int s;
  int wMax;
  int wMin;
  int rd;
  int dl;
  double v;
} Ad;

typedef struct slot{
  int s;
  int id;
  std::vector<int> ads;
} Slot;

typedef struct sol{
  std::vector<bool> yi;
  double value;
  std::vector <Slot> slots;
} Sol;

typedef struct instance{
	int A;
	int N;
	int S;
	std::vector<Ad> ads;
  std::string instanceName;
} Instance;

class LSMF {
public:
	Instance ins;
  LSMF();
	LSMF(char * fn);
	~LSMF();

  Sol runLSMF(std::vector<int> ranking);
  void genFileSol(Sol & sol);
private:
  std::pair<Sol,int> FFD(int C, std::vector<int> ranking);
  std::pair<Sol,int> SUBSET_LSMF();
  void firstFit(Sol & sol, Ad & ad, int C);
};

#endif
