#include <stdio.h>
#include "LSMF.h"
#include <time.h>
#include <vector>

using namespace std;

int timeout;
double elapsed;
clock_t clk;

int main(int argc, char* argv[]){
  if (argc < 2) {
    cout << "Usage: ./LSMF filename" << endl;
    return 1;
  }

  clk = clock();
	timeout = 3600;
	cout.precision(2);

  LSMF lsmf = LSMF(argv[1]);

  Instance ins = lsmf.ins;
  vector<pair<int, int> > ranking(ins.A);
  vector<int> ranking2(ins.A);

  for(int i=0; i<ins.A; i++){
    ranking[i].first = ins.ads[i].s * ins.ads[i].wMin;
    ranking[i].second = i;
  }

  sort(ranking.begin(), ranking.end());

  for(int i=0; i<ins.A; i++){
    ranking2[i] = ranking[i].second;
  }

  Sol sol = lsmf.runLSMF(ranking2);

  cout << fixed << "Solution = " << sol.value << endl;
  elapsed = ((double) (clock() - clk)) / CLOCKS_PER_SEC;
  cout << fixed << "Time = " << elapsed << endl;

  lsmf.genFileSol(sol);
  return 0;
}
