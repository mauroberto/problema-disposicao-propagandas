#include <stdio.h>
#include <cstdlib>
#include <ctime>
#include "TabuSearch.h"
#include "Instance.h"
#include "Solution.h"
#include "ConstructiveHeuristic.h"
#include "SolutionCompare.h"
#include "MaximizeResidue.h"
#include <time.h>
#include <sys/time.h>
#include <omp.h>
#include "Strategy.h"

using namespace std;

#ifndef PATHSOL
#define PATHSOL "solutions"
#endif

#ifdef _OPENMP
#define MAXT 4
#else
#define MAXT 1
#endif

Instance ins;

int timeout;
struct timeval start, stop;

int main(int argc, char* argv[]){
  if (argc < 2) {
    cout << "Usage: ./GRASP-Tabu filename [timeout #it alpha tabuSize #tabuSearchIt tabuType MV RPCK ADDCPY ADD CHG]" << endl;
    return 1;
  }

  gettimeofday(&start, NULL);

  int N_MAX = 5, type = 0;

  timeout = 3600;
  if(argc > 2){
	  timeout = stoi(argv[2]);
  }

	cout.precision(2);

  srand((int)time(0));

  int tabuSize = 90, tbit = 125;

  if(argc > 5){
    tabuSize = stoi(argv[5]);
  }

  if(argc > 6){
    tbit = stoi(argv[6]);
  }

  if(argc > 7){
    type = stoi(argv[7]);
  }

  Instance ins(argv[1]);
  
  vector<Neighborhood*> N;

  N.push_back(new Move(&ins));
  N.push_back(new Repack(&ins));
  N.push_back(new AddCopy(&ins));
  N.push_back(new AddItem(&ins));
  N.push_back(new ChangeItem(&ins));

  for(int init = 8; init < 8 + N_MAX; init++){
    int flag;
    if(argc > init){
      flag = strtod(argv[init], NULL);
      if(!flag){
        delete N[init-8];
        N[init-8] = NULL;
      }
    } else {
      break;
    }
  }

  N.erase(std::remove_if(N.begin(), N.end(), [](Neighborhood* n) { return n == NULL; }), N.end());

  N_MAX = N.size();

  Strategy* strategy;
  switch(type){
    case 0:
      strategy = new Randomly(tbit, N_MAX);
      break;
    case 1:
      strategy = new Cycling(tbit, N_MAX);
      break;
    default:
      strategy = new MaximumImprovement(tbit, N_MAX);
      break;
  }

  TabuSearch ts(&ins, tabuSize, tbit, timeout, start, strategy, N_MAX, N);
  ConstructiveHeuristic ch(ins);
  SolutionCompare * compare = new MaximizeResidue();
  SolutionCompare * compare2 = new SolutionCompare();

  int it = 1100;
  if(argc > 3){
    it = stoi(argv[3]);
  }
  double alpha = 0.2;
  if(argc > 4){
    alpha = strtod(argv[4], NULL);
  }

  Solution bestSol[MAXT];

  #ifdef _OPENMP
		#pragma omp parallel for num_threads(MAXT)
	#endif
  for(int i = 0; i < MAXT; i++){
    bestSol[i] = Solution(ins.N, ins.A, ins.S, &ins);
  }

  struct timeval stop;
  gettimeofday(&stop, NULL);

  double timeToTarget = ((double) (stop.tv_sec * 1000000 + stop.tv_usec) - (start.tv_sec * 1000000 + start.tv_usec)) / 1000000;

  #ifdef _OPENMP
    #pragma omp parallel for num_threads(MAXT)
  #endif
  int i = 0, bestI = 0;
  for(i = 0; i < it; i++){
    gettimeofday(&stop, NULL);
    double elapsed = ((double) (stop.tv_sec * 1000000 + stop.tv_usec) - (start.tv_sec * 1000000 + start.tv_usec)) / 1000000;
    if(elapsed < timeout){
      #ifdef _OPENMP
        int tid = omp_get_thread_num();
      #else
        int tid = 0;
      #endif
      Solution sol = ch.greedyRandomizedConstruction3(alpha);

      double v;
      do{
        ts.runTabuSearch(sol);

        v = sol.value;

        sol.setCompare(compare);

        ts.runTabuSearch(sol);

        sol.setCompare(compare2);
      } while (sol.value > v);

      cout << fixed << "iteration " << i << " sol " << sol.value << endl;

      if(sol.value > bestSol[tid].value){
        bestSol[tid] = sol;
        gettimeofday(&stop, NULL);
        timeToTarget = ((double) (stop.tv_sec * 1000000 + stop.tv_usec) - (start.tv_sec * 1000000 + start.tv_usec)) / 1000000;
        bestI = i;
      }
    } else {
      break;
    }
  }

  Solution sol = bestSol[0];

  for(int i = 1; i < MAXT; i++){
    if(bestSol[i].value > sol.value){
      sol = bestSol[i];
    }
  }

  cout << "Calls to change | Calls removed by BIT " << endl;
  cout << Solution::callChange << " | " << Solution::callChangeBIT << endl;

  cout << "Calls to add | Calls removed by BIT " << endl;
  cout << Solution::callAdd << " | " << Solution::callAddBIT << endl;

  cout << "Number of iterations = " << i << endl;
  cout << "Best Solution was found in iteration = " << bestI << endl;

  cout << "Time used by neighbor | Times each neighbor was used | Improvements" << endl;

  for(int i = 0; i < N_MAX; i++){
    cout << fixed << "N_" << i+1 << " = " << ts.tempo[i] << " | " << ts.qtd[i] << " | " << ts.melhorias[i] << endl;
  }

  cout << fixed << "Time to find the best solution = " << timeToTarget << endl;

  gettimeofday(&stop, NULL);
  cout << fixed << "Solution = " << sol.value << endl;
  double elapsed = ((double) (stop.tv_sec * 1000000 + stop.tv_usec) - (start.tv_sec * 1000000 + start.tv_usec)) / 1000000;
  cout << fixed << "Time = " << elapsed << endl;

  sol.exportToFile("MAXSPACE_VARIANTE/GRASP-Tabu/solutions/"+ins.instanceName+".sol");

  for(int i = 0; i < N_MAX; i++){
    delete N[i];
  }

  delete strategy;
  delete compare;
  delete compare2;

  return 0;
}
