/*
 * SampleDecoder.h
 *
 * Any decoder must have the format below, i.e., implement the method decode(std::vector< double >&)
 * returning a double corresponding to the fitness of that vector. If parallel decoding is to be
 * used in the BRKGA framework, then the decode() method _must_ be thread-safe; the best way to
 * guarantee this is by adding 'const' to the end of decode() so that the property will be checked
 * at compile time.
 *
 * The chromosome inside the BRKGA framework can be changed if desired. To do so, just use the
 * first signature of decode() which allows for modification. Please use double values in the
 * interval [0,1) when updating, thus obeying the BRKGA guidelines.
 *
 *  Created on: Jan 14, 2011
 *      Author: rtoso
 */

#ifndef SAMPLEDECODER_H
#define SAMPLEDECODER_H

#include <iostream>
#include <list>
#include <vector>
#include <algorithm>
#include <fstream>
#include <string>
#include "bitmap.h"

typedef struct ad{
  int id;
  int s;
  int wMax;
  int wMin;
  int rd;
  int dl;
  double v;
  bool operator<(const ad& ad) const {
    if(v/s < ad.v/ad.s){
      return true;
    }else if(v/s > ad.v/ad.s){
      return false;
    }else{
      return s < ad.s;
    }
  }
} Ad;

typedef struct slot{
  int s;
  int id;
  std::vector <int> ads;
} Slot;

typedef struct sol{
  double value;
  std::vector <Slot> slots;
} Sol;

typedef struct instance{
	int A;
	int N;
	int S;
	std::vector<Ad> ads;
  std::string instanceName;
} Instance;

class SampleDecoder {
public:
	Instance ins;
  int N;
	SampleDecoder(char * fn);
	~SampleDecoder();

	double decode(const std::vector< word >& chromosome) const;
	void genFileSol(const std::vector< word >& chromosome);
  void genFileEvol(const std::vector< double >& solutions);
private:
  Sol createSolution(const std::vector< word >& chromosome) const;
  void lastFit(Sol & sol, Ad ad, const std::vector< word >& chromosome, int count) const;
  void firstFit(Sol & sol, Ad ad, const std::vector< word >& chromosome, int count) const;
  void midFit(Sol & sol, Ad ad, const std::vector< word >& chromosome, int count) const;
  void bestFit(Sol & sol, Ad ad, const std::vector< word >& chromosome, int count) const;
  void worstFit(Sol & sol, Ad ad, const std::vector< word >& chromosome, int count) const;
};

#endif
