/*
 * SampleDecoder.cpp
 *
 *  Created on: Jan 14, 2011
 *      Author: rtoso
 */

#include "SampleDecoder.h"

#ifndef PATHSOL
#define PATHSOL "MAXSPACE_VARIANTE/brkgaAPI-TAQ/solutions"
#endif

#ifndef PATHEVOL
#define PATHEVOL "MAXSPACE_VARIANTE/brkgaAPI-TAQ/solutions_evol"
#endif

using namespace std;

int max(int a, int b){
  return a > b ? a : b;
}

int min(int a, int b){
  return a < b ? a : b;
}

std::string getFileName(const std::string& s) {

   char sep = '/';

	 #ifdef _WIN32
   	sep = '\\';
	 #endif

   std::string aux;

   size_t i = s.rfind(sep, s.length());
   if (i != std::string::npos) {
      aux = s.substr(i+1, s.length() - i);
      i = aux.rfind('.', aux.length());
      if (i != std::string::npos){
        return (aux.substr(0, i));
      }
   }

   return("");
}

SampleDecoder::SampleDecoder(char * fn) {
  N = 0;
  ifstream infile(fn);

  ins.instanceName = getFileName(fn);

  infile >> ins.A >> ins.N >> ins.S;

  int id, s, wMax, wMin, rd, dl;
  double v;

  while(infile >> id >> s >> rd >> dl >> wMin >> wMax >> v){
    Ad ad;
    ad.id = id;
    ad.s = s;
    ad.wMax = wMax;
    ad.wMin = wMin;
    ad.rd = rd;
    ad.dl = dl;
    ad.v = v;

    ins.ads.push_back(ad);
  }

  for(int i=0; i<ins.A; i++){
		N += ins.ads[i].dl - ins.ads[i].rd + 1;
	}
  std::sort(ins.ads.begin(), ins.ads.end());
}

SampleDecoder::~SampleDecoder() { }

void SampleDecoder::bestFit(Sol & sol, Ad ad, const std::vector< word >& chromosome, int count) const{
  int rd = max(ad.rd, 0);
  int dl = min(ad.dl, ins.N-1);

  int c = 0;
  for(int j=0; j < ins.N; j++){
    if(sol.slots[j].id <= dl && sol.slots[j].id >= rd && test(chromosome, count+sol.slots[j].id-rd) && sol.slots[ad.rd + j].s + ad.s <= ins.S){
      c++;
    }
  }

  if(c >= ad.wMin){
    for(int j = 0, c2 = 0; j < ins.N && c2 < ad.wMax; j++){
      if(sol.slots[j].id <= dl && sol.slots[j].id >= rd && sol.slots[j].s + ad.s <= ins.S && test(chromosome, count+sol.slots[j].id-rd)){
        sol.slots[j].s += ad.s;
        sol.value += ad.v;
        sol.slots[j].ads.push_back(ad.id);
        c2++;

        if(j == 0) continue;

        int i = j-1;
        while(i >= 0){
          if(sol.slots[i].s < sol.slots[j].s){
            i--;
          } else if(i != j-1){
            Slot aux = sol.slots[i+1];
            sol.slots[i+1] = sol.slots[j];
            sol.slots[j] = aux;
            break;
          }else{
            break;
          }
        }

        if(i < 0){
          Slot aux = sol.slots[0];
          sol.slots[0] = sol.slots[j];
          sol.slots[j] = aux;
        }
      }
    }
  }
}

void SampleDecoder::worstFit(Sol & sol, Ad ad, const std::vector< word >& chromosome, int count) const{
  int rd = max(ad.rd, 0);
  int dl = min(ad.dl, ins.N-1);

  int c = 0;
  for(int j=0; j < ins.N; j++){
    if(sol.slots[j].id <= dl && sol.slots[j].id >= rd && test(chromosome, count+sol.slots[j].id-rd) && sol.slots[ad.rd + j].s + ad.s <= ins.S){
      c++;
    }
  }

  bool added[ins.N];

  if(c >= ad.wMin){
    for(int j = 0, c2 = 0; j < ins.N && c2 < ad.wMax; j++){
      if(sol.slots[j].id <= dl && sol.slots[j].id >= rd && sol.slots[j].s + ad.s <= ins.S && !added[sol.slots[j].id] && test(chromosome, count+sol.slots[j].id-rd)){
        sol.slots[j].s += ad.s;
        sol.value += ad.v;
        sol.slots[j].ads.push_back(ad.id);
        added[sol.slots[j].id] = true;
        c2++;

        if(j == 0) continue;

        int i = j+1;
        while(i < ins.N){
          if(sol.slots[i].s < sol.slots[j].s){
            i++;
          } else if(i != j+1){
            Slot aux = sol.slots[i-1];
            sol.slots[i-1] = sol.slots[j];
            sol.slots[j] = aux;
            break;
          }else{
            break;
          }
        }

        if(i >= ins.N){
          Slot aux = sol.slots[ins.N-1];
          sol.slots[ins.N-1] = sol.slots[j];
          sol.slots[j] = aux;
        }
      }
    }
  }
}

void SampleDecoder::lastFit(Sol & sol, Ad ad, const std::vector< word >& chromosome, int count) const{
  int interval = ad.dl - ad.rd + 1;
  int c = 0;
  for(int j=interval-1; j >= 0; j--){
    if(test(chromosome, count+j) && sol.slots[ad.rd + j].s + ad.s <= ins.S){
      c++;
    }
  }

  if(c >= ad.wMin){
    int c2 = 0;
    for(int j=interval-1; j >= 0 && c2 < ad.wMax; j--){
      if(test(chromosome, count+j) && sol.slots[ad.rd + j].s + ad.s <= ins.S){
        sol.value += ad.v;
        sol.slots[ad.rd + j].s += ad.s;
        sol.slots[ad.rd + j].ads.push_back(ad.id);
        c2++;
      }
    }
  }
}

void SampleDecoder::firstFit(Sol & sol, Ad ad, const std::vector< word >& chromosome, int count) const{
  int interval = ad.dl - ad.rd + 1;
  int c = 0;
  for(int j=0; j < interval; j++){
    if(test(chromosome, count+j) && sol.slots[ad.rd + j].s + ad.s <= ins.S){
      c++;
    }
  }

  if(c >= ad.wMin){
    int c2 = 0;
    for(int j=0; j < interval && c2 < ad.wMax; j++){
      if(test(chromosome, count+j) && sol.slots[ad.rd + j].s + ad.s <= ins.S){
        sol.value += ad.v;
        sol.slots[ad.rd + j].s += ad.s;
        sol.slots[ad.rd + j].ads.push_back(ad.id);
        c2++;
      }
    }
  }
}

void SampleDecoder::midFit(Sol & sol, Ad ad, const std::vector< word >& chromosome, int count) const{
  int interval = ad.dl - ad.rd + 1;
  int c = 0;
  for(int j=0; j < interval; j++){
    if(test(chromosome, count+j) && sol.slots[ad.rd + j].s + ad.s <= ins.S){
      c++;
    }
  }

  if(c >= ad.wMin){
    int c2 = 0;


    for(int j=interval/4; j < (interval*3/4) && c2 < ad.wMax; j++){
      if(test(chromosome, count+j) && sol.slots[ad.rd + j].s + ad.s <= ins.S){
        sol.value += ad.v;
        sol.slots[ad.rd + j].s += ad.s;
        sol.slots[ad.rd + j].ads.push_back(ad.id);
        c2++;
      }
    }

    for(int j=0; j < interval/4 && c2 < ad.wMax; j++){
      if(test(chromosome, count+j) && sol.slots[ad.rd + j].s + ad.s <= ins.S){
        sol.value += ad.v;
        sol.slots[ad.rd + j].s += ad.s;
        sol.slots[ad.rd + j].ads.push_back(ad.id);
        c2++;
      }
    }

    for(int j=interval*3/4; j < interval && c2 < ad.wMax; j++){
      if(test(chromosome, count+j) && sol.slots[ad.rd + j].s + ad.s <= ins.S){
        sol.value += ad.v;
        sol.slots[ad.rd + j].s += ad.s;
        sol.slots[ad.rd + j].ads.push_back(ad.id);
        c2++;
      }
    }
  }
}

Sol SampleDecoder::createSolution(const std::vector< word >& chromosome) const{
  Sol sol;

  sol.value = 0.0;
  sol.slots = std::vector<Slot> (ins.N);

  for(int i = 0; i < ins.N; i++) {
    sol.slots[i].s = 0;
    sol.slots[i].id = i;
  }

  for(int i=ins.A-1, count = 0; i>=0; i--){
    if(i % 2 == 0){
      firstFit(sol, ins.ads[i], chromosome, count);
    }else {
      lastFit(sol, ins.ads[i], chromosome, count);
    }

    count += ins.ads[i].dl - ins.ads[i].rd + 1;
  }

  return sol;
}

double SampleDecoder::decode(const std::vector< word >& chromosome) const {
  Sol sol;

  sol = createSolution(chromosome);

	return sol.value*-1;
}

void SampleDecoder::genFileSol(const std::vector< word >& chromosome){
  Sol sol;

  sol = createSolution(chromosome);

  double result = sol.value;

  char* fn = new char[256];

  sprintf(fn, "%s/%s.sol", PATHSOL, ins.instanceName.c_str());

  ofstream opf;
  opf.open(fn);

  std::vector<Slot> slots(ins.N);

  for(int j=0; j<ins.N; j++){
    slots[sol.slots[j].id].ads = sol.slots[j].ads;
  }

  opf << result << endl;
  for(int j=0; j<ins.N; j++){
    opf << j << " ";
    for(int i = 0; i < slots[j].ads.size(); i++){
      opf << slots[j].ads[i] << " ";
    }
    opf << endl;
  }

  opf.close();

  delete[] fn;
}

void SampleDecoder::genFileEvol(const std::vector< double >& solutions){
  char* fn = new char[256];

  sprintf(fn, "%s/%s.evol", PATHEVOL, ins.instanceName.c_str());

  ofstream opf;
  opf.open(fn);

  std::vector<Slot> slots(ins.N);

  for(int i = 0, size = solutions.size(); i < size; i++){
      opf << solutions[i] << endl;
  }

  opf.close();
  delete[] fn;
}
