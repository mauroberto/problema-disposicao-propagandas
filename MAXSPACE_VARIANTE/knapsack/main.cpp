#include <stdio.h>
#include "Knapsack.h"

using namespace std;

int main(int argc, char* argv[]){
  if (argc < 2) {
    cout << "Usage: ./main filename" << endl;
    return 1;
  }

	Knapsack knapsack = Knapsack(argv[1]);

  double value;
  int c = 0;

  do{
    value = knapsack.runKnapsack(c);
    cout << ++c << " " << value << endl;
  } while(c < 500);

  return 0;
}
