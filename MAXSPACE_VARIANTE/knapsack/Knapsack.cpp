#include "Knapsack.h"

#ifndef PATHSOL
#define PATHSOL "knapsack/solutions"
#endif

using namespace std;

Knapsack::Knapsack(char * fn) {
  ifstream infile(fn);

  infile >> ins.A >> ins.N >> ins.S;

  int id, s, wMax, wMin, rd, dl;
  double v;

  while(infile >> id >> s >> rd >> dl >> wMin >> wMax >> v){
    Ad ad;
    ad.id = id;
    ad.s = s;
    ad.wMax = wMax;
    ad.wMin = wMin;
    ad.rd = rd;
    ad.dl = dl;
    ad.v = v;
    ad.qt = wMax;

    ins.ads.push_back(ad);
  }
}

Knapsack::~Knapsack() { }

double Knapsack::runKnapsack(int id){
  vector<int> a;

  for(int i=0; i<ins.A; i++){
    if(ins.ads[i].rd <= id && ins.ads[i].dl >= id){
      a.push_back(i);
    }
  }

  int size_a = a.size();

  int matrix[size_a+1][ins.S+1];

  for(int i = 0; i <= ins.S; i++){
    matrix[0][i] = 0;
  }

  for(int i = 1; i <= size_a; i++){
    int index = a[i-1];
    for(int j = 0; j <= ins.S; j++){
      if(ins.ads[index].s <= j){
        int size = ins.ads[index].s;
        matrix[i][j] = max(matrix[i-1][j], matrix[i][j-size] + size);
      } else {
        matrix[i][j] = matrix[i-1][j];
      }
    }
  }


  //Backtracking
  int s = matrix[size_a][ins.S];
  int i = size_a, j = ins.S;

  while(s > 0){

    if(matrix[i][j] > matrix[i-1][j]){
      j = j - ins.ads[a[i-1]].s;
      ins.ads[a[i-1]].qt--;
    }

    i--;
    s = matrix[i][j];
  }

  return matrix[size_a][ins.S];
}
