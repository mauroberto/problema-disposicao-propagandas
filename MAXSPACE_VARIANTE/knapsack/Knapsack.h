#ifndef BINPACKING_H
#define BINPACKING_H

#include <iostream>
#include <list>
#include <vector>
#include <algorithm>
#include <fstream>
#include <string>

typedef struct ad{
  int id;
  int s;
  int wMax;
  int wMin;
  int rd;
  int dl;
  int qt;
  double v;
} Ad;

typedef struct slot{
  int s;
  int id;
  bool * ads;
} Slot;

typedef struct sol{
  double value;
  std::vector <Slot> slots;
  bool * y;
} Sol;

typedef struct instance{
	int A;
	int N;
	int S;
	std::vector<Ad> ads;
} Instance;

class Knapsack {
public:
	Instance ins;
	Knapsack(char * fn);
	~Knapsack();

	double runKnapsack(int id);
private:

};

#endif
