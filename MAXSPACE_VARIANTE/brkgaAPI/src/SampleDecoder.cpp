/*
 * SampleDecoder.cpp
 *
 *  Created on: Jan 14, 2011
 *      Author: rtoso
 */

#include "SampleDecoder.h"

#ifndef PATHSOL
#define PATHSOL "MAXSPACE_VARIANTE/brkgaAPI/solutions"
#endif

#ifndef PATHEVOL
#define PATHEVOL "MAXSPACE_VARIANTE/brkgaAPI/solutions_evol"
#endif


using namespace std;

int max(int a, int b){
  return a > b ? a : b;
}

int min(int a, int b){
  return a < b ? a : b;
}

std::string getFileName(const std::string& s) {

   char sep = '/';

	 #ifdef _WIN32
   	sep = '\\';
	 #endif

   std::string aux;

   size_t i = s.rfind(sep, s.length());
   if (i != std::string::npos) {
      aux = s.substr(i+1, s.length() - i);
      i = aux.rfind('.', aux.length());
      if (i != std::string::npos){
        return (aux.substr(0, i));
      }
   }

   return("");
}


SampleDecoder::SampleDecoder(char * fn) {
  ifstream infile(fn);

  ins.instanceName = getFileName(fn);

  infile >> ins.A >> ins.N >> ins.S;

  int id, s, wMax, wMin, rd, dl;
  double v;

  while(infile >> id >> s >> rd >> dl >> wMin >> wMax >> v){
    Ad ad;
    ad.id = id;
    ad.s = s;
    ad.wMax = wMax;
    ad.wMin = wMin;
    ad.rd = rd;
    ad.dl = dl;
    ad.v = v;

    ins.ads.push_back(ad);
  }
}

SampleDecoder::~SampleDecoder() { }

inline void ordenaAdiciona(vector<slot> & slots, int j){
  int N = slots.size();

  if(j <= 0 || j > N-1) return;

  Slot aux = slots[j];

  int i = j-1;
  while(i >= 0){
    if(slots[i].s < aux.s){
      slots[i+1] = slots[i];
      i--;
    }else{
      slots[i+1] = aux;
      break;
    }
  }

  if(i < 0){
    slots[0] = aux;
  }
}

inline void ordenaRemove(vector<slot> & slots, int j){
  int N = slots.size();

  if(j >= N - 1 || j < 0) return;

  Slot aux = slots[j];

  int i = j+1;
  while(i < N){
    if(slots[i].s > aux.s){
      slots[i-1] = slots[i];
      i++;
    } else{
      slots[i-1] = aux;
      break;
    }
  }

  if(i >= N){
    slots[N-1] = aux;
  }
}

void SampleDecoder::bestFit(Sol & sol, Ad ad, int n_slots) const{
  int rd = max(ad.rd, 0);
  int dl = min(ad.dl, ins.N-1);

  int count = 0;
  for(int j = 0; j < ins.N; j++){
    if(count >= n_slots){
      break;
    }

    if(sol.slots[j].id <= dl && sol.slots[j].id >= rd && sol.slots[j].s + ad.s <= ins.S){
      count++;
    }
  }
  vector<bool> added(ins.N, false);

  if(count >= n_slots){
    for(int j = 0, c = 0; j < ins.N && c < n_slots; j++){
      if(sol.slots[j].id <= dl && sol.slots[j].id >= rd && sol.slots[j].s + ad.s <= ins.S && !added[sol.slots[j].id]){
        sol.slots[j].s += ad.s;
        sol.value += ad.v;
        sol.slots[j].ads.push_back(ad.id);
        c++;
        added[sol.slots[j].id] = true;

        ordenaAdiciona(sol.slots, j);
      }
    }
  }
}

void SampleDecoder::worstFit(Sol & sol, Ad ad, int n_slots) const{
  int rd = max(ad.rd, 0);
  int dl = min(ad.dl, ins.N-1);

  int count = 0;
  for(int j = 0; j < ins.N; j++){
    if(count >= n_slots){
      break;
    }

    if(sol.slots[j].id <= dl && sol.slots[j].id >= rd && sol.slots[j].s + ad.s <= ins.S){
      count++;
    }
  }

  vector<bool> added(ins.N, false);

  if(count >= n_slots){
    for(int j = ins.N-1, c = 0; j >= 0 && c < n_slots; j--){
      if(sol.slots[j].id <= dl && sol.slots[j].id >= rd && sol.slots[j].s + ad.s <= ins.S && !added[sol.slots[j].id]){
        sol.slots[j].s += ad.s;
        sol.value += ad.v;
        added[sol.slots[j].id] = true;
        sol.slots[j].ads.push_back(ad.id);

        c++;
        ordenaAdiciona(sol.slots, j);
        j++;
      }
    }
  }
}

void SampleDecoder::lastFit(Sol & sol, Ad ad, int n_slots) const{
    int rd = max(ad.rd, 0);
    int dl = min(ad.dl, ins.N-1);

    int count = 0;
    for(int j = dl; j >= rd; j--){
      if(count >= n_slots){
        break;
      }

      if(sol.slots[j].s + ad.s <= ins.S){
        count++;
      }
    }

    if(count >= n_slots){
      for(int j = dl, c = 0; j >= rd && c < n_slots; j--){
        if(sol.slots[j].s + ad.s <= ins.S){
          sol.slots[j].s += ad.s;
          sol.value += ad.v;
          sol.slots[j].ads.push_back(ad.id);
          c++;
        }
      }
    }
}

void SampleDecoder::firstFit(Sol & sol, Ad ad, int n_slots) const{
  int rd = max(ad.rd, 0);
  int dl = min(ad.dl, ins.N-1);

  int count = 0;
  for(int j = rd; j <= dl; j++){
    if(count >= n_slots){
      break;
    }

    if(sol.slots[j].s + ad.s <= ins.S){
      count++;
    }
  }

  if(count >= n_slots){
    for(int j = rd, c = 0; j <= dl && c < n_slots; j++){
      if(sol.slots[j].s + ad.s <= ins.S){
        sol.slots[j].s += ad.s;
        sol.value += ad.v;
        sol.slots[j].ads.push_back(ad.id);
        c++;
      }
    }
  }
}

Sol SampleDecoder::createSolution(const std::vector< double >& chromosome) const{
  std::vector< std::pair<double, int> > ranking(ins.A);

  Sol sol;

  sol.value = 0.0;
  sol.slots = std::vector<Slot> (ins.N);

  for(int i = 0; i < ins.N; i++) {
    sol.slots[i].s = 0;
    sol.slots[i].id = i;
  }

  for(int i = 0; i < ins.A; i++) {
    ranking[i] = std::pair< double, int >(chromosome[i], i);
  }


  std::sort(ranking.begin(), ranking.end(), std::greater<std::pair<double, int> >());

  for(int i = 0; i < ins.A; i++){
    int index = ranking[i].second;
    Ad ad = ins.ads[index];
    double orderAlele = chromosome[2*ins.A + index];

    int p = chromosome[index + ins.A];

    int n_slots = p * (ad.wMax - ad.wMin) + ad.wMin;

    if(orderAlele <= 1.0/2.0){
      worstFit(sol, ad, n_slots);
    } else{
      bestFit(sol, ad, n_slots);
    }
  }

  return sol;
}

double SampleDecoder::decode(const std::vector< double >& chromosome) const {
  Sol sol;

  sol = createSolution(chromosome);

	return sol.value*-1;
}

void SampleDecoder::genFileSol(const std::vector< double >& chromosome){
  Sol sol;

  sol = createSolution(chromosome);

  double result = sol.value;

  char* fn = new char[256];

  sprintf(fn, "%s/%s.sol", PATHSOL, ins.instanceName.c_str());

  ofstream opf;
  opf.open(fn);

  std::vector<Slot> slots(ins.N);

  for(int j=0; j<ins.N; j++){
    slots[sol.slots[j].id].ads = sol.slots[j].ads;
  }

  opf << result << endl;
  for(int j=0; j<ins.N; j++){
    opf << j << " ";
    for(int i = 0; i < slots[j].ads.size(); i++){
      opf << slots[j].ads[i] << " ";
    }
    opf << endl;
  }

  opf.close();
}

void SampleDecoder::genFileEvol(const std::vector< double >& solutions){
  char* fn = new char[256];

  sprintf(fn, "%s/%s.evol", PATHEVOL, ins.instanceName.c_str());

  ofstream opf;
  opf.open(fn);

  std::vector<Slot> slots(ins.N);

  for(int i = 0, size = solutions.size(); i < size; i++){
      opf << solutions[i] << endl;
  }

  opf.close();
}
