#include "BITree.h"
#include <iostream>

using namespace std;

int main(){
  int freq[] = {2, 1, 1, 3, 2, 3, 4, 5, 6, 7, 8, 9};
  int n = 12;
  BITree bt = BITree(freq, n);
  cout << "Sum of elements in arr[0..5] is " << bt.getSum(5) << endl;

  for(int i=0; i < n; i++){
      cout << "Max of elements in arr[0.."<< i << "] after update is " << bt.getMax(0, i) << endl;
  }

  for(int i=n-1; i >= 0; i--){
      cout << "Max of elements in arr["<< i << "..11] after update is " << bt.getMax(i, 11) << endl;
  }

  freq[3] = 11;
  bt.updateBIT(3, 11);
  cout << "Max of elements in arr[0..3] after update is " << bt.getMax(0, 3) << endl;

  cout << "Sum of elements in arr[0..5] after update is " << bt.getSum(5) << endl;

  cout << "Sum of elements in arr[2..5] after update is " << bt.getSumInterval(2, 5) << endl;

  return 0;
}
