/*
 * Decoder.cpp
 *
 *  Created on: Jan 14, 2011
 *      Author: rtoso
 */

#include "Decoder.h"

#ifndef PATHSOL
#define PATHSOL "MAXSPACE_VARIANTE/Hybrid-GA/solutions"
#endif

#ifndef PATHEVOL
#define PATHEVOL "MAXSPACE_VARIANTE/Hybrid-GA/solutions_evol"
#endif

using namespace std;

int max(int a, int b){
    return a > b ? a : b;
}

int min(int a, int b){
    return a < b ? a : b;
}

Decoder::Decoder(Instance ins) {
    this->ls = LSMF(ins);
    this->ins = ins;
}

Decoder::~Decoder() { }

Solution Decoder::createSolution(const std::vector< int > & chromosome){
    Solution sol(ins.N, ins.A, ins.S, &ins);
    Solution sol2(ins.N, ins.A, ins.S, &ins);

    vector<int> ranking(ins.A);

    for(int i = 0; i < ins.A; i++){
        int index = chromosome[i];
        Ad ad = ins.ads[index];
        sol.addFirstFit(ad);
        sol2.addWorstFit(ad);
        ranking[i] = chromosome[i];
    }

    if(sol2.value > sol.value){
        sol = sol2;
    }

    Solution sol3 = this->ls.runLSMF(ranking);

    if(sol3.value > sol.value){
        sol = sol3;
    }

    return sol;
}

double Decoder::decode(const std::vector< int > & chromosome) {
    Solution sol;

    sol = createSolution(chromosome);

    return sol.value*-1;
}

std::vector<int> Decoder::getInitialChromosome(){
    std::vector < int > chromosome;
    std::vector <std::pair< int, int> > ranking;


    for(int i = 0; i < ins.A; i++){
        ranking.push_back(make_pair(ins.ads[i].s, i));
    }

    std::sort(ranking.begin(), ranking.end(), std::greater<std::pair<int, int> >());

    for(int i = 0; i < ins.A; i++){
        chromosome.push_back(ranking[i].second);
    }

    return chromosome;
}

void Decoder::genFileEvol(const std::vector< double >& solutions){
    char* fn = new char[256];

    sprintf(fn, "%s/%s.evol", PATHEVOL, ins.instanceName.c_str());

    ofstream opf;
    opf.open(fn);

    std::vector<Slot> slots(ins.N);

    for(int i = 0, size = solutions.size(); i < size; i++){
        opf << solutions[i] << endl;
    }

    opf.close();
}

void Decoder::genFileSol(const std::vector< int >& chromosome){
  Solution sol;

  sol = createSolution(chromosome);

  sol.exportToFile("MAXSPACE_VARIANTE/Hybrid-GA/solutions/"+ins.instanceName+".sol");
}
