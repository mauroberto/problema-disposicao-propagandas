#include "TabuSearch.h"
#include <iostream>
#include <time.h>
#include <sys/time.h>
#include <algorithm>
#include <vector>

TabuSearch::TabuSearch() : maxSize(40), it(100), timeout(600), N_MAX(0){

}

TabuSearch::TabuSearch(Instance* ins, int _maxSize, int _it, int _timeout, timeval start, Strategy* strategy, int _N_MAX, std::vector<Neighborhood*> N) 
: maxSize(_maxSize), it(_it), timeout(_timeout), N_MAX(_N_MAX){
    this->ins = ins;
    this->start = start;
    this->N = N; 
    this->strategy = strategy;

    this->tempo.resize(this->N_MAX, 0);
    this->qtd.resize(this->N_MAX, 0);
    this->melhorias.resize(this->N_MAX, 0);
}

TabuSearch::~TabuSearch(){
}

bool TabuSearch::listContains(const std::vector<Change*> & list, const Change* p){
    int s = list.size();
    for(int j = 0; j < s; j++){
        if((*p) == (*list[j])){
            return true;
        }
    }
    return false;
}

void TabuSearch::runTabuSearch(Solution & sol){
    Solution sBest = sol, sBest2, bestCandidate;
    std::vector<Change*> tabuList;
    timeval stop;

    bool t = false;
    int count = 0;
    int r = 0;

    while(this->strategy->stopCondition(count, t, r)){
        count ++;
        gettimeofday(&lastChange, NULL);
        gettimeofday(&stop, NULL);
        double elapsed = ((double) (stop.tv_sec * 1000000 + stop.tv_usec) - (start.tv_sec * 1000000 + start.tv_usec)) / 1000000;

        if(elapsed > this->timeout){
            break;
        }

        r = this->strategy->getNextNeighborhood(t, r);
        
        this->qtd[r]++;

        this->N[r]->generateNeighbors(sBest);

        int s2 = this->N[r]->neighbors.size();
        t = false;
        if(!s2) continue;

        Change* p = this->N[r]->neighbors[0];

        bestCandidate = p->applyChange(sBest);

        sBest2 = sBest;

        for(int i=1; i<s2; i++){
            Change* p2 = this->N[r]->neighbors[i];
            Solution sol2 = p2->applyChange(sBest);
            bool contains = listContains(tabuList, p2);

            if(!contains && bestCandidate < sol2){
                bestCandidate = sol2;
                p = p2;
            } else if (contains &&  sBest < sol2){
                p = p2;
                sBest2 = sol2;
            }
        }

        if( sBest < bestCandidate){
            sBest = bestCandidate;
            t = true;
        }

       
        if(sBest < sBest2){
            sBest = sBest2;
            t = true;
        }

        if(t){
            this->melhorias[r]++;
        }

        gettimeofday(&stop, NULL);
        this->tempo[r] += ((double) (stop.tv_sec * 1000000 + stop.tv_usec) - (lastChange.tv_sec * 1000000 + lastChange.tv_usec)) / 1000000;


        Change *c = p->clone();

        tabuList.push_back(c);
        if((int) tabuList.size() > this->maxSize){
            std::for_each(tabuList.begin(), tabuList.begin() + 1, []( Change* element) { delete element; });
            tabuList.erase(tabuList.begin(), tabuList.begin()+1);
        }
    }

    sol = sBest;

    for (std::vector< Change* >::iterator it = tabuList.begin() ; it != tabuList.end(); ++it){
        delete (*it);
    }
}