#ifndef STRATEGY_H
#define STRATEGY_H

#include "Utils.h"

class Strategy{
protected:
    Utils utils;
    const int it, N_MAX;
public:
    Strategy();
    Strategy(int it, int N_MAX);
    virtual ~Strategy();
    virtual bool stopCondition(int count, bool hasImprovement, int currentN);
    virtual int getNextNeighborhood(bool t, int currentN);
};

inline Strategy::Strategy(): it(0), N_MAX(0){

}

inline Strategy::Strategy(int it_, int N_MAX_): it(it_), N_MAX(N_MAX_){

}

inline Strategy::~Strategy(){

}

inline bool Strategy::stopCondition(int count, bool hasImprovement, int currentN){
    return false;
}

inline int Strategy::getNextNeighborhood(bool t, int currentN){
    return 0;
}

class Randomly : public Strategy{
public:
    Randomly();
    Randomly(int it, int N_MAX);
    ~Randomly() override;
  
    bool stopCondition(int count, bool hasImprovement, int currentN) override;
    int getNextNeighborhood(bool t, int currentN) override;
};

inline Randomly::Randomly(): Strategy(){

}

inline Randomly::Randomly(int it_, int N_MAX_): Strategy(it_, N_MAX_){

}

inline Randomly::~Randomly(){

}

inline bool Randomly::stopCondition(int count, bool hasImprovement, int currentN){
    return count < this->it;
}

inline int Randomly::getNextNeighborhood(bool t, int currentN){
    return this->utils.getRandomNumber(0, this->N_MAX-1);
}

class Cycling : public Strategy{
public:
    Cycling();
    Cycling(int it, int N_MAX);
    ~Cycling() override;
  
    bool stopCondition(int count, bool hasImprovement, int currentN) override;
    int getNextNeighborhood(bool t, int currentN) override;
};

inline Cycling::Cycling(): Strategy(){

}

inline Cycling::Cycling(int it_, int N_MAX_): Strategy(it_, N_MAX_){

}

inline Cycling::~Cycling(){

}

inline bool Cycling::stopCondition(int count, bool hasImprovement, int currentN){
    return count < this->it;
}

inline int Cycling::getNextNeighborhood(bool t, int currentN){
    return (++currentN) % this->N_MAX;
}

class MaximumImprovement : public Strategy{
private:
    bool first;
public:
    MaximumImprovement();
    MaximumImprovement(int it, int N_MAX);
    ~MaximumImprovement() override;
  
    bool stopCondition(int count, bool hasImprovement, int currentN) override;
    int getNextNeighborhood(bool t, int currentN) override;
};

inline MaximumImprovement::MaximumImprovement(): Strategy(){

}

inline MaximumImprovement::MaximumImprovement(int it_, int N_MAX_): Strategy(it_, N_MAX_){
    this->first = true;
}

inline MaximumImprovement::~MaximumImprovement(){

}

inline bool MaximumImprovement::stopCondition(int count, bool hasImprovement, int currentN){
    return hasImprovement || currentN < (this->N_MAX - 1);
}

inline int MaximumImprovement::getNextNeighborhood(bool t, int currentN){
    if(t || (!t && this->first)){
        this->first = false;
        return currentN;
    }

    this->first = true;
    return ++currentN;
}

#endif