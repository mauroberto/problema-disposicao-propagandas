#include <stdio.h>
#include "TabuSearch.h"
#include "Solution.h"
#include "Instance.h"
#include "ConstructiveHeuristic.h"
#include <time.h>
#include <sys/time.h>
#include "Utils.h"

using namespace std;

int timeout;
double elapsed;
struct timeval start, stop;

int main(int argc, char* argv[]){
    if (argc < 2) {
        cout << "Usage: ./main filename" << endl;
        return 1;
    }

	timeout = 3600;
	cout.precision(2);

    Utils utils;
    Instance ins(argv[1]);
    gettimeofday(&start, NULL);
    vector<Neighborhood*> N;
    Strategy* strategy = new Randomly(100, 5); 

    N.push_back(new Move(&ins));
    N.push_back(new Repack(&ins));
    N.push_back(new AddCopy(&ins));
    N.push_back(new AddItem(&ins));
    N.push_back(new ChangeItem(&ins));

    TabuSearch * ts = new TabuSearch(&ins, 40, 100, timeout, start, strategy, 5, N);
    ConstructiveHeuristic ch(ins);
    Solution sol = ch.greedyRandomizedConstruction3(0.3);
    sol.remakeSol();
    ts->runTabuSearch(sol);

    sol.exportToFile("MAXSPACE_VARIANTE/TabuSearch/solutions/"+ins.instanceName+".sol");

    cout << fixed << "Solution = " << sol.value << endl;
    gettimeofday(&stop, NULL);
    elapsed = ((double) (stop.tv_sec * 1000000 + stop.tv_usec) - (start.tv_sec * 1000000 + start.tv_usec)) / 1000000;
    cout << fixed << "Time = " << elapsed << endl;
    delete ts;
    delete strategy;

    for(int i = 0; i < 5; i++){
        delete N[i];
    }

    return 0;
}
