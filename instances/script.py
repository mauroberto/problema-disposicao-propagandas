f = open("instances descriptions", "r")

classes = f.read().split("------------")

txt = "classes = ["

for i in range(0, len(classes)-2):
    text = "("
    linhas = classes[i].split("\n")
    for j in range(1, len(linhas)):
        linha = linhas[j].split("=")
        for k in range(1, len(linha)):
            if k > 1:
                text = text + "="    
            text = text + linha[k]
        if len(linha) > 1 and j < len(linhas) - 1 and "=" in linhas[j+1]:
            text = text + ","
    text = text + ")"
    txt = txt + ",\n"+ text

txt = txt + "]\n"

print (txt)

f.close()