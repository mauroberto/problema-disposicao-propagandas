# cada propaganda A_i recebe s_i = h_i
# a altura dos slots é H
# o número de slots é o teto da média da ocupação
# e wMax = wMin = min(d_i, número de slots)


import math
import os

dirs = [
	"Falkenauer_U",
	"Hard28_CSP",
	"Irnich CSP",
	"Scholl_CSP",
	"Schwerin_CSP",
	"Waescher_CSP",
	"ANI_AI_CSP"
]


for dirName in dirs:
	for root, dirs, files in os.walk("./"+dirName):  
		for filename in files:
			f = open("%s/%s" % (dirName, filename), "r")
			A = int(f.readline())
			S = int(f.readline())
			N = 0.0
			itensC = []
			itensT = []
			maxW = 0
			for j in range(0, A):
				linha = f.readline().split("	")
				itensT.append(int(linha[0]))
				itensC.append(int(linha[1]))
				N = N + (int(linha[1]) * int(linha[0]))
			maxW = max(maxW, int(linha[1]))
			N = int(math.ceil(N/S))
			f2 = open("literatura/%s.ebp" % (filename.split(".")[0]), "w+")
			f2.write("%d %d %d\n" % (A, N, S))
			for j in range(0, A):
				itensC[j] = min(itensC[j], N)
				f2.write("%d %d %d %d %d %d %d\n" % (j, itensT[j], 0, N-1, itensC[j], itensC[j], itensT[j]))

			f2.close()
			f.close()


