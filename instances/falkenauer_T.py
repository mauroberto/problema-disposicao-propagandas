import math
import os

dirName = "Falkenauer_T"

for root, dirs, files in os.walk("./"+dirName):  
	for filename in files:
		f = open("%s/%s" % (dirName, filename), "r")
		A = int(f.readline())
		S = int(f.readline())
		N = 0.0
		itensC = []
		itensT = []
		for j in range(0, A):
			linha = f.readline().split("	")
			itensT.append(int(linha[0]))
			itensC.append(int(linha[1]))
			N = N + int(linha[1])
		
		N = int(math.ceil(N / 3.0))
		f2 = open("literatura/%s.pdp" % (filename.split('.')[0]), "w+")
		f2.write("%d %d %d\n" % (A, N, S))
	
		for j in range(0, A):
			f2.write("%d %d %d %d %d %d %d\n" % (j, itensT[j], 0, N-1, itensC[j], itensC[j], itensT[j]))

		f2.close()
		f.close()
