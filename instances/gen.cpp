#include <iostream>
#include <stdlib.h>
#include <time.h>
#include <vector>
#include <fstream>
#include <string>

using namespace std;

typedef struct ad{
	int id;
	int s;
	int wMax;
	int wMin;
	int rd;
	int dl;
	double v;
} Ad;

int A, N, S, num = 1;
double alpha = 2.0;
vector<Ad> ads;

int max(int a, int b){
	return (a > b) ? a : b;
}

int min(int a, int b){
	return (a > b) ? b : a;
}

int getRandomNumber(int min, int max){
	int r = (rand() % (max - min + 1)) + min;
	return r;
}

int genSize(int i){
	#ifdef SIZE
		return SIZE;
	#endif
	#ifdef SIZEMIN
		return getRandomNumber(SIZEMIN, SIZEMAX);
	#else
		return getRandomNumber(S/3, 2*S/3);
	#endif
}

int genReleaseDate(int i){
	#ifdef RD
		return max(RD, 0);
	#else
		return getRandomNumber(0, N-1);
	#endif
}

int genDeadline(int i){
	#ifdef DL
		return min(DL, N-1);
	#else
		return getRandomNumber(ads[i].rd, N-1);
	#endif
}

int genWMin(int i){
	#ifdef WMIN
		return max(WMIN, 0);
	#else
		int range = ads[i].dl - ads[i].rd + 1;
		return getRandomNumber(1, range);
	#endif
}

int genWMax(int i){
	#ifdef WMAX
		return min(WMAX, N);
	#else
		int range = ads[i].dl - ads[i].rd + 1;
		return getRandomNumber(ads[i].wMin, range);
	#endif
}

double genValue(int i){
	#ifdef VALUE
		return VALUE;
	#else
		return getRandomNumber(1, 100);
	#endif
}

void genAds(){
	double total = 0.0;
	for(int i=0; i<A || total / N*S < alpha; i++){
		Ad a;
		a.id = i;
		ads.push_back(a);
		ads[i].s = genSize(i);
		ads[i].rd = genReleaseDate(i);
		ads[i].dl = genDeadline(i);
		ads[i].wMin = genWMin(i);
		ads[i].wMax = genWMax(i);
		ads[i].v = genValue(i);
		total += ads[i].s * ads[i].wMin;
	}
}

void genFile(){
	char* fn = new char[256];
	int size = ads.size();	
  	#ifdef CLASS
    	sprintf(fn, "./instances/instances/%d_%d_%d_%d_%d.pdp", size, N, S, CLASS, num);
	#else
		sprintf(fn, "./instances/instances/%d_%d_%d_%d.pdp", size, N, S, num);
	#endif

	ofstream opf;
	opf.open(fn);

	opf << size << " " << N << " " << S << endl;
	for(int i=0; i<size; i++){
		opf << ads[i].id << " ";
		opf << ads[i].s << " ";
		opf << ads[i].rd << " ";
		opf << ads[i].dl << " ";
		opf << ads[i].wMin << " ";
		opf << ads[i].wMax << " ";
		opf << ads[i].v << endl;
	}

	opf.close();

	delete[] fn;
}


int main(int argc, char *argv[]){
	num = 0;

	if(argc < 4){
		cout << "Usage: ./GEN A N S [num]" << endl;
		exit(1);
	}
	
	A = atoi(argv[1]);
	N = atoi(argv[2]);
	S = atoi(argv[3]);

	if(argc > 4){
		num = atoi(argv[4]);
	}

	long seed = A*N*S*(num + 1)*CLASS;
	srand(seed);

	genAds();

	cout << ads.size() << endl;

	genFile();

	return 0;
}
