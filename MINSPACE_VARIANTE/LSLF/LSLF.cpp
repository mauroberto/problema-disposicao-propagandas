#include "Solution.h"
#include "Instance.h"
#include <time.h>
#include <math.h>
#include <iostream>
#include <algorithm>

#define EPSILON 1

using namespace std;

int timeout;
double elapsed;
clock_t clk;

int main(int argc, char* argv[]){
    if (argc < 2) {
        cout << "Usage: ./main filename" << endl;
        return 1;
    }

    clk = clock();
    timeout = 3600;
    cout.precision(2);

    Instance ins(argv[1]);

    vector<pair<int, int>> A;

    int M = 0;

    for(int i=0; i<ins.A; i++){
        A.push_back(make_pair(ins.ads[i].s, i)); 
        M += ins.ads[i].s;
    }

    sort(A.begin(), A.end(), greater<pair<int, int> >());        

    elapsed = ((double) (clock() - clk)) / CLOCKS_PER_SEC;


    Solution sol(ins.N, ins.A, M);

    int i;
    for(i = 0; i < ins.A; i++){
        int id = A[i].second;

        if(!sol.addWorstFit(ins.ads[id])){
            break;
        }
    }

    sol.exportToFile("MINSPACE_VARIANTE/LSLF/solutions/"+ins.instanceName+".sol");

    cout << "Solution = " << sol.value << endl;
    elapsed = ((double) (clock() - clk)) / CLOCKS_PER_SEC;
    cout << fixed << "Time = " << elapsed << endl;
    return 0;
}

