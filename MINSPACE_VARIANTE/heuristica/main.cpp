#include "Solution.h"
#include "Instance.h"
#include <time.h>
#include <math.h>
#include <iostream>
#include <algorithm>

#define EPSILON 1

using namespace std;

int timeout;
double elapsed;
clock_t clk;

int max(int a, int b){
    return a > b ? a : b;
}

int min(int a, int b){
    return a < b ? a : b;
}

int main(int argc, char* argv[]){
    if (argc < 2) {
        cout << "Usage: ./main filename" << endl;
        return 1;
    }

    clk = clock();
    timeout = 3600;
    cout.precision(2);

    Instance ins(argv[1]);

    vector<vector<pair<int, int>>> A(ins.N);

    int UB = 0, LB = 0;
    int sMin = ins.ads[0].s, sMax = ins.ads[0].s;

    for(int i=0; i<ins.A; i++){
        if(ins.ads[i].s < sMin){
            sMin = ins.ads[i].s;
        }

        if(ins.ads[i].s > sMax){
            sMax = ins.ads[i].s;
        }

        UB += ins.ads[i].s;

        A[ins.ads[i].rd].push_back(make_pair(ins.ads[i].s, i)); 
    }


    //$w_{ij} = max(w_i - (j - rd_i), 0) se rd_i >= j, senão 0$

    vector<vector<int> > w(ins.A);

    for(int i = 0; i < ins.A; i++){
        w[i].resize(ins.N);
        
        for(int j = 0; j < ins.ads[i].rd; j++){
            w[i][j] = 0;
        }

        for(int j = ins.ads[i].rd; j < ins.N; j++){
            w[i][j] = max(ins.ads[i].wMin - (j - ins.ads[i].rd), 0);
        }
    }

    //$M_j = \frac{\sum\limits_{a_i \in A} w_{ij}*s_i }{N - j}$
    //$LB = max{M_j}$

    vector<double> M(ins.N);

    for(int j = 0; j < ins.N; j++){
        M[j] = 0.0;
        for(int i = 0; i < ins.A; i++){
            M[j] += w[i][j] * ins.ads[i].s;
        }

        M[j] /= (ins.N - j);

        LB = max(LB, ceil(M[j]));
    }

    std::cout << "LB = " << LB << " UB = " << UB << std::endl;

    Solution bestSol;

    for(int j = ins.N - 1; j >= 0; j--){
        sort(A[j].begin(), A[j].end(), greater<pair<int, int> >());        
    }

    elapsed = ((double) (clock() - clk)) / CLOCKS_PER_SEC;

    while(UB - LB > EPSILON && elapsed < timeout){
        double media = ( UB + LB )/ 2;
        Solution sol(ins.N, ins.A, media);

        int j;
        for(j = ins.N - 1; j >= 0; j--){
            int s = A[j].size();
            int i;
            for(i = 0; i < s; i++){
                int id = A[j][i].second;

                if(!sol.addWorstFit(ins.ads[id])){
                    break;
                }
            }

            if(i < s){
                break;
            }
        }

        if ( j >= 0 ){
            LB = ceil(media);
        } else{
            UB = min(floor(media), sol.value);
            bestSol = sol;
        }

        cout << fixed << "Média = " << media << endl; 
        cout << "Solução atual = " << bestSol.value << endl;

        elapsed = ((double) (clock() - clk)) / CLOCKS_PER_SEC;
    }


    bestSol.exportToFile("MINSPACE_VARIANTE/heuristica/solutions/"+ins.instanceName+".sol");

    cout << "Solution = " << bestSol.value << endl;
    elapsed = ((double) (clock() - clk)) / CLOCKS_PER_SEC;
    cout << fixed << "Time = " << elapsed << endl;
    return 0;
}

