Flat profile:

Each sample counts as 0.01 seconds.
  %   cumulative   self              self     total           
 time   seconds   seconds    calls  us/call  us/call  name    
 93.94     26.57    26.57 503781069     0.05     0.05  Slot::operator=(Slot const&)
  3.98     27.69     1.13  1600829     0.70    17.61  Solution::sortAdd(int)
  1.38     28.08     0.39  1621829     0.24     0.24  Slot::~Slot()
  0.42     28.20     0.12  1601329     0.07     0.07  Slot::Slot(Slot const&)
  0.25     28.27     0.07                             Solution::addWorstFit(Ad&)
  0.04     28.28     0.01                             Solution::exportToFile(std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> >)
  0.02     28.29     0.01                             Solution::~Solution()
  0.00     28.29     0.00  1600829     0.00     0.00  Slot::addCopy(Ad&)
  0.00     28.29     0.00    36383     0.00     0.00  Ad::~Ad()
  0.00     28.29     0.00    10500     0.00     0.00  Slot::Slot()
  0.00     28.29     0.00    10020     0.00     0.00  std::vector<bool, std::allocator<bool> >::_M_fill_insert(std::_Bit_iterator, unsigned long, bool)
  0.00     28.29     0.00    10000     0.00     0.00  Ad::Ad(int, int, int, int, int, int, double)
  0.00     28.29     0.00    10000     0.00     0.00  Slot::Slot(int, int)
  0.00     28.29     0.00     1052     0.00     0.00  void std::__move_median_to_first<__gnu_cxx::__normal_iterator<std::pair<int, int>*, std::vector<std::pair<int, int>, std::allocator<std::pair<int, int> > > >, __gnu_cxx::__ops::_Iter_comp_iter<std::greater<std::pair<int, int> > > >(__gnu_cxx::__normal_iterator<std::pair<int, int>*, std::vector<std::pair<int, int>, std::allocator<std::pair<int, int> > > >, __gnu_cxx::__normal_iterator<std::pair<int, int>*, std::vector<std::pair<int, int>, std::allocator<std::pair<int, int> > > >, __gnu_cxx::__normal_iterator<std::pair<int, int>*, std::vector<std::pair<int, int>, std::allocator<std::pair<int, int> > > >, __gnu_cxx::__normal_iterator<std::pair<int, int>*, std::vector<std::pair<int, int>, std::allocator<std::pair<int, int> > > >, __gnu_cxx::__ops::_Iter_comp_iter<std::greater<std::pair<int, int> > >)
  0.00     28.29     0.00       20     0.00     0.00  std::vector<Slot, std::allocator<Slot> >::_M_default_append(unsigned long)
  0.00     28.29     0.00       15     0.00     0.00  void std::vector<Ad, std::allocator<Ad> >::_M_realloc_insert<Ad const&>(__gnu_cxx::__normal_iterator<Ad*, std::vector<Ad, std::allocator<Ad> > >, Ad const&)
  0.00     28.29     0.00       15     0.00    27.11  std::vector<Slot, std::allocator<Slot> >::operator=(std::vector<Slot, std::allocator<Slot> > const&)
  0.00     28.29     0.00        1     0.00     0.00  _GLOBAL__sub_I__ZN8SolutionC2Eiii
  0.00     28.29     0.00        1     0.00     0.00  _GLOBAL__sub_I_timeout
  0.00     28.29     0.00        1     0.00     0.00  Utils::getFileName(std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > const&)
  0.00     28.29     0.00        1     0.00     0.00  Utils::Utils()
  0.00     28.29     0.00        1     0.00     0.00  Utils::~Utils()

 %         the percentage of the total running time of the
time       program used by this function.

cumulative a running sum of the number of seconds accounted
 seconds   for by this function and those listed above it.

 self      the number of seconds accounted for by this
seconds    function alone.  This is the major sort for this
           listing.

calls      the number of times this function was invoked, if
           this function is profiled, else blank.

 self      the average number of milliseconds spent in this
ms/call    function per call, if this function is profiled,
	   else blank.

 total     the average number of milliseconds spent in this
ms/call    function and its descendents per call, if this
	   function is profiled, else blank.

name       the name of the function.  This is the minor sort
           for this listing. The index shows the location of
	   the function in the gprof listing. If the index is
	   in parenthesis it shows where it would appear in
	   the gprof listing if it were to be printed.

Copyright (C) 2012-2018 Free Software Foundation, Inc.

Copying and distribution of this file, with or without modification,
are permitted in any medium without royalty provided the copyright
notice and this notice are preserved.

		     Call graph (explanation follows)


granularity: each sample hit covers 2 byte(s) for 0.04% of 28.29 seconds

index % time    self  children    called     name
                                                 <spontaneous>
[1]     99.9    0.07   28.20                 Solution::addWorstFit(Ad&) [1]
                1.13   27.07 1600829/1600829     Solution::sortAdd(int) [2]
                0.00    0.00 1600829/1600829     Slot::addCopy(Ad&) [18]
-----------------------------------------------
                1.13   27.07 1600829/1600829     Solution::addWorstFit(Ad&) [1]
[2]     99.7    1.13   27.07 1600829         Solution::sortAdd(int) [2]
               26.57    0.00 503763569/503781069     Slot::operator=(Slot const&) [3]
                0.39    0.00 1600829/1621829     Slot::~Slot() [4]
                0.12    0.00 1600829/1601329     Slot::Slot(Slot const&) [5]
-----------------------------------------------
                0.00    0.00     500/503781069     Solution::exportToFile(std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> >) [6]
                0.00    0.00    7000/503781069     std::vector<Slot, std::allocator<Slot> >::operator=(std::vector<Slot, std::allocator<Slot> > const&) [9]
                0.00    0.00   10000/503781069     Solution::Solution(int, int, int) [8]
               26.57    0.00 503763569/503781069     Solution::sortAdd(int) [2]
[3]     93.9   26.57    0.00 503781069         Slot::operator=(Slot const&) [3]
-----------------------------------------------
                0.00    0.00     500/1621829     Solution::exportToFile(std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> >) [6]
                0.00    0.00   10000/1621829     Solution::Solution(int, int, int) [8]
                0.00    0.00   10500/1621829     Solution::~Solution() [7]
                0.39    0.00 1600829/1621829     Solution::sortAdd(int) [2]
[4]      1.4    0.39    0.00 1621829         Slot::~Slot() [4]
-----------------------------------------------
                0.00    0.00     500/1601329     std::vector<Slot, std::allocator<Slot> >::operator=(std::vector<Slot, std::allocator<Slot> > const&) [9]
                0.12    0.00 1600829/1601329     Solution::sortAdd(int) [2]
[5]      0.4    0.12    0.00 1601329         Slot::Slot(Slot const&) [5]
-----------------------------------------------
                                                 <spontaneous>
[6]      0.0    0.01    0.00                 Solution::exportToFile(std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> >) [6]
                0.00    0.00     500/1621829     Slot::~Slot() [4]
                0.00    0.00     500/503781069     Slot::operator=(Slot const&) [3]
                0.00    0.00     500/10500       Slot::Slot() [20]
-----------------------------------------------
                                                 <spontaneous>
[7]      0.0    0.01    0.00                 Solution::~Solution() [7]
                0.00    0.00   10500/1621829     Slot::~Slot() [4]
-----------------------------------------------
                                                 <spontaneous>
[8]      0.0    0.00    0.00                 Solution::Solution(int, int, int) [8]
                0.00    0.00   10000/1621829     Slot::~Slot() [4]
                0.00    0.00   10000/503781069     Slot::operator=(Slot const&) [3]
                0.00    0.00   10000/10000       Slot::Slot(int, int) [23]
                0.00    0.00      20/10020       std::vector<bool, std::allocator<bool> >::_M_fill_insert(std::_Bit_iterator, unsigned long, bool) [21]
                0.00    0.00      20/20          std::vector<Slot, std::allocator<Slot> >::_M_default_append(unsigned long) [25]
-----------------------------------------------
                0.00    0.00      15/15          Solution::operator=(Solution const&) [10]
[9]      0.0    0.00    0.00      15         std::vector<Slot, std::allocator<Slot> >::operator=(std::vector<Slot, std::allocator<Slot> > const&) [9]
                0.00    0.00    7000/503781069     Slot::operator=(Slot const&) [3]
                0.00    0.00     500/1601329     Slot::Slot(Slot const&) [5]
-----------------------------------------------
                                                 <spontaneous>
[10]     0.0    0.00    0.00                 Solution::operator=(Solution const&) [10]
                0.00    0.00      15/15          std::vector<Slot, std::allocator<Slot> >::operator=(std::vector<Slot, std::allocator<Slot> > const&) [9]
-----------------------------------------------
                0.00    0.00 1600829/1600829     Solution::addWorstFit(Ad&) [1]
[18]     0.0    0.00    0.00 1600829         Slot::addCopy(Ad&) [18]
-----------------------------------------------
                0.00    0.00   10000/36383       Instance::~Instance() [37]
                0.00    0.00   10000/36383       Instance::Instance(std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> >) [36]
                0.00    0.00   16383/36383       void std::vector<Ad, std::allocator<Ad> >::_M_realloc_insert<Ad const&>(__gnu_cxx::__normal_iterator<Ad*, std::vector<Ad, std::allocator<Ad> > >, Ad const&) [26]
[19]     0.0    0.00    0.00   36383         Ad::~Ad() [19]
-----------------------------------------------
                0.00    0.00     500/10500       Solution::exportToFile(std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> >) [6]
                0.00    0.00   10000/10500       std::vector<Slot, std::allocator<Slot> >::_M_default_append(unsigned long) [25]
[20]     0.0    0.00    0.00   10500         Slot::Slot() [20]
-----------------------------------------------
                0.00    0.00      20/10020       Solution::Solution(int, int, int) [8]
                0.00    0.00   10000/10020       Slot::Slot(int, int) [23]
[21]     0.0    0.00    0.00   10020         std::vector<bool, std::allocator<bool> >::_M_fill_insert(std::_Bit_iterator, unsigned long, bool) [21]
-----------------------------------------------
                0.00    0.00   10000/10000       Instance::Instance(std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> >) [36]
[22]     0.0    0.00    0.00   10000         Ad::Ad(int, int, int, int, int, int, double) [22]
-----------------------------------------------
                0.00    0.00   10000/10000       Solution::Solution(int, int, int) [8]
[23]     0.0    0.00    0.00   10000         Slot::Slot(int, int) [23]
                0.00    0.00   10000/10020       std::vector<bool, std::allocator<bool> >::_M_fill_insert(std::_Bit_iterator, unsigned long, bool) [21]
-----------------------------------------------
                0.00    0.00    1052/1052        void std::__introsort_loop<__gnu_cxx::__normal_iterator<std::pair<int, int>*, std::vector<std::pair<int, int>, std::allocator<std::pair<int, int> > > >, long, __gnu_cxx::__ops::_Iter_comp_iter<std::greater<std::pair<int, int> > > >(__gnu_cxx::__normal_iterator<std::pair<int, int>*, std::vector<std::pair<int, int>, std::allocator<std::pair<int, int> > > >, __gnu_cxx::__normal_iterator<std::pair<int, int>*, std::vector<std::pair<int, int>, std::allocator<std::pair<int, int> > > >, long, __gnu_cxx::__ops::_Iter_comp_iter<std::greater<std::pair<int, int> > >) [53]
[24]     0.0    0.00    0.00    1052         void std::__move_median_to_first<__gnu_cxx::__normal_iterator<std::pair<int, int>*, std::vector<std::pair<int, int>, std::allocator<std::pair<int, int> > > >, __gnu_cxx::__ops::_Iter_comp_iter<std::greater<std::pair<int, int> > > >(__gnu_cxx::__normal_iterator<std::pair<int, int>*, std::vector<std::pair<int, int>, std::allocator<std::pair<int, int> > > >, __gnu_cxx::__normal_iterator<std::pair<int, int>*, std::vector<std::pair<int, int>, std::allocator<std::pair<int, int> > > >, __gnu_cxx::__normal_iterator<std::pair<int, int>*, std::vector<std::pair<int, int>, std::allocator<std::pair<int, int> > > >, __gnu_cxx::__normal_iterator<std::pair<int, int>*, std::vector<std::pair<int, int>, std::allocator<std::pair<int, int> > > >, __gnu_cxx::__ops::_Iter_comp_iter<std::greater<std::pair<int, int> > >) [24]
-----------------------------------------------
                0.00    0.00      20/20          Solution::Solution(int, int, int) [8]
[25]     0.0    0.00    0.00      20         std::vector<Slot, std::allocator<Slot> >::_M_default_append(unsigned long) [25]
                0.00    0.00   10000/10500       Slot::Slot() [20]
-----------------------------------------------
                0.00    0.00      15/15          Instance::Instance(std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> >) [36]
[26]     0.0    0.00    0.00      15         void std::vector<Ad, std::allocator<Ad> >::_M_realloc_insert<Ad const&>(__gnu_cxx::__normal_iterator<Ad*, std::vector<Ad, std::allocator<Ad> > >, Ad const&) [26]
                0.00    0.00   16383/36383       Ad::~Ad() [19]
-----------------------------------------------
                0.00    0.00       1/1           __libc_csu_init [59]
[27]     0.0    0.00    0.00       1         _GLOBAL__sub_I__ZN8SolutionC2Eiii [27]
-----------------------------------------------
                0.00    0.00       1/1           __libc_csu_init [59]
[28]     0.0    0.00    0.00       1         _GLOBAL__sub_I_timeout [28]
-----------------------------------------------
                0.00    0.00       1/1           Instance::Instance(std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> >) [36]
[29]     0.0    0.00    0.00       1         Utils::getFileName(std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > const&) [29]
-----------------------------------------------
                0.00    0.00       1/1           Instance::Instance(std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> >) [36]
[30]     0.0    0.00    0.00       1         Utils::Utils() [30]
-----------------------------------------------
                0.00    0.00       1/1           Instance::~Instance() [37]
[31]     0.0    0.00    0.00       1         Utils::~Utils() [31]
-----------------------------------------------
                                1052             void std::__introsort_loop<__gnu_cxx::__normal_iterator<std::pair<int, int>*, std::vector<std::pair<int, int>, std::allocator<std::pair<int, int> > > >, long, __gnu_cxx::__ops::_Iter_comp_iter<std::greater<std::pair<int, int> > > >(__gnu_cxx::__normal_iterator<std::pair<int, int>*, std::vector<std::pair<int, int>, std::allocator<std::pair<int, int> > > >, __gnu_cxx::__normal_iterator<std::pair<int, int>*, std::vector<std::pair<int, int>, std::allocator<std::pair<int, int> > > >, long, __gnu_cxx::__ops::_Iter_comp_iter<std::greater<std::pair<int, int> > >) [53]
[53]     0.0    0.00    0.00       0+1052    void std::__introsort_loop<__gnu_cxx::__normal_iterator<std::pair<int, int>*, std::vector<std::pair<int, int>, std::allocator<std::pair<int, int> > > >, long, __gnu_cxx::__ops::_Iter_comp_iter<std::greater<std::pair<int, int> > > >(__gnu_cxx::__normal_iterator<std::pair<int, int>*, std::vector<std::pair<int, int>, std::allocator<std::pair<int, int> > > >, __gnu_cxx::__normal_iterator<std::pair<int, int>*, std::vector<std::pair<int, int>, std::allocator<std::pair<int, int> > > >, long, __gnu_cxx::__ops::_Iter_comp_iter<std::greater<std::pair<int, int> > >) [53]
                0.00    0.00    1052/1052        void std::__move_median_to_first<__gnu_cxx::__normal_iterator<std::pair<int, int>*, std::vector<std::pair<int, int>, std::allocator<std::pair<int, int> > > >, __gnu_cxx::__ops::_Iter_comp_iter<std::greater<std::pair<int, int> > > >(__gnu_cxx::__normal_iterator<std::pair<int, int>*, std::vector<std::pair<int, int>, std::allocator<std::pair<int, int> > > >, __gnu_cxx::__normal_iterator<std::pair<int, int>*, std::vector<std::pair<int, int>, std::allocator<std::pair<int, int> > > >, __gnu_cxx::__normal_iterator<std::pair<int, int>*, std::vector<std::pair<int, int>, std::allocator<std::pair<int, int> > > >, __gnu_cxx::__normal_iterator<std::pair<int, int>*, std::vector<std::pair<int, int>, std::allocator<std::pair<int, int> > > >, __gnu_cxx::__ops::_Iter_comp_iter<std::greater<std::pair<int, int> > >) [24]
                                1052             void std::__introsort_loop<__gnu_cxx::__normal_iterator<std::pair<int, int>*, std::vector<std::pair<int, int>, std::allocator<std::pair<int, int> > > >, long, __gnu_cxx::__ops::_Iter_comp_iter<std::greater<std::pair<int, int> > > >(__gnu_cxx::__normal_iterator<std::pair<int, int>*, std::vector<std::pair<int, int>, std::allocator<std::pair<int, int> > > >, __gnu_cxx::__normal_iterator<std::pair<int, int>*, std::vector<std::pair<int, int>, std::allocator<std::pair<int, int> > > >, long, __gnu_cxx::__ops::_Iter_comp_iter<std::greater<std::pair<int, int> > >) [53]
-----------------------------------------------

 This table describes the call tree of the program, and was sorted by
 the total amount of time spent in each function and its children.

 Each entry in this table consists of several lines.  The line with the
 index number at the left hand margin lists the current function.
 The lines above it list the functions that called this function,
 and the lines below it list the functions this one called.
 This line lists:
     index	A unique number given to each element of the table.
		Index numbers are sorted numerically.
		The index number is printed next to every function name so
		it is easier to look up where the function is in the table.

     % time	This is the percentage of the `total' time that was spent
		in this function and its children.  Note that due to
		different viewpoints, functions excluded by options, etc,
		these numbers will NOT add up to 100%.

     self	This is the total amount of time spent in this function.

     children	This is the total amount of time propagated into this
		function by its children.

     called	This is the number of times the function was called.
		If the function called itself recursively, the number
		only includes non-recursive calls, and is followed by
		a `+' and the number of recursive calls.

     name	The name of the current function.  The index number is
		printed after it.  If the function is a member of a
		cycle, the cycle number is printed between the
		function's name and the index number.


 For the function's parents, the fields have the following meanings:

     self	This is the amount of time that was propagated directly
		from the function into this parent.

     children	This is the amount of time that was propagated from
		the function's children into this parent.

     called	This is the number of times this parent called the
		function `/' the total number of times the function
		was called.  Recursive calls to the function are not
		included in the number after the `/'.

     name	This is the name of the parent.  The parent's index
		number is printed after it.  If the parent is a
		member of a cycle, the cycle number is printed between
		the name and the index number.

 If the parents of the function cannot be determined, the word
 `<spontaneous>' is printed in the `name' field, and all the other
 fields are blank.

 For the function's children, the fields have the following meanings:

     self	This is the amount of time that was propagated directly
		from the child into the function.

     children	This is the amount of time that was propagated from the
		child's children to the function.

     called	This is the number of times the function called
		this child `/' the total number of times the child
		was called.  Recursive calls by the child are not
		listed in the number after the `/'.

     name	This is the name of the child.  The child's index
		number is printed after it.  If the child is a
		member of a cycle, the cycle number is printed
		between the name and the index number.

 If there are any cycles (circles) in the call graph, there is an
 entry for the cycle-as-a-whole.  This entry shows who called the
 cycle (as parents) and the members of the cycle (as children.)
 The `+' recursive calls entry shows the number of function calls that
 were internal to the cycle, and the calls entry for each member shows,
 for that member, how many times it was called from other members of
 the cycle.

Copyright (C) 2012-2018 Free Software Foundation, Inc.

Copying and distribution of this file, with or without modification,
are permitted in any medium without royalty provided the copyright
notice and this notice are preserved.

Index by function name

  [27] _GLOBAL__sub_I__ZN8SolutionC2Eiii (Solution.cpp) [4] Slot::~Slot() [7] Solution::~Solution()
  [28] _GLOBAL__sub_I_timeout (main.cpp) [3] Slot::operator=(Slot const&) [26] void std::vector<Ad, std::allocator<Ad> >::_M_realloc_insert<Ad const&>(__gnu_cxx::__normal_iterator<Ad*, std::vector<Ad, std::allocator<Ad> > >, Ad const&)
  [22] Ad::Ad(int, int, int, int, int, int, double) [29] Utils::getFileName(std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > const&) [25] std::vector<Slot, std::allocator<Slot> >::_M_default_append(unsigned long)
  [19] Ad::~Ad()              [30] Utils::Utils()          [9] std::vector<Slot, std::allocator<Slot> >::operator=(std::vector<Slot, std::allocator<Slot> > const&)
  [18] Slot::addCopy(Ad&)     [31] Utils::~Utils()        [21] std::vector<bool, std::allocator<bool> >::_M_fill_insert(std::_Bit_iterator, unsigned long, bool)
   [5] Slot::Slot(Slot const&) [1] Solution::addWorstFit(Ad&) [24] void std::__move_median_to_first<__gnu_cxx::__normal_iterator<std::pair<int, int>*, std::vector<std::pair<int, int>, std::allocator<std::pair<int, int> > > >, __gnu_cxx::__ops::_Iter_comp_iter<std::greater<std::pair<int, int> > > >(__gnu_cxx::__normal_iterator<std::pair<int, int>*, std::vector<std::pair<int, int>, std::allocator<std::pair<int, int> > > >, __gnu_cxx::__normal_iterator<std::pair<int, int>*, std::vector<std::pair<int, int>, std::allocator<std::pair<int, int> > > >, __gnu_cxx::__normal_iterator<std::pair<int, int>*, std::vector<std::pair<int, int>, std::allocator<std::pair<int, int> > > >, __gnu_cxx::__normal_iterator<std::pair<int, int>*, std::vector<std::pair<int, int>, std::allocator<std::pair<int, int> > > >, __gnu_cxx::__ops::_Iter_comp_iter<std::greater<std::pair<int, int> > >)
  [20] Slot::Slot()            [6] Solution::exportToFile(std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> >)
  [23] Slot::Slot(int, int)    [2] Solution::sortAdd(int)
