#include "Slot.h"

using namespace std;

Slot::Slot(){

}

Slot::Slot(int j, int A){
  this->s = 0;
  this->id = j;
  this->ads.resize(A, false);
}

void Slot::addCopy(Ad & ad){
  this->s += ad.s;
  this->ads[ad.id] = true;
}

void Slot::removeCopy(Ad & ad){
  this->s -= ad.s;
  this->ads[ad.id] = false;
}

Slot::~Slot(){

}
