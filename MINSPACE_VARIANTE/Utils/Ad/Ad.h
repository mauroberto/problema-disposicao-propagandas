#ifndef AD_H
#define AD_H

class Ad{
public:
  int id;
  int s;
  int wMax;
  int wMin;
  int rd;
  int dl;
  double v;

  Ad();
  Ad(int id, int s, int wMax, int wMin, int rd, int dl, double v);
	~Ad();
};

#endif
