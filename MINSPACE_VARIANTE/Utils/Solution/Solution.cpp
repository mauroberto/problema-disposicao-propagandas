#include "Solution.h"
#include <iostream>
#include <fstream>

using namespace std;

Solution::Solution(int N, int A, int S){
  this->A = A;
  this->N = N;
  this->S = S;
  this->slots.resize(N);
  this->y.resize(A, false);
  this->value = 0;

  for(int j = 0; j < N; j++) {
    this->slots[j] = Slot(j, A);
  }
}

Solution::Solution(){

}

Solution::~Solution(){

}

void Solution::sortAdd(int j){
  if(j <= 0 || j > this->N-1) return;

  Slot aux = this->slots[j];

  int i = j-1;
  while(i >= 0){
    if(this->slots[i].s < aux.s){
      this->slots[i+1] = this->slots[i];
      i--;
    }else{
      this->slots[i+1] = aux;
      break;
    }
  }

  if(i < 0){
    this->slots[0] = aux;
  }
}

void Solution::sortRemove(int j){
  if(j >= N - 1 || j < 0) return;

  Slot aux = this->slots[j];

  int i = j+1;
  while(i < N){
    if(this->slots[i].s > aux.s){
      this->slots[i-1] = this->slots[i];
      i++;
    } else{
      this->slots[i-1] = aux;
      break;
    }
  }

  if(i >= N){
    this->slots[N-1] = aux;
  }
}


bool Solution::addFirstFit(Ad & ad){
  if(this->y[ad.id]){
    cout << "WTF " << ad.id << endl;
  }

  int count = 0;
  for(int j = ad.rd; j < this->N && count < ad.wMin; j++){
    if(this->slots[j].s + ad.s <= this->S){
      count++;
    }
  }

  if(count >= ad.wMin){
    this->y[ad.id] = true;

    for(int j = ad.rd, c = 0; j < this->N && c < ad.wMin; j++){
      if(this->slots[j].s + ad.s <= this->S){
        this->addCopy(ad, j);
        c++;
      }
    }
    return true;
  }
  return false;
}

bool Solution::addBestFit(Ad & ad){
  if(this->y[ad.id]){
    cout << "WTF " << ad.id << endl;
  }

  int count = 0;
  for(int j = 0; j < this->N && count < ad.wMin; j++){
    if(this->slots[j].s + ad.s <= this->S && this->slots[j].id >= ad.rd){
      count++;
    }
  }

  if(count >= ad.wMin){
    this->y[ad.id] = true;

    for(int j = 0, c = 0; j < this->N && c < ad.wMin; j++){
      if(!this->slots[j].ads[ad.id] && this->slots[j].s + ad.s <= this->S && this->slots[j].id >= ad.rd){
        this->addCopy(ad, j);
        c++;

        this->sortAdd(j);
      }
    }
    return true;
  }
  return false;
}

bool Solution::addWorstFit(Ad & ad){
  if(this->y[ad.id]){
    cout << "WTF " << ad.id << endl;
  }

  int count = 0;
  for(int j = this->N - 1; j >= 0 && count < ad.wMin; j--){
    if(this->slots[j].s + ad.s <= this->S && this->slots[j].id >= ad.rd){
      count++;
    }
  }

  if(count >= ad.wMin){
    this->y[ad.id] = true;

    for(int j = this->N - 1, c = 0; j >= 0 && c < ad.wMin; j--){
      if(!this->slots[j].ads[ad.id] && this->slots[j].s + ad.s <= this->S && this->slots[j].id >= ad.rd){
        this->addCopy(ad, j);
        c++;
        this->sortAdd(j);
        j++;
      }
    }
    return true;
  }
  return false;
} 

void Solution::addCopy(Ad & ad, int j){
  this->slots[j].addCopy(ad);
  if(this->slots[j].s > this->value){
    this->value = this->slots[j].s;
  }
}

void Solution::remakeSol(){
  vector<Slot> slots(this->N);

  for(int j=0; j < this->N; j++){
    slots[this->slots[j].id] = this->slots[j];
  }

  for(int j=0; j < this->N; j++){
    this->slots[j] = slots[j];
  }
}

void Solution::exportToFile(const string path){

  ofstream opf;
  opf.open(path);

  remakeSol();

  opf << this->value << endl;
  for(int j = 0; j < this->N; j++){
    opf << j << " ";
    for(int i = 0; i < this->A; i++){
      if(slots[j].ads[i]){
        opf << i << " ";
      }
    }
    opf << endl;
  }


  opf.close();
}