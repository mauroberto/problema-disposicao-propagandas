#ifndef INSTANCE_H
#define INSTANCE_H
#include <vector>
#include <string>
#include "Ad.h"
#include "Utils.h"

class Instance{
public:
	int A;
	int N;
	int S;
	std::vector<Ad> ads;
  	std::string instanceName;
	Utils utils;
	
	Instance(std::string instanceName);
	Instance();
	~Instance();
};

#endif
