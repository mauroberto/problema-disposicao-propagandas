#include <iostream>
#include <vector>
#include <fstream>
#include <string>
#include <sstream>
#include <cmath>

#define EPSILON 1e-7

using namespace std;

typedef struct ad{
  int id;
  int s;
  int wMax;
  int wMin;
  int rd;
  int dl;
  double v;
  int qt;
  int first;
  int last;
} Ad;

typedef struct slot{
  int id;
  vector<int> ads;
  int S;
  int * appears;
} Slot;

int A, N, S;
vector<Ad> ads;

double V;
vector<Slot> sol;

int errors = 0;

void readFile(char * fn) {
  ifstream infile(fn);

  infile >> A >> N >> S;

  int id, s, wMax, wMin, rd, dl;
  double v;

  while(infile >> id >> s >> rd >> dl >> wMin >> wMax >> v){
    Ad ad;
    ad.id = id;
    ad.s = s;
    ad.wMax = wMax;
    ad.wMin = wMin;
    ad.rd = rd;
    ad.dl = dl;
    ad.v = v;
    ad.qt = 0;
    ad.first = N-1;
    ad.last = 0;

    ads.push_back(ad);
  }
}

void readSol(char * fn) {
  ifstream infile(fn);

  string line;
  getline(infile, line);
  istringstream iss(line);
  iss >> V;

  int id;
  while(getline(infile, line)){
    Slot slot;
    istringstream iss(line);
    iss >> slot.id;
    slot.S = 0;
    slot.appears = new int[A];

    for(int i=0; i<A; i++){
      slot.appears[i] = 0;
    }

    int aId;
    while(iss >> aId){
      slot.ads.push_back(aId);
      slot.appears[aId]++;
      slot.S += ads[aId].s;
      ads[aId].qt ++;

      if(ads[aId].first > slot.id){
        ads[aId].first = slot.id;
      }

      if(ads[aId].last < slot.id){
        ads[aId].last = slot.id;
      }

    }

    sol.push_back(slot);
  }
}

void verNumberSlot(){
  int numSlots = sol.size();
  if(numSlots != N){
    cout << "Error " << errors << ": Number of slots != N -- " << numSlots << " | " <<  N << endl;
    errors++;
  }
}

void verSlotSize(){
  for(int i=0; i<N; i++){
    if(sol[i].S > S){
      cout << "Error " << errors << ": Slot " << i << " has size > S -- " << sol[i].S << " | " <<  S << endl;
      errors++;
    }
  }
}

void verWMax(){
  for(int i=0; i<A; i++){
    if(ads[i].qt == 0){
      continue;
    }

    if(ads[i].qt > ads[i].wMax){
      cout << "Error " << errors << ": The ad " << i << " appears more then wMax times -- " << ads[i].qt << " | " <<  ads[i].wMax << endl;
      errors++;
    }
  }
}

void verWMin(){
  for(int i=0; i<A; i++){
    if(ads[i].qt == 0){
      continue;
    }

    if(ads[i].qt < ads[i].wMin){
      cout << "Error " << errors << ": The ad " << i << " appears less then wMin times -- " << ads[i].qt << " | " <<  ads[i].wMin << endl;
      errors++;
    }
  }
}

void verW(){
  for(int i=0; i<A; i++){
    if(ads[i].qt == 0){
      continue;
    }

    if(ads[i].qt != ads[i].wMin){
      cout << "Error " << errors << ": The ad " << i << " frequency not equals to w_i -- " << ads[i].qt << " | " <<  ads[i].wMin << endl;
      errors++;
    }
  }
}

void verDeadline(){
  for(int i=0; i<A; i++){
    if(ads[i].qt == 0){
      continue;
    }

    if(ads[i].last > ads[i].dl){
      cout << "Error " << errors << ": The ad " << i << " appears after its deadline -- " << ads[i].last << " | " <<  ads[i].dl << endl;
      errors++;
    }
  }
}

void verReleaseDate(){
  for(int i=0; i<A; i++){
    if(ads[i].qt == 0){
      continue;
    }

    if(ads[i].first < ads[i].rd){
      cout << "Error " << errors << ": The ad " << i << " appears before its release date -- " << ads[i].first << " | " <<  ads[i].rd << endl;
      errors++;
    }
  }
}

bool AreSame(double a, double b){
    return fabs(a - b) >= EPSILON;
}

void verValue(){
  double value = 0.0;
  for(int j=0; j<N; j++){
    Slot slot = sol[j];
    for(int i=0, length = slot.ads.size(); i<length; i++){
      int index = slot.ads[i];
      value = value + ads[index].v;
    }
  }

  if(AreSame(value, V)){
    cout << fixed << "Error " << errors << ": The solution value " << value << " isn't corret -- " << value << " | " <<  V << endl;
    errors++;
  }
}

void verValue2(){
  double value = 0.0;
  for(int j=0; j<N; j++){
    Slot slot = sol[j];
    for(int i=0, length = slot.ads.size(); i<length; i++){
      int index = slot.ads[i];
      value = value + ads[index].s;
    }
  }

  if(AreSame(value, V)){
    cout << fixed << "Error " << errors << ": The solution value " << value << " isn't corret -- " << value << " | " <<  V << endl;
    errors++;
  }
}


void verAppearsOncePerSlot(){
  for(int j=0; j<N; j++){
    for(int i=0; i<A; i++){
      if(sol[j].appears[i] > 1){
        cout << "Error " << errors << ": Ad " << i << " appears more then once in slot " << j << endl;
        errors++;
      }
    }
  }
}

int main(int argc, char *argv[]){

  if(argc < 4) {
		printf("Usage: ./VER instance solution algorithm\n");
		exit(1);
  }

  readFile(argv[1]);
  readSol(argv[2]);

  int alg = atoi(argv[3]);

  if(alg == 1){
    verNumberSlot();
    verSlotSize();
    verWMax();
    verWMin();
    verDeadline();
    verReleaseDate();
    verValue();
    verAppearsOncePerSlot();
  } else if(alg == 2){
    verNumberSlot();
    verSlotSize();
    verW();
    verValue2();
    verAppearsOncePerSlot();
  } else {
    cout << "Invalid Algorithm!" << endl;
  }

  cout << "Number of errors: " << errors << endl;


  if(errors == 0){
    cout << "Solution OK!" << endl;
  }

  return 0;
}
