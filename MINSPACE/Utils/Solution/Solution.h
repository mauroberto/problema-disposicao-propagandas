#ifndef SOLUTION_H
#define SOLUTION_H
#include "Slot.h"
//#include "BITree.h"
#include <vector>
#include <string>

class Solution {
public:
  int A, N, S, value;
  std::vector<Slot> slots;
  std::vector<bool> y;

  Solution();
  Solution(int N, int A, int S);
  bool addBestFit(Ad & ad);
  bool addWorstFit(Ad & ad);
  bool addFirstFit(Ad & ad);
  void addCopy(Ad & ad, int j);
  void remakeSol();
  long int spaceSolution();
  void exportToFile(const std::string path);
	~Solution();

private:
  void sortRemove(int j);
  void sortAdd(int j);
};

#endif
