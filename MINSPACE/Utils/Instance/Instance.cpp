#include "Instance.h"
#include <fstream>

using namespace std;

Instance::Instance(string instanceName){
  ifstream infile(instanceName);

  this->instanceName = this->utils.getFileName(instanceName);

  infile >> this->A >> this->N >> this->S;

  int id, s, wMax, wMin, rd, dl;
  double v;

  while(infile >> id >> s >> rd >> dl >> wMin >> wMax >> v){
    Ad ad = Ad(id, s, wMax, wMin, rd, dl, v);
    this->ads.push_back(ad);
  }
}

Instance::Instance(){

}

Instance::~Instance(){

}