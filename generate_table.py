import os
import matplotlib.pyplot as plt
import numpy as np
import subprocess as commands
import pandas as pd
import time
import warnings
warnings.filterwarnings("always")
warnings.filterwarnings("ignore", category=DeprecationWarning)

date = time.strftime("%Y_%m_%d")

classesMAXSPACE = [1, 5, 9, 13, 17, 21, 25, 29, 33]
classesMAXSPACE_VARIANTE = [2, 3, 4, 6, 7, 8, 10, 11, 12, 14, 15, 16, 18, 19, 20, 22, 23, 24, 26, 27, 28, 30, 31, 32, 34, 35, 36] 
classesMAXSPACE_VARIANTE2 = [39, 40, 41, 42, 43, 44, 45, 46, 47]

instances_classes1 = []

instances_classes2 = [
    ("AI 202", ["201_2500_DI_"]),
    ("AI 403", ["402_10000_DI_"]),
    ("AI 601", ["600_20000_DI_"]),
    ("AI 802", ["801_40000_DI_"]),
    ("AI 1003", ["1002_80000_DI_"]),
    ("ANI 201", ["201_2500_NR_"]), 
    ("ANI 402", ["402_10000_NR_"]),
    ("ANI 600", ["600_20000_NR_"]),
    ("ANI 801", ["801_40000_NR_"]),
    ("ANI 1002", ["1002_80000_NR_"]),
    ("line", []),
    ("FalkenauerT 60", ["Falkenauer_t60_"]),
    ("FalkenauerT 120", ["Falkenauer_t120_"]),
    ("FalkenauerT 249", ["Falkenauer_t249_"]),
    ("FalkenauerT 501", ["Falkenauer_t501_"]),
    ("FalkenauerU 120", ["Falkenauer_u120"]), 
    ("FalkenauerU 250", ["Falkenauer_u250"]),
    ("FalkenauerU 500", ["Falkenauer_u500"]),
    ("FalkenauerU 1000", ["Falkenauer_u1000"]),
    ("line", []),
    ("Hard", ["Hard28"]),
    ("Irnich", ["csAA", "csAB", "csBA", "csBB"]),
    ("Scholl 1", ["N1C", "N2C", "N3C", "N4C"]),
    ("Scholl 2", ["N1W", "N2W", "N3W", "N4W"]),
    ("Scholl 3", ["HARD"]),
    ("Schwerin", ["Schwerin"]),
    ("Wäscher", ["Waescher"]),
    ("line", []),
    ("Random 100", ["100_75_50_"]),
    ("Random 500", ["500_250_100_"]),
    ("Random 1000", ["1000_500_250_"]),
    ("Random 10000", ["10000_500_200_"])
]

PATH1 = "MAXSPACE_VARIANTE/"
PATH2 = "MAXSPACE/"

algorithms1 = [
    ("Hybrid-GA", PATH1+"Hybrid-GA", "Hybrid-GA"), 
    ("GRASP", PATH1+"GRASP", "GRASP"),
    ("GRASP+VNS", PATH1+"GRASP-VNS", "GRASP-VNS"),
    ("GRASP+Tabu", PATH1+"GRASP-Tabu", "GRASP-Tabu"),
    ("VNS", PATH1+"VNS", "VNS"),
    ("LP-UB", PATH1+"PLI", "PLI")
]

algorithms2 = [
    ("Hybrid-GA", PATH2+"Hybrid-GA", "Hybrid-GA"), 
    ("GRASP", PATH2+"GRASP", "GRASP"),
    ("GRASP+VNS", PATH2+"GRASP-VNS", "GRASP-VNS"),
    ("GRASP+Tabu", PATH2+"GRASP-Tabu", "GRASP-Tabu"),
    ("VNS", PATH2+"VNS", "VNS"),
    ("LP-UB", PATH2+"PLI", "PLI")
]

csv_sol = "2023_07_12"
csv_time = "2023_07_12"
#csv_column_vars = "2024_02_21"
#csv_timeout = "2024_02_21"

data_sol1 = pd.read_csv(PATH1+"csv/sol/%s.csv" % csv_sol, index_col=False, header=0)
data_time1 = pd.read_csv(PATH1+"csv/time/%s.csv" % csv_time, index_col=False, header=0)

data_sol2 = pd.read_csv(PATH2+"csv/sol/%s.csv" % csv_sol, index_col=False, header=0)
data_time2 = pd.read_csv(PATH2+"csv/time/%s.csv" % csv_time, index_col=False, header=0)

def groupInstances(data, inst):
    filter1 = data['instances'].str.contains("|".join(inst))
    df_ = data[filter1]

    return df_

def generateTable(data_sol_, data_time_, instances_classes_, algorithms_):
    header_table = """
    \\begin{table}
    \\begin{scriptsize}
    \\begin{tabular}{@{\extracolsep{\\fill}}lrrrrrrr}
    \\toprule
    \multicolumn{1}{c}{} & \multicolumn{1}{c}{} & \multicolumn{1}{c}{\\textbf{H-GA}} & \multicolumn{1}{c}{\\textbf{GRASP}} & \multicolumn{1}{c}{\\textbf{G\plus{VNS}}} & \multicolumn{1}{c}{\\textbf{G\plus{Tabu}}} & \multicolumn{1}{c}{\\textbf{VNS}} & \multicolumn{1}{c}{\\textbf{LP-UB}}\\\\ \cmidrule(lr){3-3}\cmidrule(lr){4-4}\cmidrule(lr){5-5}\cmidrule(lr){6-6}\cmidrule(lr){7-7}\cmidrule(lr){8-8}
    \\textbf{Class}    & \\textbf{Total}     & \\textbf{Gap \%} & \\textbf{Gap \%}  & \\textbf{Gap \%}  & \\textbf{Gap \%}  & \\textbf{Gap \%} & \\textbf{Bound} \\\\ \midrule
    """

    body_table = ""

    for (classe, prefix_class) in instances_classes_:
        if classe == "line" and len(prefix_class) == 0:
            body_row = "\midrule"
            body_table = body_table + "\n" + body_row
            continue

        sol_prefix = groupInstances(data_sol_, prefix_class)
        time_prefix = groupInstances(data_time_, prefix_class)

        body_row = classe + " & " + "$%d$" % sol_prefix.shape[0] + " & "

        sols = []
        times = []
        gaps = []

        for (alg, path, alg_name) in algorithms_:
            sol_ = sol_prefix[alg].mean()

            sols.append(sol_)

            time_ = time_prefix[alg].mean()
            times.append(time_)


        for idx, (alg, path, alg_name) in enumerate(algorithms_[:-1]):
            gap_ = ((sols[-1] - sols[idx])/(sols[-1]*1.0))*100.0
            gaps.append(gap_)

        idx_col = 0
        for idx, (alg, path, alg_name) in enumerate(algorithms_[:-1]):
            gap_ = " -- "
            if gaps[idx] >= 0:
                gap_ = "$\\textbf{%.2f}$" % gaps[idx] if gaps[idx] == min(gaps) else "$%.2f$" % gaps[idx]

            #time_ = " - "
            #if times[idx] >= 0:
                #time_ = "$\\textbf{%.1f}$" % times[idx] if times[idx] == min(n for n in times[:-1] if n >= 0) else "$%.1f$" % times[idx]

            body_row = body_row + gap_ #+ " & " + time_    

            body_row = body_row + " & "
        
        body_row = body_row + "%d" % sols[-1]

        
        body_row = body_row + " \\\\ "
            
        body_table = body_table + "\n" + body_row


    footer_table = """
    \\bottomrule
    \end{tabular}
    %\caption{Comparação dos algoritmos desenvolvidos (tempo limite de $600$s).}
    \label{table:results}
    \end{scriptsize}
    \end{table}
    """

    table = header_table + body_table + footer_table

    print(table)


sizes = [
    (100, 75, 50),
    (500, 250, 100),
    (1000, 500, 250),
    (10000, 500, 200)
]

for classe in classesMAXSPACE_VARIANTE:
    for tam in sizes:
        inst = "%d_%d_%d_%d_" % (tam[0], tam[1], tam[2], classe)
        instances_classes1.append(("%d %d" % (classe, tam[0]), [inst]))
    instances_classes1.append(("line", []))


generateTable(data_sol2, data_time2, instances_classes2, algorithms2)


generateTable(data_sol1, data_time1, instances_classes1, algorithms1)

'''
import os
import matplotlib.pyplot as plt
import numpy as np
import subprocess as commands
import pandas as pd
import time
import warnings
warnings.filterwarnings("always")
warnings.filterwarnings("ignore", category=DeprecationWarning)

date = time.strftime("%Y_%m_%d")

classesMAXSPACE = [1, 5, 9, 13, 17, 21, 25, 29, 33]
classesMAXSPACE_VARIANTE = [2, 3, 4, 6, 7, 8, 10, 11, 12, 14, 15, 16, 18, 19, 20, 22, 23, 24, 26, 27, 28, 30, 31, 32, 34, 35, 36] 
classesMAXSPACE_VARIANTE2 = [39, 40, 41, 42, 43, 44, 45, 46, 47]

instances_classes1 = []

instances_classes2 = [
    ("AI 202", ["201_2500_DI_"]),
    ("AI 403", ["402_10000_DI_"]),
    ("AI 601", ["600_20000_DI_"]),
    ("AI 802", ["801_40000_DI_"]),
    ("AI 1003", ["1002_80000_DI_"]),
    ("ANI 201", ["201_2500_NR_"]), 
    ("ANI 402", ["402_10000_NR_"]),
    ("ANI 600", ["600_20000_NR_"]),
    ("ANI 801", ["801_40000_NR_"]),
    ("ANI 1002", ["1002_80000_NR_"]),
    ("line", []),
    ("FalkenauerT 60", ["Falkenauer_t60_"]),
    ("FalkenauerT 120", ["Falkenauer_t120_"]),
    ("FalkenauerT 249", ["Falkenauer_t249_"]),
    ("FalkenauerT 501", ["Falkenauer_t501_"]),
    ("FalkenauerU 120", ["Falkenauer_u120"]), 
    ("FalkenauerU 250", ["Falkenauer_u250"]),
    ("FalkenauerU 500", ["Falkenauer_u500"]),
    ("FalkenauerU 1000", ["Falkenauer_u1000"]),
    ("line", []),
    ("Hard", ["Hard28"]),
    ("Irnich", ["csAA", "csAB", "csBA", "csBB"]),
    ("Scholl 1", ["N1C", "N2C", "N3C", "N4C"]),
    ("Scholl 2", ["N1W", "N2W", "N3W", "N4W"]),
    ("Scholl 3", ["HARD"]),
    ("Schwerin", ["Schwerin"]),
    ("Wäscher", ["Waescher"]),
    ("line", []),
    ("Random 100", ["100_75_50_"]),
    ("Random 500", ["500_250_100_"]),
    ("Random 1000", ["1000_500_250_"]),
    ("Random 10000", ["10000_500_200_"])
]

PATH1 = "MAXSPACE_VARIANTE/"
PATH2 = "MAXSPACE/"

algorithms1 = [
    ("Hybrid-GA", PATH1+"Hybrid-GA", "Hybrid-GA"), 
    ("GRASP", PATH1+"GRASP", "GRASP"),
    ("GRASP+VNS", PATH1+"GRASP-VNS", "GRASP-VNS"),
    ("GRASP+Tabu", PATH1+"GRASP-Tabu", "GRASP-Tabu"),
    ("VNS", PATH1+"VNS", "VNS"),
    ("LP-UB", PATH1+"PLI", "PLI")
]

algorithms2 = [
    ("Hybrid-GA", PATH2+"Hybrid-GA", "Hybrid-GA"), 
    ("GRASP", PATH2+"GRASP", "GRASP"),
    ("GRASP+VNS", PATH2+"GRASP-VNS", "GRASP-VNS"),
    ("GRASP+Tabu", PATH2+"GRASP-Tabu", "GRASP-Tabu"),
    ("VNS", PATH2+"VNS", "VNS"),
    ("LP-UB", PATH2+"PLI", "PLI")
]

csv_sol = "2023_07_12"
csv_time = "2023_07_12"
#csv_column_vars = "2024_02_21"
#csv_timeout = "2024_02_21"

data_sol1 = pd.read_csv(PATH1+"csv/sol/%s.csv" % csv_sol, index_col=False, header=0)
data_time1 = pd.read_csv(PATH1+"csv/time/%s.csv" % csv_time, index_col=False, header=0)

data_sol2 = pd.read_csv(PATH2+"csv/sol/%s.csv" % csv_sol, index_col=False, header=0)
data_time2 = pd.read_csv(PATH2+"csv/time/%s.csv" % csv_time, index_col=False, header=0)

def groupInstances(data, inst):
    filter1 = data['instances'].str.contains("|".join(inst))
    df_ = data[filter1]

    return df_

def generateTable(data_sol_, data_time_, instances_classes_, algorithms_):
    header_table = """
    \\begin{table}
    \\begin{scriptsize}
    \\begin{tabular}{@{\extracolsep{\\fill}}lrrrrrrr}
    \\toprule
    \multicolumn{1}{c}{} & \multicolumn{1}{c}{} & \multicolumn{1}{c}{\\textbf{H-GA}} & \multicolumn{1}{c}{\\textbf{GRASP}} & \multicolumn{1}{c}{\\textbf{G\plus{VNS}}} & \multicolumn{1}{c}{\\textbf{G\plus{Tabu}}} & \multicolumn{1}{c}{\\textbf{VNS}} & \multicolumn{1}{c}{\\textbf{LP-UB}}\\\\ \cmidrule(lr){3-3}\cmidrule(lr){4-4}\cmidrule(lr){5-5}\cmidrule(lr){6-6}\cmidrule(lr){7-7}\cmidrule(lr){8-8}
    \\textbf{Class}    & \\textbf{Total}     & \\textbf{Gap \%} & \\textbf{Gap \%}  & \\textbf{Gap \%}  & \\textbf{Gap \%}  & \\textbf{Gap \%} & \\textbf{Bound} \\\\ \midrule
    """

    body_table = ""

    for (classe, prefix_class) in instances_classes_:
        if classe == "line" and len(prefix_class) == 0:
            body_row = "\midrule"
            body_table = body_table + "\n" + body_row
            continue

        
        gaps = []
        gap_aux = {}

        total_instances = 0

        for idx, (alg, path, alg_name) in enumerate(algorithms_[:-1]):
            gap_aux[alg] = 0

        for inst in prefix_class:
            sol_prefix = groupInstances(data_sol_, [inst])
            time_prefix = groupInstances(data_time_, [inst])

            total_instances = total_instances + sol_prefix.shape[0]

            sols = []
            times = []

            for (alg, path, alg_name) in algorithms_:
                sol_ = sol_prefix[alg].mean()

                sols.append(sol_)

                time_ = time_prefix[alg].mean()
                times.append(time_)


            for idx, (alg, path, alg_name) in enumerate(algorithms_[:-1]):
                gap_aux[alg] = gap_aux[alg] + ((sols[-1] - sols[idx])/(sols[-1]*1.0))*100.0
        
        for idx, (alg, path, alg_name) in enumerate(algorithms_[:-1]):
            gaps.append(gap_aux[alg]/len(prefix_class))

        body_row = classe + " & " + "$%d$" % total_instances + " & "

        idx_col = 0
        for idx, (alg, path, alg_name) in enumerate(algorithms_[:-1]):
            gap_ = " -- "
            if gaps[idx] >= 0:
                gap_ = "$\\textbf{%.2f}$" % gaps[idx] if gaps[idx] == min(gaps) else "$%.2f$" % gaps[idx]

            #time_ = " - "
            #if times[idx] >= 0:
                #time_ = "$\\textbf{%.1f}$" % times[idx] if times[idx] == min(n for n in times[:-1] if n >= 0) else "$%.1f$" % times[idx]

            body_row = body_row + gap_ #+ " & " + time_    

            if idx < len(algorithms_[:-1]):
                body_row = body_row + " & "
        
        #body_row = body_row + "%d" % sols[-1]
        
        body_row = body_row + " \\\\ "
            
        body_table = body_table + "\n" + body_row


    footer_table = """
    \\bottomrule
    \end{tabular}
    %\caption{Comparação dos algoritmos desenvolvidos (tempo limite de $600$s).}
    \label{table:results}
    \end{scriptsize}
    \end{table}
    """

    table = header_table + body_table + footer_table

    print(table)


sizes = [
    (100, 75, 50),
    (500, 250, 100),
    (1000, 500, 250),
    (10000, 500, 200)
]

for classe in classesMAXSPACE_VARIANTE:
    insts = []
    for tam in sizes:
        inst = "%d_%d_%d_%d_" % (tam[0], tam[1], tam[2], classe)
        insts.append(inst)

    instances_classes1.append(("%d" % (classe), insts))
    #instances_classes1.append(("line", []))


generateTable(data_sol2, data_time2, instances_classes2, algorithms2)


generateTable(data_sol1, data_time1, instances_classes1, algorithms1)
'''