import seaborn as sns
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

def plot(com_bit, sem_bit, alg, corte, loc, log = False):
  plt.figure(figsize=(15,15))
  fontSize = 12
  fontSizeTitle = 12

  data = {}
  data["instances"] = com_bit["instances"]
  data["iteracoes"] = com_bit["iteracoes"]
  data["iteracoes-BIT"] = sem_bit["iteracoes"]

  df = pd.DataFrame(data=data)

  #removendo instâncias com menos de corte iterações
  df = df[df['iteracoes'] > corte]

  df.sort_values(["iteracoes-BIT"], axis=0, ascending=[True], inplace=True)

  indexes = []
  for i in range(0, len(df['instances'])):
    indexes.append(i)

  df['x1'] = indexes

  sns.set(style="white")

  gfg = sns.relplot(x="x1", y="iteracoes", data=df)
  sns.lineplot(x="x1", y="iteracoes-BIT", data=df, color='red')

  if log:
    gfg.set(yscale="log")

  gfg.set(xlabel ="Instâncias", ylabel = "Iterações", title = alg)

  plt.legend(title='BIT', loc=loc, labels=['Sem BIT', 'Com BIT'])
  plt.show()


com_bit = pd.read_csv("MAXSPACE_VARIANTE/csv/BIT/GRASP+Tabu/2022_07_07.csv", index_col=False, header=0)
sem_bit = pd.read_csv("MAXSPACE_VARIANTE/csv/BIT/GRASP+Tabu/2022_07_08.csv", index_col=False, header=0)
plot(com_bit, sem_bit, "GRASP+Tabu", 10, 'upper left')


com_bit = pd.read_csv("MAXSPACE_VARIANTE/csv/BIT/GRASP+VNS/2022_07_07.csv", index_col=False, header=0)
sem_bit = pd.read_csv("MAXSPACE_VARIANTE/csv/BIT/GRASP+VNS/2022_07_08.csv", index_col=False, header=0)
plot(com_bit, sem_bit, "GRASP+VNS", 10, 'upper right')

com_bit = pd.read_csv("MAXSPACE_VARIANTE/csv/BIT/GRASP/2022_07_07.csv", index_col=False, header=0)
sem_bit = pd.read_csv("MAXSPACE_VARIANTE/csv/BIT/GRASP/2022_07_08.csv", index_col=False, header=0)
plot(com_bit, sem_bit, "GRASP", 10, 'upper left')
plot(com_bit, sem_bit, "GRASP", 100000, 'upper left')
plot(com_bit, sem_bit, "GRASP", 10, 'upper left', True)