#include <stdio.h>
#include <cstdlib>
#include <ctime>
#include "LocalSearch.h"
#include "Instance.h"
#include "Solution.h"
#include "ConstructiveHeuristic.h"
#include "SolutionCompare.h"
#include "MaximizeResidue.h"
#include <time.h>
#include <sys/time.h>
#include <omp.h>

using namespace std;

#ifndef PATHSOL
#define PATHSOL "./solutions"
#endif

#ifdef _OPENMP
#define MAXT 4
#else
#define MAXT 1
#endif

Instance ins;

int timeout;
struct timeval start, stop;

int main(int argc, char* argv[]){
  if (argc < 2) {
    cout << "Usage: ./GRASP filename [timeout it alpha]" << endl;
    return 1;
  }

  gettimeofday(&start, NULL);

  timeout = 3600;
  if(argc > 2){
	  timeout = stoi(argv[2]);
  }

	cout.precision(2);

  srand((int)time(0));

  Instance ins(argv[1]);
  LocalSearch localSearch(&ins, timeout, start);
  ConstructiveHeuristic ch(ins);
  SolutionCompare * compare = new MaximizeResidue();
  SolutionCompare * compare2 = new SolutionCompare();

  int it = 2000, Nmax = 4;
  if(argc > 3){
    it = stoi(argv[3]);
  }
  double alpha = 0.4;
  if(argc > 4){
   alpha = strtod(argv[4], NULL);
  }

  Solution bestSol[MAXT];

  #ifdef _OPENMP
		#pragma omp parallel for num_threads(MAXT)
	#endif
  for(int i = 0; i < MAXT; i++){
    bestSol[i] = Solution(ins.N, ins.A, ins.S, &ins);
  }

  struct timeval stop;
  gettimeofday(&stop, NULL);

  double timeToTarget = ((double) (stop.tv_sec * 1000000 + stop.tv_usec) - (start.tv_sec * 1000000 + start.tv_usec)) / 1000000;

  int i = 0, bestI = 0;

  #ifdef _OPENMP
    #pragma omp parallel for num_threads(MAXT)
  #endif
  for(i=0; i < it; i++){
    gettimeofday(&stop, NULL);
    double elapsed = ((double) (stop.tv_sec * 1000000 + stop.tv_usec) - (start.tv_sec * 1000000 + start.tv_usec)) / 1000000;
    if(elapsed < timeout){
      #ifdef _OPENMP
        int tid = omp_get_thread_num();
      #else
        int tid = 0;
      #endif
      Solution sol = ch.greedyRandomizedConstruction3(alpha);

      double v;
      do{
        localSearch.BestImprovement(sol);

        v = sol.value;

        sol.setCompare(compare);

        localSearch.BestImprovement(sol);

        sol.setCompare(compare2);
      } while (sol.value > v);
  

      cout << fixed << "iteration " << i << " sol " << sol.value << endl;

      if(sol.value > bestSol[tid].value){
        bestI = i;
        bestSol[tid] = sol;

        timeToTarget = ((double) (stop.tv_sec * 1000000 + stop.tv_usec) - (start.tv_sec * 1000000 + start.tv_usec)) / 1000000;
        gettimeofday(&stop, NULL);
      }

    } else {
      break;
    }
  }

  Solution sol = bestSol[0];

  for(int i = 1; i < MAXT; i++){
    if(bestSol[i].value > sol.value){
      sol = bestSol[i];
    }
  }

  cout << "Number of iterations = " << i << endl;
  cout << "Best Solution was found in iteration = " << bestI << endl;

  cout << "Time used by neighbor | Times each neighbor was used | Improvements" << endl;

  for(int i = 0; i < Nmax; i++){
    cout << fixed << "N_" << i+1 << " = " << localSearch.tempo[i] << " | " << localSearch.qtd[i] << " | " << localSearch.melhorias[i] << endl;
  }

  cout << fixed << "Time to find the best solution = " << timeToTarget << endl;

  gettimeofday(&stop, NULL);
  cout << fixed << "Solution = " << sol.value << endl;
  double elapsed = ((double) (stop.tv_sec * 1000000 + stop.tv_usec) - (start.tv_sec * 1000000 + start.tv_usec)) / 1000000;
  cout << fixed << "Time = " << elapsed << endl;

  sol.exportToFile("MAXSPACE/GRASP/solutions/"+ins.instanceName+".sol");

  delete compare;
  delete compare2;

  return 0;
}
