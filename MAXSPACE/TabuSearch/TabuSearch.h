#ifndef TABU_SEARCH_H
#define TABU_SEARCH_H

#include "Neighborhood.h"
#include "AddItem.h"
#include "ChangeItem.h"
#include "Repack.h"
#include "Solution.h"
#include "Instance.h"
#include "Change.h"
#include "Move.h"
#include "Utils.h"
#include <vector>
#include <time.h>
#include <sys/time.h>
#include "Strategy.h"

class TabuSearch{
private:
    Instance* ins;
    std::vector <Neighborhood*> N;
    const int maxSize, it, timeout, N_MAX;
    Utils utils;
    Strategy* strategy;
    timeval start, lastChange;

    bool listContains(const std::vector<Change*> & list, const Change* p);
public:
    std::vector<double> tempo;
    std::vector<int> qtd, melhorias;

    TabuSearch();
    TabuSearch(Instance* ins, int maxSize, int it, int timeout, timeval start, Strategy* strategy, int N_MAX, std::vector<Neighborhood*> N);
    ~TabuSearch();
    void runTabuSearch(Solution & sol);
};

#endif