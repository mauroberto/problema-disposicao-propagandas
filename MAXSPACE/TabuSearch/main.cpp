#include <stdio.h>
#include "TabuSearch.h"
#include "Solution.h"
#include "Instance.h"
#include "ConstructiveHeuristic.h"
#include <time.h>
#include "Utils.h"

#include "ChangeItemChange.h"
#include "AddItemChange.h"
#include "RepackChange.h"

using namespace std;

int timeout;
double elapsed;
struct timeval start, stop;

int main(int argc, char* argv[]){
    if (argc < 2) {
        cout << "Usage: ./main filename" << endl;
        return 1;
    }

	timeout = 3600;
	cout.precision(2);
    gettimeofday(&start, NULL);

    Instance ins(argv[1]);
    vector<Neighborhood*> N;

    N.push_back(new Move(&ins));
    N.push_back(new Repack(&ins));
    N.push_back(new AddItem(&ins));
    N.push_back(new ChangeItem(&ins));

    Strategy* strategy = new Randomly(100, 4);

    TabuSearch* ts = new TabuSearch(&ins, 40, 100, timeout, start, strategy, 4, N);
    ConstructiveHeuristic ch(ins);
    Solution sol = ch.greedyRandomizedConstruction3(0.3);
    ts->runTabuSearch(sol);

    sol.exportToFile("MAXSPACE/TabuSearch/solutions/"+ins.instanceName+".sol");

    cout << fixed << "Solution = " << sol.value << endl;
    gettimeofday(&stop, NULL);
    double elapsed = ((double) (stop.tv_sec * 1000000 + stop.tv_usec) - (start.tv_sec * 1000000 + start.tv_usec)) / 1000000;
    cout << fixed << "Time = " << elapsed << endl;
    delete ts;

    /*ChangeItemChange* d1a = new ChangeItemChange(&ins, 1, 1);
    ChangeItemChange* d1b = new ChangeItemChange(&ins, 1, 2);
    Change* d1c = d1a->clone();

    AddItemChange* d2a = new AddItemChange(&ins, 1);
    AddItemChange* d2b = new AddItemChange(&ins, 2);
    Change* d2c = d2a->clone();

    RepackChange* d3a = new RepackChange(&ins,1, 2, 1, 2);
    RepackChange* d3b = new RepackChange(&ins,1, 1, 1, 1);
    Change* d3c = d3a->clone();

    std::cout << typeid(*d1c).name() << " " << typeid(*d2c).name() << " " << typeid(*d3c).name() << "\n";

    std::cout << "Compare D1 types\n";
    std::cout << std::boolalpha << (*d1a == *d1b) << "\n";
    std::cout << std::boolalpha << (*d1b == *d1c) << "\n";
    std::cout << std::boolalpha << (*d1a == *d1c) << "\n";

    std::cout << "Compare D2 types\n";
    std::cout << std::boolalpha << (*d2a == *d2b) << "\n";
    std::cout << std::boolalpha << (*d2b == *d2c) << "\n";
    std::cout << std::boolalpha << (*d2a == *d2c) << "\n";

    std::cout << "Compare D3 types\n";
    std::cout << std::boolalpha << (*d3a == *d3b) << "\n";
    std::cout << std::boolalpha << (*d3b == *d3c) << "\n";
    std::cout << std::boolalpha << (*d3a == *d3c) << "\n";

    std::cout << "Compare mixed derived types\n";
    std::cout << std::boolalpha << (*d1a == *d2a) << "\n";
    std::cout << std::boolalpha << (*d2a == *d3a) << "\n";
    std::cout << std::boolalpha << (*d1a == *d3a) << "\n";
    std::cout << std::boolalpha << (*d1b == *d2b) << "\n";
    std::cout << std::boolalpha << (*d2b == *d3b) << "\n";
    std::cout << std::boolalpha << (*d1b == *d3b) << "\n";
    std::cout << std::boolalpha << (*d1c == *d2c) << "\n";
    std::cout << std::boolalpha << (*d2c == *d3c) << "\n";
    std::cout << std::boolalpha << (*d1c == *d3c) << "\n";


    delete d1a;
    delete d1b;
    delete d1c;

    delete d2a;
    delete d2b;
    delete d2c;

    delete d3a;
    delete d3b;
    delete d3c;*/

    for(int i = 0; i < 4; i++){
        delete N[i];
    }
    
    delete strategy;

    return 0;
}
