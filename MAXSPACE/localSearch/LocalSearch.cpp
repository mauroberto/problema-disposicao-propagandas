#include "LocalSearch.h"
#include "AddItem.h"
#include "ChangeItem.h"
#include "Move.h"
#include "Repack.h"

using namespace std;

#ifndef PATHSOL
#define PATHSOL "localSearch/solutions"
#endif

LocalSearch::LocalSearch(){}

LocalSearch::LocalSearch(Instance* ins, int timeout, timeval start) {
  this->ins = ins;

  this->neighbors.resize(4);

  this->timeout = timeout;

  this->start = start;

  this->neighbors[0] = new Move(this->ins);
  this->neighbors[1] = new Repack(this->ins);
  this->neighbors[2] = new AddItem(this->ins);
  this->neighbors[3] = new ChangeItem(this->ins);

  this->tempo.resize(4, 0);
  this->qtd.resize(4, 0);
  this->melhorias.resize(4, 0);
}

LocalSearch::~LocalSearch() { 
  int s = this->neighbors.size();
  for(int i = 0; i < s; i++){
    delete this->neighbors[i];
  }
}

Solution& LocalSearch::createSolution(){
  static Solution sol = Solution(ins->N, ins->A, ins->S, ins);

  for(int i = 0; i < ins->A; i++){
    int index = this->ins->ranking[i].second;
    Ad ad = ins->ads[index];

    sol.addBestFit(ad);
  }

  sol.remakeSol();

  return sol;
}

void LocalSearch::FirstImprovement(Solution & sol){
  Solution max = sol;

  do{
    Solution sol2 = max;
    Solution aux = sol2;
    for(int k = 0; k < ins->A; k++){
      if(sol2.y[k]) continue;

      for(int l = 0; l < ins->A; l++){
        if(!sol2.y[l]) continue;

        bool troca = sol2.changeAd(ins->ads[k], ins->ads[l]);

        if(troca && sol2.value > max.value){
          max = sol2;
          sol2 = aux;
          sol = max;
          return;
        }

        if(troca){
          break;
        }
      }
    }
  } while(max.value > sol.value);

  sol = max;
}


void LocalSearch::BestImprovement(Solution & sol){
  double v;
  double elapsed;
  int s = this->neighbors.size();
  struct timeval lastChange, stop;
  do{
    Solution max(sol);
    v = sol.value;
    for(int i=0; i<s; i++){
      this->qtd[i]++;
      gettimeofday(&lastChange, NULL);
      this->neighbors[i]->bestImprovement(sol);
      gettimeofday(&stop, NULL);
      this->tempo[i] += ((double) (stop.tv_sec * 1000000 + stop.tv_usec) - (lastChange.tv_sec * 1000000 + lastChange.tv_usec)) / 1000000;
      if(max < sol){
        this->melhorias[i]++;
        max = Solution(sol);
      } 
    }
    gettimeofday(&stop, NULL);
    elapsed = ((double) (stop.tv_sec * 1000000 + stop.tv_usec) - (start.tv_sec * 1000000 + start.tv_usec)) / 1000000;
  } while (sol.value > v && elapsed < timeout);
}