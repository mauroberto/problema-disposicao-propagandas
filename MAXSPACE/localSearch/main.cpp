#include <stdio.h>
#include "LocalSearch.h"
#include "Solution.h"
#include "Instance.h"
#include <time.h>
#include <sys/time.h>

using namespace std;

int timeout;
double elapsed;
struct timeval start, stop;

int main(int argc, char* argv[]){
  if (argc < 2) {
    cout << "Usage: ./main filename" << endl;
    return 1;
  }

  gettimeofday(&start, NULL);
	timeout = 3600;
	cout.precision(2);

  Instance ins = Instance(argv[1]);
	LocalSearch localSearch(&ins, timeout, start);

  Solution sol = localSearch.createSolution();
  localSearch.BestImprovement(sol);
  sol.exportToFile("MAXSPACE/localSearch/solutions/"+ins.instanceName+".sol");

  cout << fixed << "Solution = " << sol.value << endl;
  gettimeofday(&stop, NULL);
  elapsed = ((double) (stop.tv_sec * 1000000 + stop.tv_usec) - (start.tv_sec * 1000000 + start.tv_usec)) / 1000000;
  cout << fixed << "Time = " << elapsed << endl;
  return 0;
}
