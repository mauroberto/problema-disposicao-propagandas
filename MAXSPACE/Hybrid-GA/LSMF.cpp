#include "LSMF.h"
#include "Utils.h"

#ifndef PATHSOL
#define PATHSOL "LSMF/solutions"
#endif

using namespace std;

LSMF::LSMF() {}

LSMF::LSMF(Instance ins) {
  this->ins = ins;

  valids.resize(ins.A, true);
}

LSMF::~LSMF() { }

void LSMF::firstFit(Solution & sol, Ad & ad, int C){
  int rd = max(ad.rd, 0);
  int dl = min(ad.dl, ins.N-1);

  int count = 0;
  for(int j = 0; j < ins.N; j++){
    if(count >= ad.wMin){
      break;
    }

    if(sol.slots[j].s + ad.s <= C){
      count++;
    }
  }

  if(count >= ad.wMin){
    sol.y[ad.id] = true;
    for(int j = 0, c = 0; j < ins.N && c < ad.wMin; j++){
      if(sol.slots[j].s + ad.s <= C){
        sol.addCopy(ad, j, c);
        c++;
      }
    }
  }
}

pair<Solution,int> LSMF::FFD(int C, vector<int> ranking){
  Solution sol(ins.N, ins.A, ins.S, &ins);

  for(int i=0; i<ins.A; i++){
    int index = ranking[i];
    if(!valids[index]) continue;
    firstFit(sol, ins.ads[index], C);
  }

  int count = 0;
  for(int j=0; j<ins.N; j++){
    if(sol.slots[j].s > 0){
      count++;
    }
  }

  return make_pair(sol, count);
}

pair<Solution, int> LSMF::SUBSET_LSMF(){
  vector<pair<pair<int, int>, int > > ranking(ins.A);
  vector<pair<int, int> > ranking3(ins.A);
  vector<int> ranking2(ins.A);
  vector<int> CL(20);
  vector<int> CU(20);

  int ssum = 0;
  int smax = 0;
  for(int i=0; i<ins.A; i++){
    if(!valids[i]) continue;

    if(ins.ads[i].s > smax){
      smax = ins.ads[i].s;
    }

    ssum += ins.ads[i].s;

    ranking[i] = make_pair(make_pair(ins.ads[i].s, ins.ads[i].wMin), i);
    ranking3[i] = make_pair(ins.ads[i].s, i);
  }

  ssum /= ins.N;

  int C_l = max(ssum, smax);
  int C_u = C_l * 2;

  int I = 0, K = 9;

  CL[I] = C_l;
  CU[I] = C_u;

  sort(ranking.begin(), ranking.end(), greater<pair<pair<int, int>, int > >());
  for(int i=0; i<ins.A; i++){
    ranking2[i] = ranking[i].second;
  }

  STEP7:
  int C = (CL[I] + CU[I])/2;

  pair<Solution, int> ffd = FFD(C, ranking2);

  I++;
  if(ffd.second <= ins.N){
    CU[I] = C;
    CL[I] = CL[I-1];
    if(I <= K){
      goto STEP7;
    }
  }else{
    CU[I] = CU[I-1];
    CL[I] = C;
    goto STEP7;
  }

  pair<Solution, int> A = make_pair(ffd.first, C);

  I = 0, K = 9;

  CL[I] = C_l;
  CU[I] = C_u;

  sort(ranking3.begin(), ranking3.end(), greater<pair<int, int> >());
  for(int i=0; i<ins.A; i++){
    ranking2[i] = ranking3[i].second;
  }

  STEP3:
  C = (CL[I] + CU[I])/2;

  ffd = FFD(C, ranking2);

  I++;
  if(ffd.second <= ins.N){
    CU[I] = C;
    CL[I] = CL[I-1];
    if(I <= K){
      goto STEP3;
    }
  }else{
    CU[I] = CU[I-1];
    CL[I] = C;
    goto STEP3;
  }

  pair<Solution, int> B = make_pair(ffd.first, C);

  if(A.second < B.second) return A;

  return B;
}

Solution LSMF::runLSMF(vector<int> ranking){
  vector<int> discard(ins.A);

  pair<Solution, int> ini = SUBSET_LSMF();
  pair<Solution, int> ini2;


  if(ini.second <= ins.S){
    return ini.first;
  }

  for(int i=0; i<ins.A; i++){
    valids[i] = ini.first.y[i];
  }

  int K = 0;
  int i = 0;

  do{
    int index = ranking[i++];
    valids[index] = false;
    discard[K] = index;
    ini = SUBSET_LSMF();
    if(ini.second <= ins.S) {
      goto STEP9;
    }
    K++;
  } while(1);

  STEP7:
  valids[discard[K-1]] = false;

  STEP8:
  K--;

  STEP9:
  if(K == 0){
    return ini.first;
  }

  valids[discard[K-1]] = true;
  ini2 = SUBSET_LSMF();
  if(ini2.second > ins.S) {
    goto STEP7;
  }else{
    ini = ini2;
    goto STEP8;
  }
}
