/*
 * BRKGA.h
 *
 * This class encapsulates a Biased Random-key Genetic Algorithm (for minimization problems) with K
 * independent Populations stored in two vectors of Population, current and previous. It supports
 * multi-threading via OpenMP, and implements the following key methods:
 *
 * - BRKGA() constructor: initializes the populations with parameters described below.
 * - evolve() operator: evolve each Population following the BRKGA methodology. This method
 *                      supports OpenMP to evolve up to K independent Populations in parallel.
 *                      Please note that double Decoder::decode(...) MUST be thread-safe.
 *
 * Required hyperparameters:
 * - n: number of genes in each chromosome
 * - p: number of elements in each population
 * - pe: pct of elite items into each population
 * - pm: pct of mutants introduced at each generation into the population
 * - rhoe: probability that an offspring inherits the allele of its elite parent
 *
 * Optional parameters:
 * - K: number of independent Populations
 * - MAX_THREADS: number of threads to perform parallel decoding -- WARNING: Decoder::decode() MUST
 *                be thread-safe!
 *
 * Required templates are:
 * RNG: random number generator that implements the methods below.
 *     - RNG(unsigned long seed) to initialize a new RNG with 'seed'
 *     - double rand() to return a double precision random deviate in range [0,1)
 *     - unsigned long randInt() to return a >=32-bit unsigned random deviate in range [0,2^32-1)
 *     - unsigned long randInt(N) to return a unsigned random deviate in range [0, N] with N < 2^32
 *
 * Decoder: problem-specific decoder that implements any of the decode methods outlined below. When
 *          compiling and linking BRKGA with -fopenmp (i.e., with multithreading support via
 *          OpenMP), the method must be thread-safe.
 *     - double decode(const vector< double >& chromosome) const, if you don't want to change
 *       chromosomes inside the framework, or
 *     - double decode(vector< double >& chromosome) const, if you'd like to update a chromosome
 *
 *  Created on : Jun 22, 2010 by rtoso
 *  Last update: Sep 28, 2010 by rtoso
 *      Authors: Rodrigo Franco Toso <rtoso@cs.rutgers.edu>
 */

#ifndef GA_H
#define GA_H

#include <omp.h>
#include <algorithm>
#include <exception>
#include <stdexcept>
#include "Population.h"
#include <iostream>

template< class Decoder, class RNG >
class GA{
private:
	// Hyperparameters:
    const unsigned n;	// number of genes in the chromosome
	const unsigned p;	// number of elements in the population
	const unsigned pe;	// number of elite items in the population
	const double pm;	// number of mutants introduced at each generation into the population
	const double rhoe;	// probability that an offspring inherits the allele of its elite parent

	// Templates:
	RNG& refRNG;				// reference to the random number generator
	Decoder& refDecoder;	// reference to the problem-dependent Decoder

	// Parallel populations parameters:
	const unsigned K;				// number of independent parallel populations
	const unsigned MAX_THREADS;		// number of threads for parallel decoding

	// Data:
	std::vector< Population* > previous;	// previous populations
	std::vector< Population* > current;		// current populations

	std::vector< int > bestChromosome;
	double bestFitness;

	// Local operations:
	void initialize(const unsigned i);		// initialize current population 'i' with random keys
	void evolution(Population& curr, Population& next);
    inline std::vector< std::vector< int > > crossover(const std::vector<int> & parent1, const std::vector<int> & parent2);
    inline void mutate(std::vector< int > & chromosome);
    inline std::vector< int > createChild(const std::vector<int> & parent1, const std::vector<int> & parent2, int point);
	inline int randFromRoulette(const std::vector< double > & roulette);
public:
    /*
	 * Default constructor
	 * Required hyperparameters:
	 * - n: number of genes in each chromosome
	 * - p: number of elements in each population
	 * - pe: pct of elite items into each population
	 * - pm: pct of mutants introduced at each generation into the population
	 * - rhoe: probability that an offspring inherits the allele of its elite parent
	 *
	 * Optional parameters:
	 * - K: number of independent Populations
	 * - MAX_THREADS: number of threads to perform parallel decoding
	 *                WARNING: Decoder::decode() MUST be thread-safe; safe if implemented as
	 *                + double Decoder::decode(std::vector< double >& chromosome) const
	 */
	GA(unsigned n, unsigned p, double pe, double pm, double rhoe,
			Decoder& refDecoder, RNG& refRNG, unsigned K = 1, unsigned MAX_THREADS = 1);

    GA();
	/**
	 * Destructor
	 */
	~GA();

	/**
	 * Resets all populations with brand new keys
	 */
	void reset();

	/**
	 * Evolve the current populations following the guidelines of BRKGAs
	 * @param generations number of generations (must be even and nonzero)
	 * @param J interval to exchange elite chromosomes (must be even; 0 ==> no synchronization)
	 * @param M number of elite chromosomes to select from each population in order to exchange
	 */
	void evolve(unsigned generations = 1);

	/**
	 * Exchange elite-solutions between the populations
	 * @param M number of elite chromosomes to select from each population
	 */
	void exchangeElite(unsigned M);

	/**
	 * Returns the current population
	 */
	const Population& getPopulation(unsigned k = 0);

	/**
	 * Returns the chromosome with best fitness so far among all populations
	 */
	const std::vector< int >& getBestChromosome();

	/**
	 * Returns the best fitness found so far among all populations
	 */
	double getBestFitness();

	// Return copies to the internal parameters:
	unsigned getN() const;
	unsigned getP() const;
	unsigned getPe() const;
	double getPm() const;
	unsigned getPo() const;
	double getRhoe() const;
	unsigned getK() const;
	unsigned getMAX_THREADS() const;
};

template< class Decoder, class RNG >
GA< Decoder, RNG >::GA(){

}

template< class Decoder, class RNG >
GA< Decoder, RNG >::GA(unsigned _n, unsigned _p, double _pe, double _pm, double _rhoe,
		Decoder& decoder, RNG& rng, unsigned _K, unsigned MAX) : n(_n), p(_p),
		pe(unsigned(_pe * p)), pm(_pm), rhoe(_rhoe),
		refRNG(rng), refDecoder(decoder), K(_K), MAX_THREADS(MAX),
		previous(K, 0), current(K, 0) {
    
    // Error check:
	using std::range_error;
	if(n == 0) { throw range_error("Chromosome size equals zero."); }
	if(p == 0) { throw range_error("Population size equals zero."); }
	if(pe == 0) { throw range_error("Elite-set size equals zero."); }
	if(pe > p) { throw range_error("Elite-set size greater than population size (pe > p)."); }
	if(pm > 1 || pm < 0) { throw range_error("Mutant-probability (pm) out of range [0, 1]."); }
	if(K == 0) { throw range_error("Number of parallel populations cannot be zero."); }

	this->bestFitness = 0.0;

	// Initialize and decode each chromosome of the current population, then copy to previous:
	for(unsigned i = 0; i < K; ++i) {
		// Allocate:
		current[i] = new Population(n, p);

		// Initialize:
		initialize(i);

		// Then just copy to previous:
		previous[i] = new Population(*current[i]);
	}
}

template< class Decoder, class RNG >
GA< Decoder, RNG >::~GA() {
	for(unsigned i = 0; i < K; ++i) { delete current[i]; delete previous[i]; }
}

template< class Decoder, class RNG >
const Population& GA< Decoder, RNG >::getPopulation(unsigned k){
	return (*current[k]);
}

template< class Decoder, class RNG >
double GA< Decoder, RNG >::getBestFitness(){
	return bestFitness;
}

template< class Decoder, class RNG >
const std::vector< int >& GA< Decoder, RNG >::getBestChromosome(){
	return bestChromosome;	// The top one :-)
}

template< class Decoder, class RNG >
void GA< Decoder, RNG >::reset() {
	for(unsigned i = 0; i < K; ++i) { initialize(i); }
}

template< class Decoder, class RNG >
void GA< Decoder, RNG >::evolve(unsigned generations) {
	if(generations == 0) { throw std::range_error("Cannot evolve for 0 generations."); }

	for(unsigned i = 0; i < generations; ++i) {
		for(unsigned j = 0; j < K; ++j) {
			evolution(*current[j], *previous[j]);	// First evolve the population (curr, next)
			std::swap(current[j], previous[j]);		// Update (prev = curr; curr = prev == next)
		}
	}
}

template< class Decoder, class RNG >
void GA< Decoder, RNG >::exchangeElite(unsigned M) {
	if(M == 0 || M >= p) { throw std::range_error("M cannot be zero or >= p."); }

	for(unsigned i = 0; i < K; ++i) {
		// Population i will receive some elite members from each Population j below:
		unsigned dest = p - 1;	// Last chromosome of i (will be updated below)
		for(unsigned j = 0; j < K; ++j) {
			if(j == i) { continue; }

			// Copy the M best of Population j into Population i:
			for(unsigned m = 0; m < M; ++m) {
				// Copy the m-th best of Population j into the 'dest'-th position of Population i:
				const std::vector< int >& bestOfJ = current[j]->getChromosome(m);

				std::copy(bestOfJ.begin(), bestOfJ.end(), current[i]->getChromosome(dest).begin());

				current[i]->fitness[dest].first = current[j]->fitness[m].first;

				--dest;
			}
		}
	}

	for(int j = 0; j < int(K); ++j) { current[j]->sortFitness(); }
}

template< class Decoder, class RNG >
inline void GA< Decoder, RNG >::initialize(const unsigned i) {

	std::vector < int > ads(n);

	for(int j = 0; j < int(p)-1; ++j) {
		for(int k = 0; k < n; ++k){
			ads[k] = k;
		}
		for(int s = int(n) - 1, k = 0; k < int(n); --s, ++k) {
			int r = refRNG.randInt(s);
			(*current[i])(j, k) = ads[r];
			ads[r] = ads[s];
			ads.pop_back();
		}
	}

	(*current[i])(int(p)-1) = refDecoder.getInitialChromosome();

	// Decode:
	#ifdef _OPENMP
		#pragma omp parallel for num_threads(MAX_THREADS)
	#endif
	for(int j = 0; j < int(p); ++j) {
		current[i]->setFitness(j, this->refDecoder.decode((*current[i])(j)) );
	}

	// Sort:
	current[i]->sortFitness();
	if(current[i]->getBestFitness() < bestFitness){
		bestFitness = current[i]->getBestFitness();
		bestChromosome = current[i]->getChromosome(0);
	}
}


template< class Decoder, class RNG >
inline std::vector< int > GA< Decoder, RNG >::createChild(const std::vector<int> & parent1, const std::vector<int> & parent2, int point){
	std::vector < int > child(n);
    std::vector < bool > aux(n, false);

    for(unsigned j = 0; j < point; ++j){
        child[j] = parent1[j];
        aux[parent1[j]] = true;
    }

    for(unsigned j = 0, i = point; j < n && i < int(n); ++j){
        if(!aux[parent2[j]]){
            child[i] = parent2[j];
			aux[parent2[j]] = true;
            ++i; 
        }
    }

	return child;
}

template< class Decoder, class RNG >
inline void GA< Decoder, RNG >::mutate(std::vector< int > & chromosome){
	unsigned ad1, ad2;
	ad1 = refRNG.randInt(n-1);

	do{
		ad2 = refRNG.randInt(n-1);
	} while(ad2 == ad1);

	std::swap(chromosome[ad1], chromosome[ad2]);
}

template< class Decoder, class RNG >
inline std::vector< std::vector< int > > GA< Decoder, RNG >::crossover(const std::vector< int > & parent1, const std::vector< int > & parent2){
    unsigned point = refRNG.randInt(n-1);

    std::vector< std::vector< int > > children(2);

    children[0] = createChild(parent1, parent2, point);
	children[1] = createChild(parent2, parent1, point);

    return children;
}

template< class Decoder, class RNG >
inline int GA< Decoder, RNG >::randFromRoulette(const std::vector< double > & roulette){
	double r = refRNG.rand();
	int i = roulette.size() - 1;
	for(; i > 0 && roulette[i] > r; --i);
	return i;
}


template< class Decoder, class RNG >
inline void GA< Decoder, RNG >::evolution(Population& curr, Population& next) {

	//Create Roulette
	double total = 0.0;
	std::vector< double > fitness_relative(pe);
	for(unsigned i = 0; i < int(pe); ++i){
		total += curr.getFitness(i);
	}

	fitness_relative[0] = curr.getFitness(0);
	for(unsigned i = 1; i < int(pe); ++i){
		fitness_relative[i] = curr.getFitness(i)/total + fitness_relative[i-1];
	}

	unsigned k = 0;
	while(k < int(pe)){
		int p1, p2;

		//Get element from roullete
		p1 = randFromRoulette(fitness_relative);

		//Create Roulette2
		double total = 0.0;
		std::vector< int > index_roulette;
		std::vector< double > fitness_relative2(pe-1);
		for(unsigned i = 0; i < int(pe); ++i){
			if(i != p1){
				total += curr.getFitness(i);
				index_roulette.push_back(i); 
			}
		}

		fitness_relative2[0] = curr.getFitness(0);
		for(unsigned i = 1; i < int(pe)-1 ; ++i){
			fitness_relative[i] = curr.getFitness(index_roulette[i])/total + fitness_relative[i-1];
		}
		p2 = randFromRoulette(fitness_relative2);
		p2 = index_roulette[p2];

		/*do{
			p2 = refRNG.randInt(p-1);
		} while(p1 == p2);*/

		if(p1 == p2){
			std::cout << "Igual" << std::endl;
		}

		if(refRNG.rand() > rhoe) continue; //Testing crossover probability

		std::vector< std::vector < int > > children = crossover(curr.population[p1], curr.population[p2]);

		for(unsigned j = 0; j < 2; ++j){
			if(refRNG.rand() > pm) continue; //Testing mutation probability
			mutate(children[j]);
		}

		next(k) = children[0];
		k++;

		if(k == int(pe)) break;
		next(k) = children[1];
		k++;
	}

	#ifdef _OPENMP
		#pragma omp parallel for num_threads(MAX_THREADS)
	#endif
	for(int j = 0; j < int(p); ++j) {
		double fitness = refDecoder.decode(next.population[j]);
		/*if(fitness < bestFitness){
			std::cout << std::fixed << fitness << std::endl;
		}*/
		next.setFitness(j, fitness);
	}

	// Sort:
	next.sortFitness();
	if(next.getBestFitness() < bestFitness){
		bestFitness = next.getBestFitness();
		bestChromosome = next.getChromosome(0);
	}
}

template< class Decoder, class RNG >
unsigned GA<Decoder, RNG>::getN() const { return n; }

template< class Decoder, class RNG >
unsigned GA<Decoder, RNG>::getP() const { return p; }

template< class Decoder, class RNG >
unsigned GA<Decoder, RNG>::getPe() const { return pe; }

template< class Decoder, class RNG >
double GA<Decoder, RNG>::getPm() const { return pm; }

template< class Decoder, class RNG >
unsigned GA<Decoder, RNG>::getPo() const { return p - pe; }

template< class Decoder, class RNG >
double GA<Decoder, RNG>::getRhoe() const { return rhoe; }

template< class Decoder, class RNG >
unsigned GA<Decoder, RNG>::getK() const { return K; }

template< class Decoder, class RNG >
unsigned GA<Decoder, RNG>::getMAX_THREADS() const { return MAX_THREADS; }

#endif