#include <iostream>
#include "Decoder.h"
#include "MTRand.h"
#include "GA.h"
#include <vector>
#include <time.h>

using namespace std;

int timeout;
double elapsed;
clock_t clk;

int main(int argc, char* argv[]) {
	if (argc < 2) {
    	cout << "Usage: ./HGA filename [timeout p pe pm rhoe K MAX_GENS]" << endl;
    	return 1;
  	}

	clk = clock();
	timeout = 3600;
	cout.precision(2);
	if(argc > 2){
	  timeout = stoi(argv[2]);
  	}

	std::vector<double> solutions;

	double bestSolutionValue = 0.0, timeToTarget = 0.0;
	int bestI = 0;

	Instance ins = Instance(argv[1]);
	Decoder decoder = Decoder(ins);	 // initialize the decoder

	unsigned n = ins.A; 	// size of chromosomes
	unsigned p = 550;		// size of population
	double pe = 0.14;		// fraction of population to be the elite-set
	double pm = 0.20;		// fraction of population to be replaced by mutants
	double rhoe = 0.69;	// probability that offspring inherit an allele from elite parent
	unsigned K = 3;		// number of independent populations
	unsigned MAXT = 1;	// number of threads for parallel decoding

	long unsigned rngSeed = 0;	// seed to the random number generator
	MTRand rng;	// initialize the random number generator

	// initialize the BRKGA-based heuristic
	GA< Decoder, MTRand > algorithm(n, p, pe, pm, rhoe, decoder, rng, K, MAXT);

	unsigned generation = 0;		// current generation
	//const unsigned X_INTVL = 100;	// exchange best individuals at every 100 generations
	//const unsigned X_NUMBER = 2;	// exchange top 2 best
	unsigned MAX_GENS = 200;	// run for 2000 gens


	if(argc > 3){
   		p = stoi(argv[3], NULL);
  	}	   

	if(argc > 4){
   		pe = strtod(argv[4], NULL);
  	}

	if(argc > 5){
   		pm = strtod(argv[5], NULL);
  	}

	if(argc > 6){
   		rhoe = strtod(argv[6], NULL);
  	}

	if(argc > 7){
   		K = stoi(argv[7], NULL);
  	}	  

	if(argc > 8){
   		MAX_GENS = stoi(argv[8], NULL);
  	}	   

	do {
	    std::cout << "Generation " << ++generation << " of " << MAX_GENS << "\n";
		algorithm.evolve();	// evolve the population for one generation

		/*if((++generation) % X_INTVL == 0) {
			algorithm.exchangeElite(X_NUMBER);	// exchange top individuals
		}*/

		double v = algorithm.getBestFitness()*-1;

		solutions.push_back(v);

		if(v > bestSolutionValue){
			bestSolutionValue  = v;
			bestI = generation;
			timeToTarget = ((double) (clock() - clk)) / CLOCKS_PER_SEC;
		}

		cout << fixed << v << endl;

		elapsed = ((double) (clock() - clk)) / CLOCKS_PER_SEC;

		if(elapsed > timeout){
			cout << "TIMEOUT" << endl;
			break;
		}
	} while (generation < MAX_GENS);

	cout << "Number of iterations = " << generation << endl;
  	cout << "Best Solution was found in iteration = " << bestI << endl;
  	cout << fixed << "Time to find the best solution = " << timeToTarget << endl;

	cout << fixed << "Best solution found has objective value = " << algorithm.getBestFitness()*-1 << endl;
	elapsed = ((double) (clock() - clk)) / CLOCKS_PER_SEC;
	cout << fixed << "Time = " << elapsed << endl;

	decoder.genFileSol(algorithm.getBestChromosome());

	decoder.genFileEvol(solutions);

	return 0;
}
