#ifndef DECODER_H
#define DECODER_H

#include <iostream>
#include <list>
#include <vector>
#include <algorithm>
#include <fstream>
#include <string>
#include "Instance.h"
#include "Solution.h"
#include "LSMF.h"

class Decoder {
public:
	Instance ins;
    LSMF ls;
	Decoder(Instance ins);
	~Decoder();

	double decode(const std::vector< int >& chromosome);
    void genFileEvol(const std::vector< double >& solutions);
    void genFileSol(const std::vector< int >& chromosome);
    std::vector < int > getInitialChromosome();
private:
    Solution createSolution(const std::vector< int >& chromosome);
};

#endif
