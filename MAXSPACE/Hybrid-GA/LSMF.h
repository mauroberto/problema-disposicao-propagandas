#ifndef LSMF_H
#define LSMF_H

#include <iostream>
#include <vector>
#include <algorithm>
#include <fstream>
#include <string>
#include "Instance.h"
#include "Solution.h"

class LSMF {
public:
	Instance ins;
  std::vector<bool> valids;
  LSMF();
	LSMF(Instance ins);
	~LSMF();

  Solution runLSMF(std::vector<int> ranking);
private:
  std::pair<Solution,int> FFD(int C, std::vector<int> ranking);
  std::pair<Solution,int> SUBSET_LSMF();
  void firstFit(Solution & sol, Ad & ad, int C);
};

#endif
