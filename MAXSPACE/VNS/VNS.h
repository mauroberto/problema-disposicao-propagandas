#ifndef VNS_H
#define VNS_H

#include "Ad.h"
#include "Slot.h"
#include "Solution.h"
#include "Instance.h"
#include "ConstructiveHeuristic.h"
#include "Neighborhood.h"
#include "AddItem.h"
#include "ChangeItem.h"
#include "Repack.h"
#include "LocalSearch.h"
#include "TabuSearch.h"
#include <vector>
#include <time.h>
#include <sys/time.h>
#include "Move.h"

class VNS {
private:
  const int K_MAX, N_MAX, timeout;
  std::vector<int> c;
  std::vector<Neighborhood*> N;
  std::vector<Neighborhood*> N2;
  LocalSearch* ls;
  struct timeval start, stop, lastChange;

  void neighborhoodChange(Solution & sol, Solution & sol2, int & k, int & n, bool force_next);
  bool shake(Solution & sol, int n, int k);
  void VND(Solution & sol);
  void neighborhoodChangeVND(Solution & sol, Solution & sol2, int & n);
public:
  Instance* ins;
  std::vector<double> tempo;
  std::vector<int> qtd;
  std::vector<double> tempoVND;
  std::vector<int> qtdVND;
  std::vector<int> melhorias;
  std::vector<int> melhoriasVND;
  double timeToTarget;
  double tempoLS;

  VNS();
  VNS(Instance* instance, int K_MAX, int N_MAX, std::vector<Neighborhood*> N, std::vector<Neighborhood*> N2, int timeout, struct timeval START);
	~VNS();
  void runVNS(Solution & sol);
  void runVNSDualPhases(Solution & sol);
};

#endif
