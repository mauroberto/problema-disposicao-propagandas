#include "VNS.h"
#include <iostream>

using namespace std;

VNS::VNS(): K_MAX(1), N_MAX(0), timeout(3600){

}

VNS::VNS(Instance* instance, int _K_MAX, int _N_MAX, std::vector<Neighborhood*> N, std::vector<Neighborhood*> N2, int _timeout, struct timeval START)
: K_MAX(_K_MAX), N_MAX(_N_MAX), timeout(_timeout){

  gettimeofday(&start, NULL);
  this->ins = instance;
  this->start = START;

  this->ls = new LocalSearch(this->ins, timeout, start);
  Utils utils;

  this->c.resize(this->N_MAX, 1);

  this->tempo.resize(this->N_MAX, 0);
  this->qtd.resize(this->N_MAX, 0);
  this->tempoVND.resize(this->N_MAX, 0);
  this->qtdVND.resize(this->N_MAX, 0);
  this->melhorias.resize(this->N_MAX, 0);
  this->melhoriasVND.resize(this->N_MAX, 0);

  this->tempoLS = 0;

  this->N = N;
  this->N2 = N2;
}

VNS::~VNS(){
  delete this->ls;
}

void VNS::neighborhoodChange(Solution & sol, Solution & sol2, int & k, int & n, bool force_next){
  if(sol < sol2){
    if(sol.value < sol2.value){
      gettimeofday(&stop, NULL);
      timeToTarget = ((double) (stop.tv_sec * 1000000 + stop.tv_usec) - (start.tv_sec * 1000000 + start.tv_usec)) / 1000000;
    }

    melhorias[n]++;
    k = 1;
    n = 0;
    sol = sol2;
    this->qtd[n]++;

    gettimeofday(&lastChange, NULL);
    this->N[n]->generateNeighbors(sol);
    gettimeofday(&stop, NULL);
    this->tempo[n] += ((double) (stop.tv_sec * 1000000 + stop.tv_usec) - (lastChange.tv_sec * 1000000 + lastChange.tv_usec)) / 1000000;
  }else{
    k++;
  }

  if(k > this->K_MAX || force_next){
    k = 1;
    n++;
    if(n < this->N_MAX){
      this->qtd[n]++;
      gettimeofday(&lastChange, NULL);
      this->N[n]->generateNeighbors(sol);
      gettimeofday(&stop, NULL);
      this->tempo[n] += ((double) (stop.tv_sec * 1000000 + stop.tv_usec) - (lastChange.tv_sec * 1000000 + lastChange.tv_usec)) / 1000000;
    }
  }
}

void VNS::neighborhoodChangeVND(Solution & sol, Solution & sol2, int & n){
  if(sol < sol2){
    if(sol.value < sol2.value){
      gettimeofday(&stop, NULL);
      timeToTarget = ((double) (stop.tv_sec * 1000000 + stop.tv_usec) - (start.tv_sec * 1000000 + start.tv_usec)) / 1000000;
    }

    melhoriasVND[n]++;
    n = 0;
    sol = sol2;
  }else{
    n++;
  }
}

bool VNS::shake(Solution & sol, int n, int k){
  return this->N[n]->randomNeighborhood(sol);
}

void VNS::VND(Solution & sol){
  int i = 0;
  while(i < this->N_MAX){
    Solution sol2 = sol;

    this->qtdVND[i]++;
    gettimeofday(&lastChange, NULL);
    this->N2[i]->generateNeighbors(sol);
    this->N2[i]->maxNeighborhood(sol2);
    gettimeofday(&stop, NULL);
    this->tempoVND[i] += ((double) (stop.tv_sec * 1000000 + stop.tv_usec) - (lastChange.tv_sec * 1000000 + lastChange.tv_usec)) / 1000000;

    this->neighborhoodChangeVND(sol, sol2, i);

    gettimeofday(&stop, NULL);
    double elapsed = ((double) (stop.tv_sec * 1000000 + stop.tv_usec) - (start.tv_sec * 1000000 + start.tv_usec)) / 1000000;
    if(elapsed > timeout){
  		cout << "TIMEOUT" << endl;
  		break;
  	}
  }
}

void VNS::runVNS(Solution & sol){
  if(this->N_MAX == 0) return;

  this->c.resize(this->N_MAX, 1);
  
  int n = 0;
  this->qtd[n]++;
  gettimeofday(&lastChange, NULL);
  this->N[n]->generateNeighbors(sol);
  gettimeofday(&stop, NULL);
  this->tempo[n] += ((double) (stop.tv_sec * 1000000 + stop.tv_usec) - (lastChange.tv_sec * 1000000 + lastChange.tv_usec)) / 1000000;

  while(n < this->N_MAX){
    Solution sol2 = sol;


    gettimeofday(&lastChange, NULL);
    bool force_next = shake(sol2, n, c[n]);
    gettimeofday(&stop, NULL);
    this->tempo[n] += ((double) (stop.tv_sec * 1000000 + stop.tv_usec) - (lastChange.tv_sec * 1000000 + lastChange.tv_usec)) / 1000000;

    struct timeval vnd;

    gettimeofday(&vnd, NULL);  
    this->VND(sol2);
    gettimeofday(&stop, NULL);
    this->tempoLS += ((double) (stop.tv_sec * 1000000 + stop.tv_usec) - (vnd.tv_sec * 1000000 + vnd.tv_usec)) / 1000000;

    gettimeofday(&stop, NULL);
    double elapsed = ((double) (stop.tv_sec * 1000000 + stop.tv_usec) - (start.tv_sec * 1000000 + start.tv_usec)) / 1000000;
    if(elapsed > timeout){
  		cout << "TIMEOUT" << endl;
  		break;
  	}

    this->neighborhoodChange(sol, sol2, c[n], n, force_next);

    gettimeofday(&stop, NULL);
    elapsed = ((double) (stop.tv_sec * 1000000 + stop.tv_usec) - (start.tv_sec * 1000000 + start.tv_usec)) / 1000000;
    if(elapsed > timeout){
  		cout << "TIMEOUT" << endl;
  		break;
  	}

    cout << fixed << "solucao atual " << sol.value << endl;
  }
}


void VNS::runVNSDualPhases(Solution & sol){
  SolutionCompare * max = new MaximizeResidue();
  SolutionCompare * sc = new SolutionCompare();

  double v;

  sol.setCompare(sc); // apagar
  do {
    runVNS(sol);

    v = sol.value;

    gettimeofday(&stop, NULL);
    double elapsed = ((double) (stop.tv_sec * 1000000 + stop.tv_usec) - (start.tv_sec * 1000000 + start.tv_usec)) / 1000000;
    if(elapsed > timeout){
      cout << "TIMEOUT" << endl;
      break;
    }


    /*sol.setCompare(max);

    runVNS(sol);

    gettimeofday(&stop, NULL);
    elapsed = ((double) (stop.tv_sec * 1000000 + stop.tv_usec) - (start.tv_sec * 1000000 + start.tv_usec)) / 1000000;
    if(elapsed > timeout){
      cout << "TIMEOUT" << endl;
      break;
    }

    sol.setCompare(sc);*/
  } while (sol.value > v);

  delete sc;
  delete max;
}
