#ifndef NEIGHBORHOOD_H
#define NEIGHBORHOOD_H

#include "Ad.h"
#include "Slot.h"
#include "Solution.h"
#include "Instance.h"
#include "Utils.h"
#include "Change.h"

class Neighborhood{
public:
  Instance* ins;
  Utils utils;
  Solution max;
  std::vector<Change*> neighbors;
  
  Neighborhood();
  Neighborhood(Instance* ins);
  virtual ~Neighborhood();
  void maxNeighborhood(Solution & sol);
  bool randomNeighborhood(Solution & sol);
  virtual void generateNeighbors(Solution & sol);
  virtual void bestImprovement(Solution & sol);
};

#endif