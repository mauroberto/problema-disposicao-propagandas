#ifndef CHANGE_ITEM_CHANGE_H
#define CHANGE_ITEM_CHANGE_H

#include "Change.h"

class ChangeItemChange : public Change{
private:
    int add, rm;

    virtual bool isEqual(const Change& other) const{
        ChangeItemChange cic = static_cast<const ChangeItemChange&>(other);
        return add == cic.add && rm == cic.rm;
    }

public:
    ChangeItemChange();
    ChangeItemChange(Instance* ins, int add, int rm);
    ~ChangeItemChange() override;
    Solution applyChange(const Solution & sol) override;
    virtual Change* clone() const override;

    ChangeItemChange& operator=(const ChangeItemChange& other){
        Change::operator =(other);
        add = other.add;
        rm = other.rm;
        return *this;
    }
};

#endif