#include "ChangeItemChange.h"
#include <iostream>

ChangeItemChange::ChangeItemChange(){}

ChangeItemChange::ChangeItemChange(Instance* ins, int add, int rm) : Change(ins){
    this->ins = ins;
    this->add = add;
    this->rm = rm;
}

ChangeItemChange::~ChangeItemChange(){}

Solution ChangeItemChange::applyChange(const Solution & sol){
    Solution sol2(sol);
    sol2.changeAd(this->ins->ads[this->add], this->ins->ads[this->rm]);
    return sol2;
}

Change* ChangeItemChange::clone() const{
    return new ChangeItemChange(this->ins, this->add, this->rm);
}
