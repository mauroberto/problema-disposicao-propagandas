#include "Move.h"
#include <iostream>
#include <algorithm>

using namespace std;

Move::Move(){

}

Move::Move(Instance* instance) : Neighborhood(instance){
  this->ins = instance;
}

Move::~Move(){
}

void Move::generateNeighbors(Solution & sol){

  for (std::vector< Change* >::iterator it = this->neighbors.begin() ; it != this->neighbors.end(); ++it){
    delete (*it);
  }
  
  this->neighbors.clear();

  this->max = sol;
  for(int a1 = 0; a1 < this->ins->A - 1; a1++){
    if(!sol.y[a1]) continue;
    Ad ad1 = this->ins->ads[a1];

    int s = sol.w[a1].size();
    for(int j = 0; j < s; j++){
      int s1 = sol.w[a1][j];
      for(int s2 = s1+1; s2 < this->ins->N; s2++){
        if(!sol.slots[s2].ads[a1] && sol.canMove(this->ins->ads[a1], s1, s2)){
          Change *c = new MoveChange(this->ins, a1, j, s2);
          this->neighbors.push_back(c);
          Solution sol2 = c->applyChange(sol);
          if(max < sol2){
            this->max = sol2;
          }
        }
      }
    }
  }
}

void Move::bestImprovement(Solution & sol){
  for(int a1 = 0; a1 < this->ins->A - 1; a1++){
    if(!sol.y[a1]) continue;   
    int s = sol.w[a1].size();
    for(int j = 0; j < s; j++){
      int s1 = sol.w[a1][j];
        
      for(int s2 = s1+1; s2 < this->ins->N; s2++){
        if(sol.slots[s2].ads[a1]) continue;
          
        if(sol.slots[s2].s + this->ins->ads[a1].s <= this->ins->S){
          if(sol.canMove(this->ins->ads[a1], s1, s2)){
            sol.move(this->ins->ads[a1], j, s2);
            s1 = s2;
          }
        }
      }
    }
  }
}