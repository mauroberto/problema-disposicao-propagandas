#include "MoveChange.h"
#include <iostream>

MoveChange::MoveChange(){}

MoveChange::MoveChange(Instance* ins, int ad1, int c1, int s2) : Change(ins){
    this->ins = ins;
    this->ad1 = ad1;
    this->c1 = c1;
    this->s2 = s2;
}

MoveChange::~MoveChange(){}


Solution MoveChange::applyChange(const Solution & sol){
    Solution sol2(sol);
    sol2.move(this->ins->ads[ad1], c1, s2);
    return sol2;
}

Change* MoveChange::clone() const{
    return new MoveChange(this->ins, this->ad1, this->c1, this->s2);
}