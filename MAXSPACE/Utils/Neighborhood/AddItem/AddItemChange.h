#ifndef ADD_ITEM_CHANGE_H
#define ADD_ITEM_CHANGE_H

#include "Change.h"

class AddItemChange : public Change{
private:
    int add;

    virtual bool isEqual(const Change& other) const{
        AddItemChange aic = static_cast<const AddItemChange&>(other);
        return add == aic.add;
    }
public:
    AddItemChange();
    AddItemChange(Instance* ins, int add);
    ~AddItemChange() override;
    Solution applyChange(const Solution & sol) override;
    virtual Change* clone() const override;

    virtual AddItemChange& operator=(const AddItemChange& other){
        Change::operator =(other);
        add = other.add;
        return *this;
    }
};

#endif