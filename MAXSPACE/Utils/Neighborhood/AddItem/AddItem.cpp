#include "AddItem.h"
#include <iostream>
#include <algorithm>

AddItem::AddItem(){

}

AddItem::AddItem(Instance* instance) : Neighborhood(instance){
  this->ins = instance;
}

AddItem::~AddItem(){

}

void AddItem::generateNeighbors(Solution & sol){
  for (std::vector< Change* >::iterator it = this->neighbors.begin() ; it != this->neighbors.end(); ++it){
    delete (*it);
  }

  this->neighbors.clear();

  Solution sol2 = sol;
  this->max = sol;
  for(int i=0; i<this->ins->A; i++){
    if(!sol2.y[i] && sol2.addFirstFit(this->ins->ads[i])){// && sol2.bt.getSumInterval(ins->ads[i].rd, ins->ads[i].dl) >= ins->ads[i].s * ins->ads[i].wMin && sol2.bt.getMax(ins->ads[i].rd, ins->ads[i].dl) >= ins->ads[i].s){
      this->neighbors.push_back(new AddItemChange(this->ins, i));
      if(max < sol2){
        this->max = sol2;
      }
      sol2 = sol;
    }
  }
}

void AddItem::bestImprovement(Solution & sol){
  for(int k = 0; k < this->ins->A; k++){
    int i = this->ins->ranking[k].second;
    if(!sol.y[i]){
      sol.addFirstFit(this->ins->ads[i]);
    }
  }
}