#include "RepackChange.h"
#include <iostream>

RepackChange::RepackChange(){}

RepackChange::RepackChange(Instance* ins, int ad1, int ad2, int c1, int c2) : Change(ins){
    this->ins = ins;
    this->ad1 = ad1;
    this->c1 = c1;
    this->ad2 = ad2;
    this->c2 = c2;
}

RepackChange::~RepackChange(){}


Solution RepackChange::applyChange(const Solution & sol){
    Solution sol2(sol);
    sol2.repack(this->ins->ads[ad1], this->ins->ads[ad2], c1, c2);
    return sol2;
}

Change* RepackChange::clone() const{
    return new RepackChange(this->ins, this->ad1, this->ad2, this->c1, this->c2);
}