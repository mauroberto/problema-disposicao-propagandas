#ifndef REPACK_H
#define REPACK_H

#include "Neighborhood.h"
#include "RepackChange.h"
#include <vector>

class Repack : public Neighborhood{
public:
  Repack();
  Repack(Instance* instance);
  ~Repack() override;

  void generateNeighbors(Solution & sol) override;
  void bestImprovement(Solution & sol) override;
};

#endif