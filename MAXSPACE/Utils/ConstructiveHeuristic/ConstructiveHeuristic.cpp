#include "ConstructiveHeuristic.h"
#include <math.h>

using namespace std;

ConstructiveHeuristic::ConstructiveHeuristic(Instance instance){
  this->ins = instance;

  this->ranking.resize(ins.A);

  for(int i = 0; i < ins.A; i++) {
    double v = ins.ads[i].s * ins.ads[i].wMin;
    this->ranking[i].first = v;
    this->ranking[i].second = i;
  }

  sort(this->ranking.begin(), this->ranking.end(), greater<pair<double, int> >());
}

Solution ConstructiveHeuristic::createSolution(){
  Solution sol = Solution(ins.N, ins.A, ins.S, &ins);

  for(int i = 0; i < ins.A; i++){
    int index = this->ranking[i].second;
    Ad ad = ins.ads[index];

    sol.addFirstFit(ad);
  }

  return sol;
}

ConstructiveHeuristic::ConstructiveHeuristic(){

}

vector<int> ConstructiveHeuristic::greedyRanking(double alpha){
  vector<int> ranking;
  vector<int> restricted_candidate_list;
  vector<int> candidate_list;
  vector<double> cost;

  for(int i=0; i<ins.A; i++){
    candidate_list.push_back(i);
    cost.push_back(this->ins.ads[i].s);
  }

  for(int s = this->ins.A; s>0; s--){
    double max = cost[candidate_list[0]];
    double min = cost[candidate_list[0]];
    for(int i=1; i<s; i++){
      if(cost[candidate_list[i]] > max){
        max = cost[candidate_list[i]];
      }

      if(cost[candidate_list[i]] < min){
        min = cost[candidate_list[i]];
      }
    }

    double threshold = max - (alpha * (max-min));

    restricted_candidate_list.clear();
    for(int i=0; i<s; i++){
      if(cost[candidate_list[i]] >= threshold){
        restricted_candidate_list.push_back(i);
      }
    }

    int s2 = restricted_candidate_list.size();
    int item = 0;
    if(s2 > 1){
      item = utils.getRandomNumber(0, s2-1);
    }
    ranking.push_back(candidate_list[restricted_candidate_list[item]]);
    candidate_list[restricted_candidate_list[item]] = candidate_list[s-1];
  }

  return ranking;
}


Solution ConstructiveHeuristic::greedyRandomizedConstruction(double alpha){
  vector<int> ranking = this->greedyRanking(alpha);
  Solution sol = Solution(this->ins.N, this->ins.A, this->ins.S, &ins);

  for(int i=0; i<this->ins.A; i++){
    sol.addBestFit(this->ins.ads[ranking[i]]);
  }

  return sol;
}

Solution ConstructiveHeuristic::greedyRandomizedConstruction2(double alpha){
  vector<int> ranking = this->greedyRanking(alpha);
  Solution sol = Solution(this->ins.N, this->ins.A, this->ins.S, &ins);

  for(int i=0; i<this->ins.A; i++){
    sol.addWorstFit(this->ins.ads[ranking[i]]);
  }

  return sol;
}

Solution ConstructiveHeuristic::greedyRandomizedConstruction3(double alpha){
  vector<int> ranking = this->greedyRanking(alpha);
  Solution sol = Solution(this->ins.N, this->ins.A, this->ins.S, &ins);

  for(int i=0; i<this->ins.A; i++){
    sol.addFirstFit(this->ins.ads[ranking[i]]);
  }

  return sol;
}

pair <double, double> ConstructiveHeuristic::computeMDP(int it, double alpha){
  pair<double, double> p;
  p.first = 0.0;
  p.second = 0.0;

  vector<double> sols;

  for(int i=0; i<it; i++){
    Solution sol = greedyRandomizedConstruction3(alpha);
    p.first += sol.value;
    sols.push_back(sol.value);
  }

  p.first /= it;

  for(int i=0; i<it; i++){
    p.second += (sols[i] - p.first)*(sols[i] - p.first);
  }

  p.second = sqrt(p.second);

  return p;
}

ConstructiveHeuristic::~ConstructiveHeuristic(){

}
