#ifndef CH_H
#define CH_H
#include <vector>
#include "Instance.h"
#include "Solution.h"
#include "Utils.h"
#include "LocalSearch.h"

class ConstructiveHeuristic {
private:
  std::vector<std::pair<double, int> > ranking;
  std::vector<int> greedyRanking(double alpha);
public:
  Instance ins;
  Utils utils;
  ConstructiveHeuristic(Instance instance);
  ConstructiveHeuristic();
  Solution greedyRandomizedConstruction(double alpha);
  Solution greedyRandomizedConstruction2(double alpha);
  Solution greedyRandomizedConstruction3(double alpha);
  std::pair <double, double> computeMDP(int it, double alpha);
	~ConstructiveHeuristic();
  Solution createSolution();
};

#endif
