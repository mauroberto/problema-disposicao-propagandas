#include "Slot.h"

using namespace std;

Slot::Slot(){

}

Slot::Slot(int j, int A){
  this->s = 0;
  this->id = j;
  this->ads.resize(A, false);
}

Slot::Slot(const Slot& slot2){
  this->s = slot2.s;
  this->id = slot2.id;  
  this->ads = slot2.ads;  
}

void Slot::addCopy(Ad & ad){
  this->s += ad.s;
  this->ads[ad.id] = true;
}

void Slot::removeCopy(Ad & ad){
  this->s -= ad.s;
  this->ads[ad.id] = false;
}

Slot::~Slot(){

}

Slot& Slot::operator= (const Slot& slot2){
  if(this == &slot2){
    return *this;
  }
  this->s = slot2.s;
  this->id = slot2.id;
  this->ads = slot2.ads;
  return *this;
}
