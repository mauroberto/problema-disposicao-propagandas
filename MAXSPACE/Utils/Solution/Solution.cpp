#include "Solution.h"
#include <iostream>
#include <fstream>

using namespace std;

Solution::Solution(int N, int A, int S, Instance * ins){
  this->A = A;
  this->N = N;
  this->S = S;
  this->value = 0.0;
  this->slots.resize(N);
  this->y.resize(A, false);
  this->emptySpace = N*S;

  this->w.resize(A);
  for(int i = 0; i < A; i++){
    this->w[i].resize(ins->ads[i].wMin, -1);
  }

  for(int j = 0; j < N; j++) {
    this->slots[j] = Slot(j, A);
  }

  this->compare = new SolutionCompare();
}

Solution::Solution(int N, int A, int S, Instance * ins, SolutionCompare* compare){
  this->A = A;
  this->N = N;
  this->S = S;
  this->value = 0.0;
  this->slots.resize(N);
  this->y.resize(A, false);
  this->emptySpace = N*S;

  this->w.resize(A);
  for(int i = 0; i < A; i++){
    this->w[i].resize(ins->ads[i].wMin, -1);
  }

  for(int j = 0; j < N; j++) {
    this->slots[j] = Slot(j, A);
  }

  this->compare = compare->clone();
}

Solution::Solution(const Solution& sol2){
  this->A = sol2.A;
  this->S = sol2.S;
  this->N = sol2.N;
  this->emptySpace = sol2.emptySpace;
  this->value = sol2.value;
  this->slots = sol2.slots;
  this->y = sol2.y;
  this->w = sol2.w;
  this->compare = sol2.compare->clone();
}

Solution::Solution(){
  this->compare = new SolutionCompare();
}

Solution::Solution(SolutionCompare* compare){
  this->compare = compare->clone();
}

Solution::~Solution(){
  delete this->compare;
}

void Solution::setCompare(SolutionCompare* compare){
  delete this->compare;
  this->compare = compare->clone();
}

void Solution::sortAdd(int j){
  if(j <= 0 || j > this->N-1) return;

  Slot aux = this->slots[j];

  int i = j-1;
  while(i >= 0){
    if(this->slots[i].s < aux.s){
      this->slots[i+1] = this->slots[i];
      i--;
    }else{
      this->slots[i+1] = aux;
      break;
    }
  }

  if(i < 0){
    this->slots[0] = aux;
  }
}

void Solution::sortRemove(int j){
  if(j >= N - 1 || j < 0) return;

  Slot aux = this->slots[j];

  int i = j+1;
  while(i < N){
    if(this->slots[i].s > aux.s){
      this->slots[i-1] = this->slots[i];
      i++;
    } else{
      this->slots[i-1] = aux;
      break;
    }
  }

  if(i >= N){
    this->slots[N-1] = aux;
  }
}

bool Solution::addBestFit(Ad & ad){
  if(this->emptySpace < (ad.s * ad.wMin)) return false;

  if(this->y[ad.id]){
    cout << "WTF " << ad.id << endl;
  }

  int count = 0;
  for(int j = 0; j < this->N; j++){
    if(count >= ad.wMin){
      break;
    }

    if(this->slots[j].s + ad.s <= this->S){
      count++;
    }
  }

  if(count >= ad.wMin){
    this->y[ad.id] = true;

    for(int j = 0, c = 0; j < this->N && c < ad.wMin; j++){
      if(!this->slots[j].ads[ad.id] && this->slots[j].s + ad.s <= this->S){
        this->addCopy(ad, j, c);
        c++;

        this->sortAdd(j);
      }
    }
    return true;
  }
  return false;
}

bool Solution::addWorstFit(Ad & ad){
  if(this->emptySpace < (ad.s * ad.wMin)) return false;

  if(this->y[ad.id]){
    cout << "WTF " << ad.id << endl;
  }

  int count = 0;
  for(int j = this->N - 1; j >= 0; j--){
    if(count >= ad.wMin){
      break;
    }

    if(this->slots[j].s + ad.s <= this->S){
      count++;
    }
  }

  if(count >= ad.wMin){
    this->y[ad.id] = true;

    for(int j = this->N - 1, c = 0; j >= 0 && c < ad.wMin; j--){
      if(!this->slots[j].ads[ad.id] && this->slots[j].s + ad.s <= this->S){
        this->addCopy(ad, j, c);
        c++;

        this->sortAdd(j);
        j++;
      }
    }
    return true;
  }
  return false;
} 

bool Solution::addFirstFit(Ad & ad){
  if(this->emptySpace < (ad.s * ad.wMin)) return false;

  if(this->y[ad.id]){
    cout << "WTF " << ad.id << endl;
  }

  int count = 0;
  for(int j = 0; j < this->N; j++){
    if(count >= ad.wMin){
      break;
    }

    if(this->slots[j].s + ad.s <= this->S){
      count++;
    }
  }

  if(count >= ad.wMin){
    this->y[ad.id] = true;

    for(int j = 0, c = 0; j < this->N && c < ad.wMin; j++){
      if(this->slots[j].s + ad.s <= this->S){
        this->addCopy(ad, j, c);
        c++;
      }
    }
    return true;
  }
  return false;
}

void Solution::addCopy(Ad & ad, int j, int copy){
  this->slots[j].addCopy(ad);
  this->w[ad.id][copy] = this->slots[j].id;
  this->value += ad.s;
  this->emptySpace -= ad.s;
}

void Solution::removeCopy(Ad & ad, int j, int copy){
  this->slots[j].removeCopy(ad);
  this->value -= ad.s;
  this->w[ad.id][copy] = -1;
  this->emptySpace += ad.s;
}

bool Solution::changeAd(Ad & add, Ad & rm){
  if(this->emptySpace + (rm.s * rm.wMin) < (add.s * add.wMin)) return false;
  vector<int> slots;

  this->y[rm.id] = false;

  for(int j = 0; j < rm.wMin; j++){
    slots.push_back(this->w[rm.id][j]);
    removeCopy(rm, this->w[rm.id][j], j);
  }

  if(!this->addFirstFit(add)){
    for(int i=0, s = slots.size(); i < s; i++){
      int j = slots[i];
      addCopy(rm, j, i);
    }

    this->y[rm.id] = true;
    return false;
  }
  return true;
}

bool Solution::canRepack(Ad & a1, Ad & a2, int c1, int c2){
  return this->compare->canRepack(this, a1, a2, c1, c2);
}

bool Solution::canMove(Ad & a1, int s1, int s2){
  return this->compare->canMove(this, a1, s1, s2);
}

void Solution::move(Ad & a1, int c1, int s2){
  int s1 = this->w[a1.id][c1];
  this->slots[s1].removeCopy(a1);
  this->slots[s2].addCopy(a1);
  this->w[a1.id][c1] = s2;
}

void Solution::repack(Ad & a1, Ad & a2, int c1, int c2){
  int s1 = this->w[a1.id][c1];
  int s2 = this->w[a2.id][c2];
  this->move(a1, c1, s2);
  this->move(a2, c2, s1);
}

long int Solution::spaceSolution(){
  long int space = 0;
  for(int j=0; j<this->N; j++){
    int dif = this->S - this->slots[j].s;
    space += dif*dif;
  }

  return space;
}

void Solution::remakeSol(){
  vector<Slot> slots(this->N);

  for(int j=0; j < this->N; j++){
    slots[this->slots[j].id] = this->slots[j];
  }

  for(int j=0; j < this->N; j++){
    this->slots[j] = slots[j];
    //arr[j] = ins.S - sol.slots[j].s;
  }

  //this->bt = BITree(arr, ins.N);
}

Solution& Solution::operator= (const Solution& sol2){
  if (this == &sol2)
    return *this; //self assignment
  this->A = sol2.A;
  this->S = sol2.S;
  this->N = sol2.N;
  this->value = sol2.value;
  this->slots = sol2.slots;
  this->y = sol2.y;
  this->emptySpace = sol2.emptySpace;
  this->w = sol2.w;
  delete this->compare;
  this->compare = sol2.compare->clone();
  return *this;
}

bool Solution::operator< (Solution& sol2){
  return this->compare->compare(this, &sol2);
}


void Solution::exportToFile(const string path){

  ofstream opf;
  opf.open(path);

  vector<Slot> slots(this->N);

  for(int j=0; j < this->N; j++){
    slots[this->slots[j].id] = this->slots[j];
  }

  opf << fixed << this->value << endl;
  for(int j = 0; j < this->N; j++){
    opf << j << " ";
    for(int i = 0; i < this->A; i++){
      if(slots[j].ads[i]){
        opf << i << " ";
      }
    }
    opf << endl;
  }


  opf.close();
}