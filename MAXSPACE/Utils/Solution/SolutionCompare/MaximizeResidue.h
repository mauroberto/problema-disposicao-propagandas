#ifndef MAXIMIZE_RESIDUE_H
#define MAXIMIZE_RESIDUE_H

#include "SolutionCompare.h"


class MaximizeResidue : public SolutionCompare{
public:
  MaximizeResidue();
  ~MaximizeResidue() override;
  bool compare(void* sol, void* sol2) override;
  bool canRepack(void* sol, Ad & a1, Ad & a2, int c1, int c2) override;
  bool canMove(void* sol, Ad & a1, int s1, int s2) override;
  MaximizeResidue* clone() override;
};

#endif