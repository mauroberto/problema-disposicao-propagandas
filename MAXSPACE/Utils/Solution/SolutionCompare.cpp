#include "SolutionCompare.h"
#include "Solution.h"
#include <iostream>
#include <algorithm>

SolutionCompare::SolutionCompare(){}


SolutionCompare::~SolutionCompare(){

}

bool SolutionCompare::compare(void* s1, void* s2){
    Solution * sol = static_cast<Solution *>(s1);
    Solution * sol2 = static_cast<Solution *>(s2);

    if (&sol == &sol2)
        return false;

    if (sol->value < sol2->value){
        return true;
    }

    if (sol->value == sol2->value && sol->spaceSolution() > sol2->spaceSolution()){
        return true;
    }

    return false;
}

bool SolutionCompare::canRepack(void* s, Ad & a1, Ad & a2, int c1, int c2){

    Solution * sol = static_cast<Solution *>(s);

    int s1 = sol->w[a1.id][c1];
    int s2 = sol->w[a2.id][c2];
    int spc1 = sol->S - sol->slots[s1].s;
    int spc2 = sol->S - sol->slots[s2].s;
    int t1 = spc1*spc1 + spc2*spc2;
    int orS1 = sol->slots[s1].s;
    int orS2 = sol->slots[s2].s;

    int tam1 = orS1 + a2.s - a1.s;
    int tam2 = orS2 + a1.s - a2.s;
    spc1 = sol->S - tam1;
    spc2 = sol->S - tam2;
    int t2 = spc1*spc1 + spc2*spc2;

    if(t2 < t1 && tam1 <= sol->S && tam2 <= sol->S){
        return true;
    }
    return false;
}

bool SolutionCompare::canMove(void* s, Ad & a1, int s1, int s2){

    Solution * sol = static_cast<Solution *>(s);

    int spc1 = sol->S - sol->slots[s1].s;
    int spc2 = sol->S - sol->slots[s2].s;
    int t1 = spc1*spc1 + spc2*spc2;
    int orS1 = sol->slots[s1].s;
    int orS2 = sol->slots[s2].s;

    int tam1 = orS1 - a1.s;
    int tam2 = orS2 + a1.s;
    spc1 = sol->S - tam1;
    spc2 = sol->S - tam2;
    int t2 = spc1*spc1 + spc2*spc2;

    if(t2 < t1 && tam1 <= sol->S && tam2 <= sol->S){
        return true;
    }
    return false;
}

SolutionCompare* SolutionCompare::clone(){
    return new SolutionCompare();
}