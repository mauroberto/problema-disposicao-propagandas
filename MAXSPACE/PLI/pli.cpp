#include <iostream>
#include <vector>
#include <fstream>
#include <string>
#include <sstream>
#include "gurobi_c++.h"
#include <iostream>
#include <list>
#include <vector>
#include <algorithm>
#include <fstream>
#include <string>

//#define GETRELAX

typedef struct ad{
  int id;
  int s;
  int wMax;
  int wMin;
  int rd;
  int dl;
  int qt;
  double v;
} Ad;

typedef struct slot{
  int s;
  int id;
  bool * ads;
} Slot;

typedef struct sol{
  double value;
  std::vector <Slot> slots;
  bool * y;
} Sol;

typedef struct instance{
	int A;
	int N;
	int S;
	std::vector<Ad> ads;
} Instance;


using namespace std;

int A, N, S;
string instanceName;
vector<Ad> ads;

#ifndef PATHSOL
#define PATHSOL "MAXSPACE/PLI/solutions"
#endif

string getFileName(const string& s) {

   char sep = '/';

	 #ifdef _WIN32
   	sep = '\\';
	 #endif

   string aux;

   size_t i = s.rfind(sep, s.length());
   if (i != string::npos) {
      aux = s.substr(i+1, s.length() - i);
      i = aux.rfind('.', aux.length());
      if (i != string::npos){
        return (aux.substr(0, i));
      }
   }

   return("");
}

void readFile(char * fn) {
  ifstream infile(fn);

  instanceName = getFileName(fn);
  cout << instanceName << endl;

  infile >> A >> N >> S;

  int id, s, wMax, wMin, rd, dl;
  double v;

  while(infile >> id >> s >> rd >> dl >> wMin >> wMax >> v){
    Ad ad;
    ad.id = id;
    ad.s = s;
    ad.wMax = wMax;
    ad.wMin = wMin;
    ad.rd = rd;
    ad.dl = dl;
    ad.v = v;

    ads.push_back(ad);
  }
}

void formatData(int * dl, int * rd, double * val, double * size, int * wMin, int * wMax){
  for(int i=0; i<A; i++){
    dl[i] = ads[i].dl;
    rd[i] = ads[i].rd;
    val[i] = ads[i].s;
    size[i] = ads[i].s;
    wMin[i] = ads[i].wMin;
    wMax[i] = ads[i].wMax;
  }
}

void genFileSol(GRBVar ** X, double obj){
  char* fn = new char[256];

  sprintf(fn, "%s/%s.sol", PATHSOL, instanceName.c_str());

  ofstream opf;
  opf.open(fn);

  opf << fixed << obj << endl;
  for(int j=0; j<N; j++){
    opf << j << " ";
    for(int i=0; i<A; i++){
      int val = X[j][i].get(GRB_DoubleAttr_X);
      if(val == 1.0){
        opf << i << " ";
      }
    }
    opf << endl;
  }

  opf.close();
  delete [] fn;
}

int main(int argc, char *argv[]){
  if (argc < 2) {
    cout << "Usage: ./PLI filename timeout" << endl;
    return 1;
  }

  readFile(argv[1]);

  int timeout = 600;
  if(argc > 2){
	  timeout = stoi(argv[2]);
  }

  int dl[A];
  int rd[A];
  double size[A];
  double val[A];
  int wMin[A];
  int wMax[A];

  formatData(dl, rd, val, size, wMin, wMax);

  try {
    GRBEnv env = GRBEnv();
    #ifndef GETRELAX
      env.set(GRB_IntParam_Threads, 1);
    #endif
    GRBModel model = GRBModel(env);
    model.set(GRB_DoubleParam_TimeLimit, timeout);
    GRBVar **X = new GRBVar*[N];
    GRBVar *Y = new GRBVar[A];

    for(int i = 0; i < N; i++){
      X[i] = new GRBVar[A];
    }

    for (int j = 0; j < N; j++) {
      for (int i = 0; i < A; i++) {
        #ifdef GETRELAX
          X[j][i] = model.addVar(0, 1, 1, GRB_CONTINUOUS, "X_" + to_string(i) + "_" + to_string(j));
        #else 
          X[j][i] = model.addVar(0, 1, 1, GRB_BINARY, "X_" + to_string(i) + "_" + to_string(j));
        #endif
      }
    }

    for (int i = 0; i < A; i++) {
      #ifdef GETRELAX
        Y[i] = model.addVar(0, 1, 1, GRB_CONTINUOUS, "Y_" + to_string(i));
      #else 
        Y[i] = model.addVar(0, 1, 1, GRB_BINARY, "Y_" + to_string(i));
      #endif
    }

    GRBLinExpr obj = 0.0;

    for (int j = 0; j < N; j++) {
      obj.addTerms(val, X[j], A);
    }

    model.setObjective(obj, GRB_MAXIMIZE);

    //Constraint 01
    for (int j = 0; j < N; j++) {
      GRBLinExpr expr = 0.0;

      expr.addTerms(size, X[j], A);

      model.addConstr(expr, GRB_LESS_EQUAL, S, "c1" + to_string(j));
    }

    //Constraints 02
    for (int i = 0; i < A; i++) {
      GRBLinExpr expr = 0.0;

      int length = N;

      double * vals = new double[length];
      GRBVar * vars = new GRBVar[length];


      for (int j = 0; j < N; j++) {
        vals[j] = 1.0;
        vars[j] = X[j][i];
      }

      expr.addTerms(vals, vars, length);

      model.addConstr(expr, GRB_EQUAL, wMin[i]*Y[i], "c2" + to_string(i));

      delete[] vals;
      delete[] vars;
    }

    #ifdef GETRELAX
      model.optimize();
      cout << fixed << "Bound = " << model.get(GRB_DoubleAttr_ObjVal) << endl;
    #else 
      model.optimize();
      cout << fixed << "Solution = " << model.get(GRB_DoubleAttr_ObjVal) << endl;
      genFileSol(X, model.get(GRB_DoubleAttr_ObjVal));
    #endif

    for(int i=0; i < N; i++)
      delete[] X[i];
    delete[] Y;
    delete[] X;
  } catch(GRBException e) {
    cout << "Error code = " << e.getErrorCode() << endl;
    cout << e.getMessage() << endl;
  } catch (...) {
    cout << "Error during optimization" << endl;
  }

  return 0;
}
