from multiprocessing.dummy import Pool as ThreadPool
import os
#import commands
import sys
import datetime
import socket

threads = 4

problems = ["MAXSPACE", "MAXSPACE_VARIANTE"]
algorithms = ["GRASP", "GRASP-Tabu", "GRASP-VNS", "Hybrid-GA", "VNS"]
#algorithms = ["GRASP-VNS", "VNS"]

def run(cmd):
   os.system(cmd)

cmds = []

for problem in problems:
    for algorithm in algorithms:
        cmd = "cd irace/%s/%s && Rscript run.R > run.out" % (problem, algorithm)
        cmds.append(cmd)

pool = ThreadPool(threads)

pool.map(run, cmds)
pool.close()
pool.join()
