from multiprocessing.dummy import Pool as ThreadPool
import os
#import commands
import sys
import datetime
import socket
import pandas as pd

threads = 7
tempo = 600

PATH1 = "MAXSPACE_VARIANTE/"
PATH2 = "MAXSPACE/"
PATH3 = "MINSPACE_VARIANTE/"
PATH4 = "MINSPACE/"

classesMAXSPACE = [1, 5, 9, 13, 17, 21, 25, 29, 33]
classesMAXSPACE_VARIANTE = [2, 3, 4, 6, 7, 8, 10, 11, 12, 14, 15, 16, 18, 19, 20, 22, 23, 24, 26, 27, 28, 30, 31, 32, 34, 35, 36] 
classesMAXSPACE_VARIANTE2 = [39, 40, 41, 42, 43, 44, 45, 46, 47]


algorithms1 = [
    (PATH1+"PLI/PLI", PATH1+"PLI", "PLI", PATH1+"PLI", "")
]

algorithms2 = [
    (PATH2+"PLI/PLI", PATH2+"PLI", "PLI", PATH2+"PLI", "")
]


sizes = [
    (10000, 500, 200),
    (1000, 500, 250),
    (500, 250, 100),
    #(100, 75, 50)
]


data1 = []
data_time1 = []
data_gap1 = []
data2 = []
data_time2 = []
data_gap2 = []


def read_info_by_inst (inst, path):
    file_object = open("./%s/solutions_log/%s.log" % (path, inst), "r")
    file_object_time = open("./%s/solutions_time/%s.out" % (path, inst), "r")
    lines = file_object.readlines()
    lines_time = file_object_time.readlines()

    sol = -1
    if "Solution = " in lines[-1]:
        line = lines[-1].split(" = ")
        sol = float(line[1])

    gap = -1
    if "gap" in lines[-2]:
        line = lines[-2].split(" ")
        if line[-1][:-2] != '':
            gap = float(line[-1][:-2])
        else:
            gap = -2

    try:
        time = float(lines_time[-1])
    except:
        time = -1

    file_object.close()
    file_object_time.close()

    return (sol, gap, time)


for classe in classesMAXSPACE_VARIANTE:
    for tam in sizes:
        for i in range(0, 10):
            inst = "%d_%d_%d_%d_%d" % (tam[0], tam[1], tam[2], classe, i)
            for (alg, path, alg_name, alg_comp, params) in algorithms1:
                (sol, gap, time) = read_info_by_inst(inst, path)
                data1.append(sol)
                data_time1.append(time)
                data_gap1.append(gap)

for classe in classesMAXSPACE:
    for tam in sizes:
        for i in range(0, 10):
            inst = "%d_%d_%d_%d_%d" % (tam[0], tam[1], tam[2], classe, i)
            for (alg, path, alg_name, alg_comp, params) in algorithms2:
                (sol, gap, time) = read_info_by_inst(inst, path)
                data2.append(sol)
                data_time2.append(time)
                data_gap2.append(gap)


arr = os.listdir("./instances/literatura") 

instances_classes = ["201_25", "402_1", "600_2", "801_4", "1002_8", "t120_", "t501_", "u250_", "u500_", "u1000", "Hard28", "csAA", "csAB", "csBA", "csBB", "HARD"]

#"Schwerin" "Waescher_"

def contains(string, array):
    for substring in array:
        if substring in string:
            return True
    return False

for inst in arr:
    inst = inst.split(".")[0]
    if contains(inst, instances_classes):
        for (alg, path, alg_name, alg_comp, params) in algorithms2:
            (sol, gap, time) = read_info_by_inst(inst, path)
            data2.append(sol)
            data_time2.append(time)
            data_gap2.append(gap)


data_max = {
    "sol": data2,
    "gap": data_gap2,
    "time": data_time2
}

df = pd.DataFrame(data = data_max)

print(df.gap[df.gap >= 0].shape[0])
print(df.gap)
mean_gap_MAX = df.gap[df.gap >= 0].mean()
print(mean_gap_MAX)
qtd_solved = df[df.gap == 0].shape[0]
print(qtd_solved)
qtd_total = df.shape[0]
print(qtd_total)
qtd_not_started = df[df.gap == -2].shape[0]
print(((qtd_solved*1.0)/qtd_total)*100.0, "%")
print(qtd_not_started)


data_max_var = {
    "sol": data1,
    "gap": data_gap1,
    "time": data_time1
}

df2 = pd.DataFrame(data = data_max_var)

print(df2.gap[df2.gap >= 0].shape[0])
print(df2.gap)
mean_gap_MAX = df2.gap[df2.gap >= 0].mean()
print(mean_gap_MAX)
qtd_solved = df2[df2.gap == 0].shape[0]
print(qtd_solved)
qtd_total = df2.shape[0]
print(qtd_total)
qtd_not_started = df2[df2.gap == -2].shape[0]
print(((qtd_solved*1.0)/qtd_total)*100.0, "%")
print(qtd_not_started)